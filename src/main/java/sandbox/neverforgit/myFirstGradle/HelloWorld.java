package sandbox.neverforgit.myFirstGradle;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Created by Andrew A. Campbell on 10/22/15.
 */
public class HelloWorld {

    private static final Logger LOGGER =   LogManager.getLogger(HelloWorld.class);

    public static void main(String[] args) {
    MessageService messageService = new MessageService();

    String message = messageService.getMessage();
    LOGGER.info("Received message: " + message);
    }
}