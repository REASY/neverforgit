package sandbox.neverforgit.serialTripRouter;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.router.RoutingModule;

import java.util.LinkedList;

public abstract class SerialRouter implements RoutingModule {
	public abstract sandbox.neverforgit.serialTripRouter.TripInformation getTripInformation(double time, Link startLink, Link endLink);

	public abstract LinkedList<sandbox.neverforgit.serialTripRouter.RouteInformationElement> calcRoute(Link fromLink, Link toLink, double departureTime, Person person);

}
