package sandbox.neverforgit.serialTripRouter.run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * For testing inter-process communication with Python.
 * Created by Andrew A. Campbell on 3/16/17.
 */
public class IPCTest {
	public static void main(String[] args){
		System.out.printf("////////////////////////////////////////////////////////////////////////////////////\n");
		System.out.printf("Testing P2J communication!\n");
		System.out.printf("////////////////////////////////////////////////////////////////////////////////////\n");
		try {
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//			PrintWriter writer = new PrintWriter("result.txt", "UTF-8");
			String s = bufferRead.readLine();
			while(s.equals("x")==false) {
//				writer.println(s);
				System.out.printf(s + "\n");
				s = bufferRead.readLine();
			}
//			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		System.out.printf("////////////////////////////////////////////////////////////////////////////////////\n");
		System.out.printf("Testing J2P communication!\n");
		System.out.printf("////////////////////////////////////////////////////////////////////////////////////\n");
		System.out.println("First msg");
		System.out.println("Second msg");
		System.out.println("x");

	}
}
