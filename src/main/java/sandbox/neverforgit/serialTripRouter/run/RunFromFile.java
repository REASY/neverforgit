package sandbox.neverforgit.serialTripRouter.run;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import sandbox.neverforgit.serialTripRouter.SerialRouterImpl;
import sandbox.neverforgit.serialTripRouter.TripInformation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Used to test an implementation from within Java. Reads OD pairs and departure times from an input file.
 *
 * Sys input:
 * 0) Path to network file
 * 1) Path to network validation file. This contains all the link travel times.
 * 2) Path to file with example OD pairs and departures times
 *
 * Created by Andrew A. Campbell on 3/15/17.
 */
public class RunFromFile {

	public static void main(String args[]) throws IOException {
		String netPath = args[0];
		String valPath = args[1];
		String testPath = args[2];


		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(netPath);


		System.out.printf("Initializing Serial Router");
		SerialRouterImpl router = new SerialRouterImpl(valPath, network);

		BufferedReader br = new BufferedReader(new FileReader(testPath));
		int i = 0;
		for (String line = br.readLine(); line != null; line = br.readLine()){
			String[] row = line.split(",");
			double oX = Double.valueOf(row[0]);
			double oY = Double.valueOf(row[1]);
			double dX = Double.valueOf(row[2]);
			double dY = Double.valueOf(row[3]);
			double dT = Double.valueOf(row[4]); //departure time
			Link oL = NetworkUtils.getNearestLink(network, new Coord(oX, oY));
			Link dL = NetworkUtils.getNearestLink(network, new Coord(dX, dY));
			TripInformation tI = router.getTripInformation(dT, oL, dL);
			System.out.printf("Estimated travle time: %f%n", tI.getTripTravelTime());
			System.out.printf("%n");
		}
	}

}
