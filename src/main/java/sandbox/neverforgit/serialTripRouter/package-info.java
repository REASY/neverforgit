/**
 * Created by Andrew A. Campbell on 3/9/17.
 *
 * Mostly copied from https://github.com/colinsheppard/beam/tree/calibration/src/main/java/beam/sim/traveltime
 *
 * Used to implement a MATSim TripRouter from serialized dynamic network data
 *
 */
package sandbox.neverforgit.serialTripRouter;