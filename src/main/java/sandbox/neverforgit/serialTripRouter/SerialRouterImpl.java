package sandbox.neverforgit.serialTripRouter;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.population.routes.LinkNetworkRouteImpl;
import org.matsim.core.router.AStarEuclidean;
import org.matsim.core.router.EmptyStageActivityTypes;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.PreProcessEuclidean;
import org.matsim.facilities.Facility;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SerialRouterImpl extends SerialRouter {
	private static final Logger log = Logger.getLogger(SerialRouterImpl.class);
	private ExogenousTravelTime travelTimeFunction;
	private Network network;

	AStarEuclidean routingAlg;

	int cachMiss = 0, getCount = 0;

//	SerialRouterImpl() {
//		this(EVGlobalData.data.TRAVEL_TIME_FILEPATH, EVGlobalData.data.ROUTER_CACHE_READ_FILEPATH);
//	}

	/*
	 * Construct a BeamRouter where the travel time data are deserialized from the file system
	 */
//	public SerialRouterImpl(String travelTimeFunctionSerialPath) {
//		// TODO - set travelTimeFunction right here
////		if (EVGlobalData.data.travelTimeFunction == null) {
////			EVGlobalData.data.travelTimeFunction = ExogenousTravelTime.LoadTravelTimeFromSerializedData(travelTimeFunctionSerialPath);
////		}
//		ExogenousTravelTime travelTimeFunction = ExogenousTravelTime.LoadTravelTimeFromSerializedData(travelTimeFunctionSerialPath);
////		if (EVGlobalData.data.newTripInformationCache == null) {
////			EVGlobalData.data.newTripInformationCache = new TripInfoCache();
////			if ((new File(routerCacheSerialPath)).exists()) {
////				EVGlobalData.data.newTripInformationCache.deserializeHotCacheKryo(routerCacheSerialPath);
////			}
////		}
//	}

	/*
	 * Construct a BeamRouter where the travel time data are extracted from an object of type TravelTimeCalculator
	 */
	public SerialRouterImpl(String validationTravelTimeDataFilePath, Network network) {
		this.travelTimeFunction = ExogenousTravelTime.LoadTravelTimeFromValidationData(validationTravelTimeDataFilePath, true);
		this.network = network;
		configure();
	}

	//TODO this class should use dependency injection instead of hard-coded configuration
	private void configure() {
		PreProcessEuclidean preProcessData = new PreProcessEuclidean(this.travelTimeFunction);
		preProcessData.run(network);
		routingAlg = new AStarEuclidean(network, preProcessData, this.travelTimeFunction);
	}

	public LinkedList<RouteInformationElement> calcRoute(Link fromLink, Link toLink, double departureTime, Person person) {
//		if (network == null) configure();
		Path path = null;
		try {
			path = routingAlg.calcLeastCostPath(fromLink.getFromNode(), toLink.getToNode(), departureTime, person, null);
		} catch (NullPointerException e) {
		}
		double now = departureTime;
		LinkedList<RouteInformationElement> routeInformation = new LinkedList<>();
		if (path == null) return routeInformation;
		if (path.links.size() == 0) {
			double linkTravelTime = this.travelTimeFunction.getLinkTravelTime(fromLink, now, person, null);
			routeInformation.add(new RouteInformationElement(fromLink, linkTravelTime));
		} else {
			for (Link link : path.links) {
				double linkTravelTime = this.travelTimeFunction.getLinkTravelTime(link, now, person, null);
				if (linkTravelTime == Double.MAX_VALUE) {
					linkTravelTime = link.getLength() / link.getFreespeed();
				}
				routeInformation.add(new RouteInformationElement(link, linkTravelTime));
				now += linkTravelTime;
			}
		}
		return routeInformation;
	}

	public TripInformation getTripInformation(double time, Link startLink, Link endLink) {

		MathUtil mu = new MathUtil();
		double roundedTime = mu.roundDownToNearestInterval(time, 30.0 * 60.0);
		// This key is used for getting cached routes
//		String key = EVGlobalData.data.linkAttributes.get(startLink.getId().toString()).get("group") + "---" +
//				EVGlobalData.data.linkAttributes.get(endLink.getId().toString()).get("group") + "---" +
//				EVGlobalData.data.travelTimeFunction.convertTimeToBin(roundedTime);
		getCount++;
//		TripInformation resultTrip = EVGlobalData.data.newTripInformationCache.getTripInformation(key);
//		if (resultTrip == null) {
//			cachMiss++;
//			resultTrip = new TripInformation(roundedTime, calcRoute(startLink, endLink, roundedTime, null));
//			EVGlobalData.data.newTripInformationCache.putTripInformation(key, resultTrip);
//		}
//		TripInformation resultTrip = new TripInformation(roundedTime, calcRoute(startLink, endLink, roundedTime, null));
		TripInformation resultTrip = new TripInformation(roundedTime, calcRoute(startLink, endLink, roundedTime, null));
		return resultTrip;
		/*
		if(!EVGlobalData.data.tripInformationCache.containsKey(key)){
			cachMiss++;
			TripInformation newInfo = new TripInformation(roundedTime, calcRoute(startLink, endLink, roundedTime, null));
			synchronized (EVGlobalData.data.tripInformationCache) {
				EVGlobalData.data.tripInformationCache.put(key, newInfo);
			}
		}
		*/
//		return EVGlobalData.data.tripInformationCache.get(key);
	}

	private TripInformation getTripInformation(double departureTime, Id<Link> fromLinkId, Id<Link> toLinkId) {
		if (network == null) configure();
		return getTripInformation(departureTime, network.getLinks().get(fromLinkId), network.getLinks().get(toLinkId));
	}

	public List<? extends PlanElement> calcRoute(Facility<?> fromFacility, Facility<?> toFacility, double departureTime, Person person) {
		List list = new ArrayList();
		SerialLeg leg = new SerialLeg("car");
		list.add(leg);
		leg.setDepartureTime(departureTime);
		leg.setTravelTime(getTripInformation(departureTime, fromFacility.getLinkId(), toFacility.getLinkId()).getTripTravelTime());
		LinkNetworkRouteImpl route = new LinkNetworkRouteImpl(fromFacility.getLinkId(), toFacility.getLinkId());
		leg.setRoute(route);
		route.setDistance(1);
		return list;
	}

	public StageActivityTypes getStageActivityTypes() {
		return EmptyStageActivityTypes.INSTANCE;
	}

//	public String toString() {
//		return "BeamRouter: hot cache contains " + EVGlobalData.data.newTripInformationCache.hotCache.size() + " trips, current cache miss rate: " + this.cachMiss + "/" + this.getCount;
//	}

	public class MathUtil {
		public Double roundUpToNearestInterval(double num, double interval) {
			return Math.floor(num / interval) * interval + Math.ceil((num / interval) - Math.floor(num / interval)) * interval;
		}

		public double roundDownToNearestInterval(double num, double interval) {
			return Math.floor(num / interval) * interval + Math.floor((num / interval) - Math.floor(num / interval)) * interval;
		}
	}
}

