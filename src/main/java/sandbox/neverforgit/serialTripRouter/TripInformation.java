package sandbox.neverforgit.serialTripRouter;

import java.io.Serializable;
import java.util.LinkedList;

public class TripInformation implements Serializable {
	LinkedList<RouteInformationElement> routeInformationElements = new LinkedList<>();
	double departureTime, tripTravelTime = 0.0, tripTravelDistance = 0.0, tripAverageSpeed;

	// Zero-arg constructor necessary to use kyro for serailization
	public TripInformation(){
	}
	public TripInformation(double departureTime, LinkedList<RouteInformationElement> route){
		this.departureTime = departureTime;
		routeInformationElements = route;
		for(RouteInformationElement elem : routeInformationElements){
			tripTravelTime += elem.getLinkTravelTime();
			tripTravelDistance += elem.getLinkTravelDistance();
		}
		tripAverageSpeed = tripTravelDistance / tripTravelTime;
	}

	public LinkedList<RouteInformationElement> getRouteInfoElements() {
		return routeInformationElements;
	}

	public double getTripDistance() {
		return this.tripTravelDistance;
	}

	public double getTripTravelTime() {
		return this.tripTravelTime;
	}

	public double getTripAverageSpeed() {
		return this.tripAverageSpeed;
	}


}
