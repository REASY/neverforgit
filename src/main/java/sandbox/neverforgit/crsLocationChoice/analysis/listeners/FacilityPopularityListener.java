package sandbox.neverforgit.crsLocationChoice.analysis.listeners;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.population.*;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.ShutdownEvent;
import org.matsim.core.controler.events.StartupEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.ShutdownListener;
import org.matsim.core.controler.listener.StartupListener;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.utils.CSVWriters;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Records how many agents visited each facility during each iteration and writes to a csv in the MATSim output
 * directory of the last iteration.
 *
 * Created by Andrew A. Campbell on 2/11/17.
 */
public class FacilityPopularityListener implements StartupListener, IterationEndsListener, ShutdownListener{

    private HashMap<String, Integer[]>  facilityCrowds = new HashMap<>();
    private String rootOutDir;
    private String lastIteration;

    public FacilityPopularityListener(String rootOutDir){
        this.rootOutDir = rootOutDir;
    }

    /**
     * Initialize the data containers at start of simulation.
     * @param event
     */
    @Override
    public void notifyStartup(StartupEvent event) {
        ActivityFacilities activityFacilities = event.getServices().getScenario().getActivityFacilities();
        // The first iteration may not be 0!
        int nIterations = event.getServices().getScenario().getConfig().controler().getLastIteration()
                - event.getServices().getScenario().getConfig().controler().getFirstIteration()
                + 1;
        this.lastIteration = String.valueOf(event.getServices().getScenario().getConfig().controler().getLastIteration());
        // Initialize with Arrays of all zeros
        for (Id<ActivityFacility> id: activityFacilities.getFacilities().keySet()){
            Integer[] dataContainer = new Integer[nIterations];
            Arrays.fill(dataContainer, 0);
            this.facilityCrowds.put(id.toString(), dataContainer);
        }
    }

    /**
     * Updates the data containers with how many agents attended each location in the most recent iteration.
     * @param event
     */
    @Override
    public void notifyIterationEnds(IterationEndsEvent event) {
        int idx = event.getIteration() - event.getServices().getScenario().getConfig().controler().getFirstIteration();
        // Iterate through all the experienced plans. Increment the count for the approp
        for (Person person: event.getServices().getScenario().getPopulation().getPersons().values()){
            Plan experiencedPlan = (Plan)person.getSelectedPlan().getCustomAttributes().get(PlanCalcScoreConfigGroup.EXPERIENCED_PLAN_KEY);
            for (PlanElement pe: experiencedPlan.getPlanElements()){
                if (pe instanceof Activity){
                    Activity act = (Activity) pe;
                    String id = act.getFacilityId().toString();
                    this.facilityCrowds.get(id)[idx]++; //increment the crowd count for that iteration
                }
            }
        }
    }

    /**
     * Write the results to output file.
     * @param event
     */
    @Override
    public void notifyShutdown(ShutdownEvent event) {
        // Process the data for writing
        ArrayList<String> ids = new ArrayList<>();
        ArrayList<ArrayList<Integer>> values = new ArrayList<>();
        for (String id: this.facilityCrowds.keySet()){
            ids.add(id);
            ArrayList<Integer> theseVals = new ArrayList<>(Arrays.asList(this.facilityCrowds.get(id)));
            values.add(theseVals);
        }
        // Write the output
        String runId = event.getServices().getConfig().controler().getRunId();
        String outPath = MessageFormat.format("{0}/ITERS/it.{1}/{2}.{1}.Analysis.FacilityPopularity.txt",
                this.rootOutDir, this.lastIteration, runId);
        try {
            CSVWriters.writeFileGeneric(values, ids, ',', outPath);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
