package sandbox.neverforgit.crsLocationChoice.analysis.listeners;

import org.matsim.api.core.v01.population.*;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.ShutdownEvent;
import org.matsim.core.controler.events.StartupEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.ShutdownListener;
import org.matsim.core.controler.listener.StartupListener;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;
import sandbox.neverforgit.utils.CSVWriters;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;

/**
 * This should be the only listener we need for analysis. It records every agent-tour. Value recorded are:
 * iteration number, facility ID, person id, round trip distance, round trip time, rated activity performance time
 *
 * Created by Andrew A. Campbell on 2/11/17.
 */
public class RatedActivityTravelListener implements StartupListener, IterationEndsListener, ShutdownListener {
    private ArrayList<String> iterations = new ArrayList<>();
    private ArrayList<String> facilityIds = new ArrayList<>();
    private ArrayList<String> personIds = new ArrayList<>();
    private ArrayList<String> facilityDists = new ArrayList<>();
    private ArrayList<String> facilityTimes = new ArrayList<>();
    private ArrayList<String> activityTimes = new ArrayList<>();

    private String rootOutDir;
    private String lastIteration;

    public RatedActivityTravelListener(String rootOutDir){
        this.rootOutDir = rootOutDir;
    }

    /**
     * Initialize the data containers.
     * @param event
     */
    @Override
    public void notifyStartup(StartupEvent event) {
        // The first iteration may not be 0!
        int nIterations = event.getServices().getScenario().getConfig().controler().getLastIteration()
                - event.getServices().getScenario().getConfig().controler().getFirstIteration()
                + 1;
        int nPersons = event.getServices().getScenario().getPopulation().getPersons().size();
        this.lastIteration = String.valueOf(event.getServices().getScenario().getConfig().controler().getLastIteration());
    }


    /**
     * Updates the data containers with the round-trip distances and times
     * @param event
     */
    @Override
    public void notifyIterationEnds(IterationEndsEvent event) {
        int idx = event.getIteration() - event.getServices().getScenario().getConfig().controler().getFirstIteration();
        // Iterate through all the experienced plans. Increment the count for the appropriate facility
        for (Person person: event.getServices().getScenario().getPopulation().getPersons().values()){
            Plan experiencedPlan = (Plan)person.getSelectedPlan().getCustomAttributes().get(PlanCalcScoreConfigGroup.EXPERIENCED_PLAN_KEY);
            Double dist = 0.0;
            Double travelTime = 0.0;
            Double actTime = 0.0;
            String facId = "";
            for (PlanElement pe: experiencedPlan.getPlanElements()){
                if (pe instanceof Leg){
                    Leg leg = (Leg) pe;
                    dist += leg.getRoute().getDistance();
                    travelTime += leg.getTravelTime();
                } else if (pe instanceof Activity){
                    Activity act = (Activity) pe;
                    // Check if it is a rated activity facility
                    if (act.getType().equals(CRSGlobalData.data.SINGLE_RATED_ACTIVITY_TYPE)){
                        facId = act.getFacilityId().toString();
                        actTime += (act.getEndTime() - act.getStartTime());
                    }
                }
            }
            this.iterations.add(String.valueOf(event.getIteration()));
            this.facilityIds.add(facId);
            this.personIds.add(person.getId().toString());
            this.facilityDists.add(String.valueOf(dist));
            this.facilityTimes.add(String.valueOf(travelTime));
            this.activityTimes.add(String.valueOf(actTime));
        }
    }

    /**
     * Write the results to output file.
     * @param event
     */
    @Override
    public void notifyShutdown(ShutdownEvent event) {
        // Populate header
        ArrayList<String> header = new ArrayList<>();
        header.add("iteration");
        header.add("facility_id");
        header.add("person_id");
        header.add("distance");
        header.add("travel_time");
        header.add("act_time");

        // Populate values
        ArrayList<ArrayList<String>> values = new ArrayList<>();
        values.add(this.iterations);
        values.add(this.facilityIds);
        values.add(this.personIds);
        values.add(this.facilityDists);
        values.add(this.facilityTimes);
        values.add(this.activityTimes);

        // Write the output
        String runId = event.getServices().getConfig().controler().getRunId();
        String outPathDist = MessageFormat.format("{0}/ITERS/it.{1}/{2}.{1}.Analysis.RatedActivityTravel.txt",
                this.rootOutDir, this.lastIteration, runId);
        try {
            CSVWriters.writeFileGeneric(values, header, ',', outPathDist);
        }  catch (IOException e) {
            e.printStackTrace();
        }

    }
}
