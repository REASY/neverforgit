package sandbox.neverforgit.crsLocationChoice.analysis.listeners;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.ShutdownEvent;
import org.matsim.core.controler.events.StartupEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.ShutdownListener;
import org.matsim.core.controler.listener.StartupListener;
import sandbox.neverforgit.utils.CSVWriters;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Records the round-trip distance and travel time for each agent on each iteration.
 *
 * Created by Andrew A. Campbell on 2/11/17.
 */
public class AgentTravelListener implements StartupListener, IterationEndsListener, ShutdownListener {
    private HashMap<String, Double[]> agentDists = new HashMap<>();
    private HashMap<String, Double[]> agentTimes = new HashMap<>();
    private String rootOutDir;
    private String lastIteration;

    public AgentTravelListener(String rootOutDir){
        this.rootOutDir = rootOutDir;
    }

    /**
     * Initialize the data containers.
     * @param event
     */
    @Override
    public void notifyStartup(StartupEvent event) {
        // The first iteration may not be 0!
        int nIterations = event.getServices().getScenario().getConfig().controler().getLastIteration()
                - event.getServices().getScenario().getConfig().controler().getFirstIteration()
                + 1;
        this.lastIteration = String.valueOf(event.getServices().getScenario().getConfig().controler().getLastIteration());
        // Initialize data containers
        for (Id<Person> id: event.getServices().getScenario().getPopulation().getPersons().keySet()){
            Double[] distsContainer = new Double[nIterations];
            Double[] timesContainer = new Double[nIterations];
            Arrays.fill(distsContainer, 0.0);
            Arrays.fill(timesContainer, 0.0);
            this.agentDists.put(id.toString(), distsContainer);
            this.agentTimes.put(id.toString(), timesContainer);
        }
    }


    /**
     * Updates the data containers with the round-trip distances and times
     * @param event
     */
    @Override
    public void notifyIterationEnds(IterationEndsEvent event) {
        int idx = event.getIteration() - event.getServices().getScenario().getConfig().controler().getFirstIteration();
        // Iterate through the memorized experienced plans and increment distances and times
        for (Person person: event.getServices().getScenario().getPopulation().getPersons().values()){
            Plan experiencedPlan = (Plan)person.getSelectedPlan().getCustomAttributes().get(PlanCalcScoreConfigGroup.EXPERIENCED_PLAN_KEY);
            String personId = person.getId().toString();
            Double dist = 0.0;
            Double time = 0.0;
            for (PlanElement pe: experiencedPlan.getPlanElements()){
                if (pe instanceof Leg){
                    Leg leg = (Leg) pe;
                    dist += leg.getRoute().getDistance();
                    time += leg.getTravelTime();
                    this.agentDists.get(personId)[idx] += dist;
                    this.agentTimes.get(personId)[idx] += time;
                }
            }
        }
    }

    /**
     * Write the results to output file.
     * @param event
     */
    @Override
    public void notifyShutdown(ShutdownEvent event) {
        // Process the data for writing
        ArrayList<String> ids = new ArrayList<>();
        ArrayList<ArrayList<Double>> dists = new ArrayList<>();
        ArrayList<ArrayList<Double>> times = new ArrayList<>();

        for (String id: this.agentDists.keySet()){
            ids.add(id);
            ArrayList<Double> distVals = new ArrayList<>(Arrays.asList(this.agentDists.get(id)));
            ArrayList<Double> timeVals = new ArrayList<>(Arrays.asList(this.agentTimes.get(id)));
            dists.add(distVals);
            times.add(timeVals);
        }
        // Write the output
        String runId = event.getServices().getConfig().controler().getRunId();
        // distances
        String outPathDist = MessageFormat.format("{0}/ITERS/it.{1}/{2}.{1}.AgentDistances.txt",
                this.rootOutDir, this.lastIteration, runId);
        try {
            CSVWriters.writeFileGeneric(dists, ids, ',', outPathDist);
        }  catch (IOException e) {
            e.printStackTrace();
        }
        // travel times
        String outPathTime = MessageFormat.format("{0}/ITERS/it.{1}/{2}.{1}.Analysis.AgentTimes.txt",
                this.rootOutDir, this.lastIteration, runId);
        try {
            CSVWriters.writeFileGeneric(times, ids, ',', outPathTime);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
