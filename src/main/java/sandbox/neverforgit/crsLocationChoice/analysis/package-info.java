/**
 * Package for conducting analysis on experimental results.
 *
 * Created by Andrew A. Campbell on 2/11/17.
 */
package sandbox.neverforgit.crsLocationChoice.analysis;