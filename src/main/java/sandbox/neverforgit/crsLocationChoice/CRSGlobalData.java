package sandbox.neverforgit.crsLocationChoice;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.core.router.TripRouter;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.crowdRatingSystem.CrowdRatingSystem;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Andrew A. Campbell on 1/25/17.
 */
public class CRSGlobalData {

    private static final Logger log = Logger.getLogger(CRSGlobalData.class);

    public static CRSGlobalData data = null;

    public static void initializer() {
        data = new CRSGlobalData();
    }

    //Data fields to be populated by Main class
    public CrowdRatingSystem CROWD_RATING_SYSTEM;
    public ArrayList<String> RATED_ACTIVITIES;
    public ActivityFacilities ACTIVITY_FACILITIES;
    public TripRouter TRIP_ROUTER;

    ////
    //Simple simulation data
    //
    public String HOME_ACTIVITY_TYPE;
    public String SINGLE_RATED_ACTIVITY_TYPE; // name of the single rated activity in the simple simulation
    // Parameters from config.xml file
    public double HOME_DEP_TIME;
    public double VOT_TRAVELING;  // the cost of traveling
    public double VOT_ACTIVITY;
    public double RATED_ACTIVITY_DURATION;
    public double REPLANNING_COEFFICIENT;


    //Other data fields
    public HashMap<Id<ActivityFacility>, HashMap<Double, Double>> RATED_ACTIVITY_SCORES = new HashMap<>();  //keys are person and activity start time
    public void addRatedActivity(Id<ActivityFacility> id, Double time, Double score){
        if (this.RATED_ACTIVITY_SCORES.containsKey(id)){
            this.RATED_ACTIVITY_SCORES.get(id).put(time, score);
         } else {
            HashMap<Double, Double> scores = new HashMap<>();
            scores.put(time, score);
            this.RATED_ACTIVITY_SCORES.put(id, scores);
        }
    }

    public void resetRatedActivities(){
        this.RATED_ACTIVITY_SCORES.clear();
    }



}
