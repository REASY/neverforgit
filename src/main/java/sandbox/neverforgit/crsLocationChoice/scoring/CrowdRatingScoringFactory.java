package sandbox.neverforgit.crsLocationChoice.scoring;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.scoring.ScoringFunction;
import org.matsim.core.scoring.ScoringFunctionFactory;
import org.matsim.core.scoring.SumScoringFunction;
import org.matsim.core.scoring.functions.*;

/**
 * Created by Andrew A. Campbell on 1/26/17.
 */
public class CrowdRatingScoringFactory implements ScoringFunctionFactory {

    private Scenario scenario;

    public CrowdRatingScoringFactory(Scenario scenario){
        this.scenario = scenario;
    }


    @Override
    public ScoringFunction createNewScoringFunction(Person person) {
        SumScoringFunction sumScoringFunction = new SumScoringFunction();

        // Score activities, legs, payments and being stuck
        // with the default MATSim scoring based on predictedUtility parameters in the config file.
        // Only difference is
        final ScoringParameters params =
                new ScoringParameters.Builder(this.scenario, person.getId()).build();
        sumScoringFunction.addScoringFunction(new RatedCharyparNagelActivityScoring(params));
        sumScoringFunction.addScoringFunction(new CharyparNagelLegScoring(params, this.scenario.getNetwork()));
        sumScoringFunction.addScoringFunction(new CharyparNagelMoneyScoring(params));
        sumScoringFunction.addScoringFunction(new CharyparNagelAgentStuckScoring(params));

        return sumScoringFunction;
    }
}
