package sandbox.neverforgit.crsLocationChoice.scoring;


import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.core.scoring.functions.ActivityTypeOpeningIntervalCalculator;
import org.matsim.core.scoring.functions.ActivityUtilityParameters;
//import org.matsim.core.scoring.functions.ScoringParameters;
import org.matsim.core.scoring.functions.OpeningIntervalCalculator;
import org.matsim.core.scoring.functions.ScoringParameters;
import org.matsim.core.utils.misc.Time;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;

import java.util.ArrayList;

/**
 * Based on CharyparNagelActivityScoring (copied 01/26/17). This version is identical to the original except that
 * duration scores of rated activities are scaled by a quality coefficient unique to the facility.
 */
public class RatedCharyparNagelActivityScoring implements org.matsim.core.scoring.SumScoringFunction.ActivityScoring {

    protected double score;
    private double currentActivityStartTime;
    private double firstActivityEndTime;

    private static final double INITIAL_LAST_TIME = 0.0;
    private static final double INITIAL_FIRST_ACT_END_TIME = Time.UNDEFINED_TIME;
    private static final double INITIAL_SCORE = 0.0;

    private static int firstLastActWarning = 0;
    private static short firstLastActOpeningTimesWarning = 0;

    private final ScoringParameters params;
    private final OpeningIntervalCalculator openingIntervalCalculator;

    private Activity firstActivity;

    private ArrayList<String> ratedActivities;
    private ActivityFacilities activityFacilities;

    private static final Logger log = Logger.getLogger(org.matsim.core.scoring.functions.CharyparNagelActivityScoring.class);

    public RatedCharyparNagelActivityScoring(final ScoringParameters params) {
        this(params, new ActivityTypeOpeningIntervalCalculator(params));
    }

    public RatedCharyparNagelActivityScoring(final ScoringParameters params,
                                             final OpeningIntervalCalculator openingIntervalCalculator) {
        this.params = params;
        this.currentActivityStartTime = INITIAL_LAST_TIME;
        this.firstActivityEndTime = INITIAL_FIRST_ACT_END_TIME;
        this.score = INITIAL_SCORE;

        firstLastActWarning = 0 ;
        firstLastActOpeningTimesWarning = 0 ;
        this.openingIntervalCalculator = openingIntervalCalculator;
        this.ratedActivities = CRSGlobalData.data.RATED_ACTIVITIES;
        this.activityFacilities = CRSGlobalData.data.ACTIVITY_FACILITIES;
    }

    @Override
    public void finish() {
        if (this.firstActivity != null) {
            handleMorningActivity();
        }
        // Else, no activity has started so far.
        // This probably means that the plan contains at most one activity.
        // We cannot handle that correctly, because we do not know what it is.
    }

    @Override
    public double getScore() {
        return this.score;
    }

    public double calcActScore(final double arrivalTime, final double departureTime, final Activity act) {

        ActivityUtilityParameters actParams = this.params.utilParams.get(act.getType());
        if (actParams == null) {
            throw new IllegalArgumentException("acttype \"" + act.getType() + "\" is not known in predictedUtility parameters " +
                    "(module name=\"planCalcScore\" in the config file).");
        }

        double tmpScore = 0.0;

        if (actParams.isScoreAtAll()) {
			/* Calculate the times the agent actually performs the
			 * activity.  The facility must be open for the agent to
			 * perform the activity.  If it's closed, but the agent is
			 * there, the agent must wait instead of performing the
			 * activity (until it opens).
			 *
			 *                                             Interval during which
			 * Relationship between times:                 activity is performed:
			 *
			 *      O________C A~~D  ( 0 <= C <= A <= D )   D...D (not performed)
			 * A~~D O________C       ( A <= D <= O <= C )   D...D (not performed)
			 *      O__A+++++C~~D    ( O <= A <= C <= D )   A...C
			 *      O__A++D__C       ( O <= A <= D <= C )   A...D
			 *   A~~O++++++++C~~D    ( A <= O <= C <= D )   O...C
			 *   A~~O+++++D__C       ( A <= O <= D <= C )   O...D
			 *
			 * Legend:
			 *  A = arrivalTime    (when agent gets to the facility)
			 *  D = departureTime  (when agent leaves the facility)
			 *  O = openingTime    (when facility opens)
			 *  C = closingTime    (when facility closes)
			 *  + = agent performs activity
			 *  ~ = agent waits (agent at facility, but not performing activity)
			 *  _ = facility open, but agent not there
			 *
			 * assume O <= C
			 * assume A <= D
			 */

            double[] openingInterval = openingIntervalCalculator.getOpeningInterval(act);
            double openingTime = openingInterval[0];
            double closingTime = openingInterval[1];

            double activityStart = arrivalTime;
            double activityEnd = departureTime;

            if ((openingTime >=  0) && (arrivalTime < openingTime)) {
                activityStart = openingTime;
            }
            if ((closingTime >= 0) && (closingTime < departureTime)) {
                activityEnd = closingTime;
            }
            if ((openingTime >= 0) && (closingTime >= 0)
                    && ((openingTime > departureTime) || (closingTime < arrivalTime))) {
                // agent could not perform action
                activityStart = departureTime;
                activityEnd = departureTime;
            }
            double duration = activityEnd - activityStart;

            // disutility if too early
            if (arrivalTime < activityStart) {
                // agent arrives to early, has to wait
                tmpScore += this.params.marginalUtilityOfWaiting_s * (activityStart - arrivalTime);
            }

            // disutility if too late

            double latestStartTime = actParams.getLatestStartTime();
            if ((latestStartTime >= 0) && (activityStart > latestStartTime)) {
                tmpScore += this.params.marginalUtilityOfLateArrival_s * (activityStart - latestStartTime);
            }

            //NOTE: this is one of two points where I change the original code. The second is at the end
            // when we update the CRSGlobalData (Andrew)
            // predictedUtility of performing an action, duration is >= 1, thus log is no problem
            double typicalDuration = actParams.getTypicalDuration();

            double qualityCoefficient;

            if (this.ratedActivities.contains(act.getType())){
                Id<ActivityFacility> facId = act.getFacilityId();
                ActivityFacility aF = this.activityFacilities.getFacilities().get(act.getFacilityId());
                qualityCoefficient = (Double) aF.getCustomAttributes().get("qualityCoefficient");
            } else {
                qualityCoefficient = 1;
            }

            if ( this.params.usingOldScoringBelowZeroUtilityDuration ) {
                if (duration > 0) {
                    double utilPerf = this.params.marginalUtilityOfPerforming_s * qualityCoefficient * typicalDuration
                            * Math.log((duration / 3600.0) / actParams.getZeroUtilityDuration_h());
                    double utilWait = this.params.marginalUtilityOfWaiting_s * duration;
                    tmpScore += Math.max(0, Math.max(utilPerf, utilWait));
                } else {
                    tmpScore += 2*this.params.marginalUtilityOfLateArrival_s*Math.abs(duration);
                }
            } else {
                if ( duration >= 3600.*actParams.getZeroUtilityDuration_h() ) {
                    double utilPerf = this.params.marginalUtilityOfPerforming_s * qualityCoefficient * typicalDuration
                            * Math.log((duration / 3600.0) / actParams.getZeroUtilityDuration_h());
                    // also removing the "wait" alternative scoring.
                    tmpScore += utilPerf ;
                } else {
//					if ( wrnCnt < 1 ) {
//						wrnCnt++ ;
//						log.warn("encountering duration < zeroUtilityDuration; the logic for this was changed around mid-nov 2013.") ;
//						log.warn( "your final score thus will be different from earlier runs; set usingOldScoringBelowZeroUtilityDuration to true if you "
//								+ "absolutely need the old version.  See https://matsim.atlassian.net/browse/MATSIM-191." );
//						log.warn( Gbl.ONLYONCE ) ;
//					}

                    // below zeroUtilityDuration, we linearly extend the slope ...:
                    double slopeAtZeroUtility = this.params.marginalUtilityOfPerforming_s * qualityCoefficient * typicalDuration / ( 3600.*actParams.getZeroUtilityDuration_h() ) ;
                    if ( slopeAtZeroUtility < 0. ) {
                        // (beta_perf might be = 0)
                        System.err.println("beta_perf: " + this.params.marginalUtilityOfPerforming_s);
                        System.err.println("typicalDuration: " + typicalDuration );
                        System.err.println( "zero utl duration: " + actParams.getZeroUtilityDuration_h() );
                        throw new RuntimeException( "slope at zero predictedUtility < 0.; this should not happen ...");
                    }
                    double durationUnderrun = actParams.getZeroUtilityDuration_h()*3600. - duration ;
                    if ( durationUnderrun < 0. ) {
                        throw new RuntimeException( "durationUnderrun < 0; this should not happen ...") ;
                    }
                    tmpScore -= slopeAtZeroUtility * durationUnderrun ;
                }


            }

            // disutility if stopping too early
            double earliestEndTime = actParams.getEarliestEndTime();
            if ((earliestEndTime >= 0) && (activityEnd < earliestEndTime)) {
                tmpScore += this.params.marginalUtilityOfEarlyDeparture_s * (earliestEndTime - activityEnd);
            }

            // disutility if going to away to late
            if (activityEnd < departureTime) {
                tmpScore += this.params.marginalUtilityOfWaiting_s * (departureTime - activityEnd);
            }

            // disutility if duration was too short
            double minimalDuration = actParams.getMinimalDuration();
            if ((minimalDuration >= 0) && (duration < minimalDuration)) {
                tmpScore += this.params.marginalUtilityOfEarlyDeparture_s * (minimalDuration - duration);
            }
        }
        //NOTE: This is the second point where I change the code (Andrew)
        // Update CRSGLobalData to store activity scores of most recent iteration
        CRSGlobalData.data.addRatedActivity(act.getFacilityId(), arrivalTime, tmpScore );
        return tmpScore;
    }

    private void handleOvernightActivity(Activity lastActivity) {
        assert firstActivity != null;
        assert lastActivity != null;


        if (lastActivity.getType().equals(this.firstActivity.getType())) {
            // the first Act and the last Act have the same type:
            if (firstLastActOpeningTimesWarning <= 10) {
                double[] openInterval = openingIntervalCalculator.getOpeningInterval(lastActivity);
                if (openInterval[0] >= 0 || openInterval[1] >= 0){
                    log.warn("There are opening or closing times defined for the first and last activity. The correctness of the scoring function can thus not be guaranteed.");
                    log.warn("first activity: " + firstActivity ) ;
                    log.warn("last activity: " + lastActivity ) ;
                    if (firstLastActOpeningTimesWarning == 10) {
                        log.warn("Additional warnings of this type are suppressed.");
                    }
                    firstLastActOpeningTimesWarning++;
                }
            }

            double calcActScore = calcActScore(this.currentActivityStartTime, this.firstActivityEndTime + 24*3600, lastActivity);
            this.score += calcActScore; // SCENARIO_DURATION
        } else {
            // the first Act and the last Act have NOT the same type:
            if (this.params.scoreActs) {
                if (firstLastActWarning <= 5) {
                    log.warn("The first and the last activity do not have the same type. "
                            + "Will score the first activity from midnight to its end, and the last activity from its start "
                            + "to midnight.  Because of the nonlinear function, this is not the same as scoring from start to end.");
                    log.warn("first activity: " + firstActivity ) ;
                    log.warn("last activity: " + lastActivity ) ;
                    log.warn("This may also happen when plans are not completed when the simulation ends.") ;
                    if (firstLastActWarning == 5) {
                        log.warn("Additional warnings of this type are suppressed.");
                    }
                    firstLastActWarning++;
                }

                // score first activity
                this.score += calcActScore(0.0, this.firstActivityEndTime, firstActivity);
                // score last activity
                this.score += calcActScore(this.currentActivityStartTime, this.params.simulationPeriodInDays * 24*3600, lastActivity);
            }
        }

    }

    private void handleMorningActivity() {
        assert firstActivity != null;
        // score first activity
        this.score += calcActScore(0.0, this.firstActivityEndTime, firstActivity);
    }

    @Override
    public void handleFirstActivity(Activity act) {
        assert act != null;
        this.firstActivityEndTime = act.getEndTime();
        this.firstActivity = act;
    }

    @Override
    public void handleActivity(Activity act) {
        this.score += calcActScore(act.getStartTime(), act.getEndTime(), act);
    }

    @Override
    public void handleLastActivity(Activity act) {
        this.currentActivityStartTime = act.getStartTime();
        this.handleOvernightActivity(act);
        this.firstActivity = null;
    }

}
