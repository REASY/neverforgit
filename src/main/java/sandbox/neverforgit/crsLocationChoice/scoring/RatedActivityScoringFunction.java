package sandbox.neverforgit.crsLocationChoice.scoring;

import org.matsim.api.core.v01.events.Event;
import org.matsim.core.scoring.SumScoringFunction;

/**
 * Created by Andrew A. Campbell on 1/26/17.
 */
public class RatedActivityScoringFunction implements SumScoringFunction.ArbitraryEventScoring {

//    public RatedActivityScoringFunction()

    @Override
    public void handleEvent(Event event) {

    }

    @Override
    public void finish() {

    }

    @Override
    public double getScore() {
        return 0;
    }
}
