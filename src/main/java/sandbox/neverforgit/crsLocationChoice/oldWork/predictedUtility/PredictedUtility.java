package sandbox.neverforgit.crsLocationChoice.oldWork.predictedUtility;

import com.google.inject.Inject;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.config.Config;
import org.matsim.core.router.TripRouter;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;
import sandbox.neverforgit.crsLocationChoice.oldWork.PredictedPlan;

/**
 * Created by Andrew A. Campbell on 1/30/17.
 */
public class PredictedUtility {

    private Person person;
    private Activity activity;
    private ActivityFacility activityFacility;

    private double expectedUtility;
    private TripRouter tripRouter;  // T\he TripRouter from the most recent Iteration.
    private PredictedPlan predPlan = new PredictedPlan();

    @Inject
    private Scenario scenario;

    public PredictedUtility(Person person, Activity activity, ActivityFacility activityFacility){
        this.person = person;
        this.activity = activity;
        this.activityFacility = activityFacility;
    }

    //TODO calculate expected predictedUtility
    public void calcExpectedUtility(){
        //#1  - Calc expected travel cost and time to activityFacility
        Config config = this.scenario.getConfig();
//        String singleActivity = CRSGlobalData.data.SINGLE_RATED_ACTIVITY;
        double homeDepTime = CRSGlobalData.data.HOME_DEP_TIME;


        //#2 - Calc expected predictedUtility of performing activity (taken from CRS)

        //#3 - Calc expected travel cost and time of coming home


    }

    public double getExpectedUtility(){
        return this.expectedUtility;
    }
}
