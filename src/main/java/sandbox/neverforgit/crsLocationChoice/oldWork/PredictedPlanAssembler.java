package sandbox.neverforgit.crsLocationChoice.oldWork;

import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.router.TripRouter;
import org.matsim.facilities.ActivityFacility;
import org.matsim.facilities.Facility;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;
import sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan.PredictedActivity;
import sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan.PredictedLeg;

import java.util.List;

/**
 * Assembles a predicted plan element by element. This version is intended for the simple simulation.
 *
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class PredictedPlanAssembler {

    private Person person;
    private Facility homeFacility;
    private ActivityFacility activityFacility;
    private String actType;
    private double homeDepTime;
    private TripRouter tripRouter;
    private PredictedPlan predPlanOutput;
    private double votTraveling;


    public PredictedPlanAssembler(Person person, String actType, Facility homeFacility, ActivityFacility activityFacility){
        this.person = person;
        this.homeFacility = homeFacility;
        this.activityFacility = activityFacility;
        this.actType = actType;
        this.homeDepTime= CRSGlobalData.data.HOME_DEP_TIME;
        this.tripRouter = CRSGlobalData.data.TRIP_ROUTER;
        this.votTraveling = CRSGlobalData.data.VOT_TRAVELING;
    }

    public PredictedPlan assembleSimplePlan(){
        // #1 - create and score first leg (home to activity)
        List<? extends PlanElement> routeList = this.tripRouter.calcRoute("car", this.homeFacility, this.activityFacility,
                this.homeDepTime, this.person);
        PredictedLeg leg = (PredictedLeg) routeList.get(0);
        double travelScore = leg.getTravelTime()*this.votTraveling;
        leg.setPredictedScore(travelScore);
        // #2 - create activity and get predicted score from the CrowdRatingSystem
        PredictedActivity act = new PredictedActivity();





        return null;
    }


}
