package sandbox.neverforgit.crsLocationChoice.oldWork;

import org.matsim.core.scoring.EventsToActivities;
import org.matsim.core.scoring.PersonExperiencedActivity;

import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;
import sandbox.neverforgit.crsLocationChoice.crowdRatingSystem.CrowdRatingSystem;

import java.util.ArrayList;

/**
 * The purpose of this class is to update the CrowdRatingSystem after each mobsim.
 *
 * Created by Andrew A. Campbell on 1/27/17.
 */
public class RatedActivityHandler implements EventsToActivities.ActivityHandler {

    public CrowdRatingSystem crs;
    private ArrayList<String> ratedActivities;


    public RatedActivityHandler(){
        this.ratedActivities = CRSGlobalData.data.RATED_ACTIVITIES;
        this.crs = CRSGlobalData.data.CROWD_RATING_SYSTEM;
    }

    @Override
    public void handleActivity(PersonExperiencedActivity activity) {
        if (!this.ratedActivities.contains(activity.getActivity().getType())){
            return;  //not a rated activity, do nothing
        }
        //TODO - calc the score using RatedCharyparNagelActivityScoring, which should be stored in the CRSGLobalData?

    }
}
