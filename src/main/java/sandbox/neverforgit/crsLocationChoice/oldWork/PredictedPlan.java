package sandbox.neverforgit.crsLocationChoice.oldWork;

import org.matsim.api.core.v01.population.*;
import org.matsim.utils.objectattributes.attributable.Attributes;

import java.util.List;
import java.util.Map;

/**
 *
 * Used building Predicted Plans when agents are replanning, consulting the TripRouter and CrowdRatingSytem.
 * The main difference is that the score is predicted based on the CrowdRatingSystem scores for each ActivityFacility.
 *
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class PredictedPlan implements Plan {


    @Override
    public List<PlanElement> getPlanElements() {
        return null;
    }

    @Override
    public void addLeg(Leg leg) {

    }

    @Override
    public void addActivity(Activity act) {

    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public void setType(String type) {

    }

    @Override
    public Person getPerson() {
        return null;
    }

    @Override
    public void setPerson(Person person) {

    }

    @Override
    public Map<String, Object> getCustomAttributes() {
        return null;
    }

    @Override
    public void setScore(Double score) {

    }

    @Override
    public Double getScore() {
        return null;
    }

    @Override
    public Attributes getAttributes() {
        return null;
    }
}
