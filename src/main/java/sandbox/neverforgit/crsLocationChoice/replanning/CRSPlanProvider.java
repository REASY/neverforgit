package sandbox.neverforgit.crsLocationChoice.replanning;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.replanning.PlanStrategy;
import org.matsim.core.replanning.PlanStrategyImpl;
import org.matsim.core.router.TripRouter;


/**
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class CRSPlanProvider implements Provider<PlanStrategy> {
    @Inject
    Network network;

    @Inject
    Population population;

    @Inject
    EventsManager eventsManager;

    @Inject
    private Scenario scenario;  // overall scenario

    @Inject
    private TripRouter tripRouter;

    @Override
    public PlanStrategy get() {
        PlanStrategyImpl.Builder builder = new PlanStrategyImpl.Builder(new CRSPlanCreatorSimple(scenario, tripRouter));
        return builder.build();
    }
}
