package sandbox.neverforgit.crsLocationChoice.replanning;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.*;
import org.matsim.core.replanning.selectors.PlanSelector;
import org.matsim.core.router.TripRouter;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;
import sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan.PredictedPlanAssembler;
import sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan.PredictedPlan;

import java.util.ArrayList;

/**
 * This class is intended to be used with the simple toy simulation. In this world every agent has a 3-activity plan
 * home, rated activity, home.
 *
 * This class overrides the intended behavior of the PlanSelector. It does not choose from one of the existing plans,
 * instead it generates a new 3-step plan using the updated CrowdRatingSystem and TripRouter to choose the best
 * location.
 *
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class CRSPlanCreatorSimple implements PlanSelector{

    private ActivityFacilities activityFacilities;
    private SelectionAlgorithm selectionAlg = new SelectionAlgorithm.MNLDraw();  // default is MNLDraw
    private TripRouter tripRouter;
    private Scenario scenario;

    public CRSPlanCreatorSimple(Scenario scenario, TripRouter tripRouter){
//        this.activityFacilities = CRSGlobalData.data.ACTIVITY_FACILITIES;
        this.activityFacilities = scenario.getActivityFacilities();
        this.tripRouter = tripRouter;
        this.scenario = scenario;

    }

    public void setSelectionAlg(SelectionAlgorithm alg){
        this.selectionAlg = alg;
    }

    @Override
    public BasicPlan selectPlan(HasPlansAndId member) {
        // #1 - Generate a new set of predicted plans for all rated activity locations.
        ArrayList<PredictedPlan> predictedPlans = new ArrayList<>();
        PredictedPlanAssembler predictedPlanAssembler = new PredictedPlanAssembler(tripRouter);
        int i = 0;
        for (ActivityFacility aF : this.activityFacilities.getFacilities().values()){
            // Only evaluate facility if the rated activity is performed there
            if (aF.getActivityOptions().keySet().contains(CRSGlobalData.data.SINGLE_RATED_ACTIVITY_TYPE)) {
                Plan firstPlan = (Plan) member.getPlans().get(0);
                ActivityFacility homeFacility = this.getHomeFacility(firstPlan);
                PredictedPlan pP = predictedPlanAssembler.assembleSimplePlan(firstPlan.getPerson(), homeFacility, aF);
                predictedPlans.add(pP);
                i++;
            }
        }
        // #2 - Select one of them using the SelectionAlgorithms
        Plan selectedPlan = this.selectionAlg.selectPlan(predictedPlans);
        member.getPlans().add(selectedPlan);
        member.setSelectedPlan(selectedPlan);
        return selectedPlan;
    }

    /**
     * Helper method to get the home ActivityFacility.
     * @param plan
     * @return
     */
    private ActivityFacility getHomeFacility(Plan plan){
        for (PlanElement pe : plan.getPlanElements()){
            if (pe instanceof Activity){
                Activity act = (Activity) pe;
                if (act.getType().equals(CRSGlobalData.data.HOME_ACTIVITY_TYPE)){
                    return this.activityFacilities.getFacilities().get(act.getFacilityId());
                }
            }
        }
        return null;
    }
}
