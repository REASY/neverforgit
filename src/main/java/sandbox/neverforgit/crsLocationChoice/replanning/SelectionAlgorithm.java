package sandbox.neverforgit.crsLocationChoice.replanning;

import org.matsim.api.core.v01.population.Plan;
import sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan.PredictedPlan;

import java.util.ArrayList;
import java.util.Random;

/**
 * Defines the methods for selecting from the predicted plans. Mimics the default selectors: i.e. MNL draw,
 * highest predicted predictedUtility...
 * <p>
 * Created by Andrew A. Campbell on 2/1/17.
 */
public interface SelectionAlgorithm {


    /**
     * Returns a plan using one of the implementing algorithms.
     *
     * @param plans An array of predicted plans for a single person.
     * @return
     */
    Plan selectPlan(ArrayList<PredictedPlan> plans);

    /**
     * Takes Multinomial draw from the predicted plans.
     */
    class MNLDraw implements SelectionAlgorithm {
        private static Random rnd = new Random();

        @Override
        public Plan selectPlan(ArrayList<PredictedPlan> plans) {
            // Get the scores for plans
            Double[] allScores = new Double[plans.size()];
            int i = 0;
            for (PredictedPlan p : plans) {
                allScores[i] = p.getScore();
                i++;
            }

            // Calc denominator for MNL
            Double denom = 0.0;
            for (Double score : allScores) {
                denom += Math.exp(score);
            }

            // Calc the MNL probability for each plan
            Double[] probs = new Double[plans.size()];
            i = 0;
            for (PredictedPlan p : plans) {
                Double prob = Math.exp(p.getScore()) / denom;
                probs[i]=prob;
                i++;
            }

            // Make the CDF
            Double[] cdf = new Double[probs.length];
            cdf[0] = probs[0];
            for (int j = 1; j < plans.size(); j++) {
                cdf[j] = cdf[j - 1] + probs[j];
            }
            double rndDraw = rnd.nextDouble();
            int drawIdx = this.getIndex(cdf, rndDraw);
            return plans.get(drawIdx).getPlan();
        }

        /**
         * Utility for finding the index of the first probability that is greater than the input double. This is how
         * we take a draw from the empirical cdf.
         * @param cdf Empirical cdf
         * @param rnd A random uniform draw from 0-1.
         * @return
         */
        private int getIndex(Double[] cdf, double rnd) {
            for (int j = 0; j < cdf.length; j++) {
                if (rnd <= cdf[j]) {
                    return j;
                }
            }
            return -1; //
        }

    }

    /**
     * Randomly selects one of the predicted plans.
     */
    class RandomDraw implements SelectionAlgorithm {
        private static Random rnd = new Random();

        @Override
        public Plan selectPlan(ArrayList<PredictedPlan> plans) {
            int rndIdx = this.rnd.nextInt(plans.size());
            return plans.get(rndIdx).getPlan();
        }
    }

    /**
     * Selects the predicted plan with the highest score.
     */
    class HighestScore implements SelectionAlgorithm {

        @Override
        public Plan selectPlan(ArrayList<PredictedPlan> plans) {
            int idx = 0;
            Double bestScore = plans.get(idx).getScore();
            int j;
            for (j = 1; j < plans.size(); j++) {
                if (plans.get(j).getScore() > bestScore) {
                    bestScore = plans.get(j).getScore();
                    idx = j;
                }
            }
            return plans.get(j).getPlan();
        }
    }
}
