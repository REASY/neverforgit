package sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan;

import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;

import java.util.HashMap;

/**
 *
 * Data container used when building predicted plans during Replanning. Usefulness is that it stores the scores of
 * individual PlanElements. This should be good for visualizing results (AAC 17/02/03).
 *
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class PredictedPlan {

    private Plan plan;
    private HashMap<PlanElement, Double> elementScores;
    private double score = 0.0;

    public PredictedPlan(Plan plan, HashMap<PlanElement, Double> elementScores){
        this.plan = plan;
        this.elementScores = elementScores;
        for (Double score : this.elementScores.values()){
            this.score += score;
        }
    }

    public Plan getPlan(){
        return this.plan;
    }

    public HashMap<PlanElement, Double> getElementScores(){
        return this.elementScores;
    }

    public double getScore(){
        return this.score;
    }

}
