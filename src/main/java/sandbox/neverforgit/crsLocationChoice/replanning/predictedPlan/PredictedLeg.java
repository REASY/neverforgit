package sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan;

import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Route;
import org.matsim.utils.objectattributes.attributable.Attributes;

/**
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class PredictedLeg implements Leg {

    private double predictedScore;

    @Override
    public String getMode() {
        return null;
    }

    @Override
    public void setMode(String mode) {

    }

    @Override
    public Route getRoute() {
        return null;
    }

    @Override
    public void setRoute(Route route) {

    }

    @Override
    public double getDepartureTime() {
        return 0;
    }

    @Override
    public void setDepartureTime(double seconds) {

    }

    @Override
    public double getTravelTime() {
        return 0;
    }

    @Override
    public void setTravelTime(double seconds) {

    }

    @Override
    public Attributes getAttributes() {
        return null;
    }

    public void setPredictedScore(double score){
        this.predictedScore = score;
    }

    public double getPredictedScore(){
        return this.predictedScore;
    }
}
