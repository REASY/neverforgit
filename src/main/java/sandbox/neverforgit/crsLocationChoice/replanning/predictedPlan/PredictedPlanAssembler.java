package sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.population.*;
import org.matsim.core.population.PopulationUtils;
import org.matsim.core.router.TripRouter;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;
import sandbox.neverforgit.crsLocationChoice.crowdRatingSystem.CrowdRatingSystem;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Assembles a predicted plan element by element. This version is intended for the simple simulation. I used PedictedPlan_V2
 * instead of the native Plan because I think it may be useful down the line for results visualization to have
 * access to the PlanElement predicted scores (AAC 17/02/03).
 *
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class PredictedPlanAssembler {

    private TripRouter tripRouter;
    private double votTraveling;
    private CrowdRatingSystem crs;
    private String homeActivityType;
    private double homeDepTime;
    private String ratedActivityType;
    private double ratedActivityDuration;
    private double replanningCoeff = CRSGlobalData.data.REPLANNING_COEFFICIENT;

    public PredictedPlanAssembler(TripRouter tripRouter){
//        Injector injector = org.matsim.core.controler.Injector.createInjector(ConfigUtils.createConfig(), new TripRouterModule());
//
//        this.tripRouter = injector.getInstance(TripRouter.class);
        this.tripRouter = tripRouter;
        this.votTraveling = CRSGlobalData.data.VOT_TRAVELING;
        this.crs = CRSGlobalData.data.CROWD_RATING_SYSTEM;
        this.homeActivityType = CRSGlobalData.data.HOME_ACTIVITY_TYPE;
        this.homeDepTime = CRSGlobalData.data.HOME_DEP_TIME;
        this.ratedActivityType = CRSGlobalData.data.SINGLE_RATED_ACTIVITY_TYPE;
        this.ratedActivityDuration = CRSGlobalData.data.RATED_ACTIVITY_DURATION;

////        this.votTraveling = Double.valueOf(this.scenario.getConfig().planCalcScore().getMarginalUtility)
//        PlanCalcScoreConfigGroup pcs = this.scenario.getConfig().planCalcScore();
//        this. votTraveling = pcs.getModes().get(TransportMode.car).getMarginalUtilityOfTraveling();

    }

//    public PredictedPlanAssembler_V2(){
//        this.tripRouter = CRSGlobalData.data.TRIP_ROUTER;
//        this.votTraveling = CRSGlobalData.data.VOT_TRAVELING;
//        this.crs = CRSGlobalData.data.CROWD_RATING_SYSTEM;
//        this.homeActivityType = CRSGlobalData.data.HOME_ACTIVITY_TYPE;
//        this.homeDepTime = CRSGlobalData.data.HOME_DEP_TIME;
//        this.ratedActivityType = CRSGlobalData.data.SINGLE_RATED_ACTIVITY_TYPE;
//        this.ratedActivityDuration = CRSGlobalData.data.RATED_ACTIVITY_DURATION;
//    }

    public PredictedPlan assembleSimplePlan(Person person, ActivityFacility homeFacility, ActivityFacility ratedActivityFacility){
        LinkedHashMap<PlanElement, Double> elementScores = new LinkedHashMap<>();  // use linked to preserve order

        ////
        // #1 - create starting home activity
        ////
        Coord homeCoord = homeFacility.getCoord();
        Activity homeAct = PopulationUtils.createActivityFromCoord(this.homeActivityType, homeCoord);
        homeAct.setEndTime(this.homeDepTime);
        homeAct.setFacilityId(homeFacility.getId());  //Will our facilities automatically be mapped to links?
        homeAct.setLinkId(homeFacility.getLinkId());
        double homeScore = 0; //TODO is this the right way to do this?
        elementScores.put(homeAct, homeScore);

        ////
        // #2 - create and score first leg (home to activity)
        ////
        List<? extends PlanElement> routeListTo = this.tripRouter.calcRoute("car", homeFacility,
                ratedActivityFacility, this.homeDepTime, person);
        Leg legTo = (Leg) routeListTo.get(0);
        // calculate the travel score, scaled by the replanning coefficient
        double travelScoreTo = (1 - this.replanningCoeff) * legTo.getTravelTime()*this.votTraveling;
        elementScores.put(legTo, travelScoreTo);

        ////
        // #3 - create activity and get predicted score from the CrowdRatingSystem
        ////
        Coord ratedActivityCoord = ratedActivityFacility.getCoord();
        Activity ratedActivity = PopulationUtils.createActivityFromCoord(this.ratedActivityType, ratedActivityCoord);
        double rAStartTime = this.homeDepTime + legTo.getTravelTime();
        ratedActivity.setStartTime(rAStartTime);
        //TODO - this is a very naive way of setting the end time.
        ratedActivity.setEndTime(rAStartTime + this.ratedActivityDuration);
        ratedActivity.setFacilityId(ratedActivityFacility.getId());
        // Predicted activity score
        double predictedActScore = this.replanningCoeff * this.crs.getAverageRating(ratedActivityFacility.getId());
        elementScores.put(ratedActivity, predictedActScore);

        ////
        // #4 - create second leg
        ////
        List<? extends PlanElement> routeListFrom = this.tripRouter.calcRoute("car", ratedActivityFacility,
                homeFacility, this.homeDepTime, person);
        Leg legFrom = (Leg) routeListFrom.get(0);
        double travelScoreFrom = (this.replanningCoeff) * legFrom.getTravelTime()*this.votTraveling;
        elementScores.put(legFrom, travelScoreFrom);

        ////
        // #4 - create final home activity
        ////
        Activity finalHomeActivity = PopulationUtils.createActivityFromCoord(this.homeActivityType, homeCoord);
        elementScores.put(finalHomeActivity, 0.0);

        ////
        // #5 - assemble into plan
        ////
        Plan plan = PopulationUtils.createPlan(person);
        plan.addActivity(homeAct);
        plan.addLeg(legTo);
        plan.addActivity(ratedActivity);
        plan.addLeg(legFrom);
        plan.addActivity(homeAct);

        ////
        // #6 - initialize and return PredictePlan
        ////
        PredictedPlan predictedPlan = new PredictedPlan(plan, elementScores);
        return predictedPlan;
    }
}
