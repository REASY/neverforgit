package sandbox.neverforgit.crsLocationChoice.replanning.predictedPlan;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.core.utils.misc.Time;
import org.matsim.facilities.ActivityFacility;
import org.matsim.utils.objectattributes.attributable.Attributes;

/**
 * Created by Andrew A. Campbell on 2/1/17.
 */
public class PredictedActivity implements Activity {

    private double endTime = Time.UNDEFINED_TIME;

    /**
     * Used for reporting outcomes in the scoring. Not interpreted for the demand.
     */
    private double startTime = Time.UNDEFINED_TIME;

    private double dur = Time.UNDEFINED_TIME;

    private String type;
    private Coord coord = null;
    private Id<Link> linkId = null;
    private Id<ActivityFacility> facilityId = null;

    private double predictedScore;

    @Override
    public double getEndTime() {
        return 0;
    }

    @Override
    public void setEndTime(double seconds) {

    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public void setType(String type) {

    }

    @Override
    public Coord getCoord() {
        return null;
    }

    @Override
    public double getStartTime() {
        return 0;
    }

    @Override
    public void setStartTime(double seconds) {

    }

    @Override
    public double getMaximumDuration() {
        return 0;
    }

    @Override
    public void setMaximumDuration(double seconds) {

    }

    @Override
    public Id<Link> getLinkId() {
        return null;
    }

    @Override
    public Id<ActivityFacility> getFacilityId() {
        return null;
    }

    @Override
    public void setLinkId(Id<Link> id) {

    }

    @Override
    public void setFacilityId(Id<ActivityFacility> id) {
        this.facilityId = facilityId;
    }

    @Override
    public void setCoord(Coord coord) {

    }

    @Override
    public Attributes getAttributes() {
        return null;
    }
}
