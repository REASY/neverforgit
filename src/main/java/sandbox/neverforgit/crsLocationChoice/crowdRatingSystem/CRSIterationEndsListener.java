package sandbox.neverforgit.crsLocationChoice.crowdRatingSystem;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.scoring.ExperiencedPlansService;

import java.util.ArrayList;
import java.util.Map;

import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;

/**
 * Created by Andrew A. Campbell on 1/27/17.
 */
public class CRSIterationEndsListener implements IterationEndsListener {

    private CrowdRatingSystem crs;
    private ArrayList<String> ratedActivities;


    public CRSIterationEndsListener(){
        this.crs = CRSGlobalData.data.CROWD_RATING_SYSTEM;
        this.ratedActivities = CRSGlobalData.data.RATED_ACTIVITIES;
    }

    /**
     * This has two jobs: 1) Update the CrowdRatingSystem with experienced predictedUtility ratings. 2) Put the most recent
     * TripRouter in the CRSGLobalData so we can access it during replanning.
     * @param event
     */
    @Override
    public void notifyIterationEnds(IterationEndsEvent event) {
        ExperiencedPlansService expPS = event.getServices().getInjector().getProvider(ExperiencedPlansService.class).get();
        Map<Id<Person>, Plan> experiencedPlans = expPS.getExperiencedPlans();
        for (Id<Person> id : experiencedPlans.keySet()){
            Plan plan = experiencedPlans.get(id);
            for (PlanElement pe : plan.getPlanElements()){
                if (pe instanceof Activity &&  this.ratedActivities.contains(((Activity) pe).getType())){
                    Activity act = (Activity) pe;
                    Id<ActivityFacility> fId = act.getFacilityId();
                    Double startTime = act.getStartTime();
                    Double score = CRSGlobalData.data.RATED_ACTIVITY_SCORES.get(fId).get(startTime);
                    this.crs.putRating(fId, score);
                }
            }
        }
        CRSGlobalData.data.resetRatedActivities();
    }
}
