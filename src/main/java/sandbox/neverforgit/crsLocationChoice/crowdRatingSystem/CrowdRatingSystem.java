package sandbox.neverforgit.crsLocationChoice.crowdRatingSystem;

import org.matsim.api.core.v01.Id;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Andrew A. Campbell on 1/25/17.
 */
public class CrowdRatingSystem {

    private HashMap<Id<ActivityFacility>, ArrayList<Double>> ratings = new HashMap<>();
    private HashMap<Id<ActivityFacility>, Double>  avgRatings = new HashMap<>();

    //TODO - think about also storing the activity type
    public CrowdRatingSystem(ActivityFacilities activityFacilities){
        for (Id<ActivityFacility> id : activityFacilities.getFacilities().keySet()) {
            this.ratings.put(id, new ArrayList<Double>());
            this.avgRatings.put(id, 0.0);
        }
    }

    public Double getAverageRating(Id<ActivityFacility> id){
        return this.avgRatings.get(id);
    }

    public ArrayList<Double> getRatings(Id<ActivityFacility> id){
        return this.ratings.get(id);
    }

    /**
     * Adds a new rating to the ratings ArrayList. Update the averageRating for that facility.
     * @param id
     * @param rating
     */
    public void putRating(Id<ActivityFacility> id, Double rating){
        this.ratings.get(id).add(rating);
        this.avgRatings.put(id, this.calcMean(this.ratings.get(id)));
    }

    private Double calcMean(ArrayList<Double> ratings){
        Double sum = 0.0;
        for (Double d : ratings){
            sum += d;
        }
        return sum / ratings.size();
    }

}
