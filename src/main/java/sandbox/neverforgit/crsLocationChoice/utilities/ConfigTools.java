package sandbox.neverforgit.crsLocationChoice.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Andrew A. Campbell on 2/6/17.
 */
public class ConfigTools {

    /**
     * Parses a row and column config file. Assumes all rows contain data. There is no header.
     * @param path
     * @return
     * @throws IOException
     */
    static public HashMap parseConfigNoHeader(String path, String sep) throws IOException {
        HashMap<String, String> outMap = new HashMap<>();
        BufferedReader br = new BufferedReader(new FileReader(path));
        String line;

        while( (line=br.readLine())!=null){
            String str[] = line.split(sep);
            outMap.put(str[0], str[1]);
        }
        return outMap;
    }

    /**
     * Parses a row and column config file. Option to skip first nRows.
     * @param path
     * @param nRows
     * @return
     * @throws IOException
     */
    static public HashMap parseConfigWithHeader(String path, String sep, int nRows) throws IOException {
        HashMap<String, String> outMap = new HashMap<>();
        BufferedReader br = new BufferedReader(new FileReader(path));
        String line;

        // burn of nRows
        for (int i=0; i < nRows; i++){
            br.read();
        }

        while( (line=br.readLine())!=null){
            String str[] = line.split(sep);
            outMap.put(str[0], str[1]);
        }
        return outMap;
    }
}
