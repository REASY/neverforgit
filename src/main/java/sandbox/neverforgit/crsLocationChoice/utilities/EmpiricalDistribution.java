package sandbox.neverforgit.crsLocationChoice.utilities;

import java.util.Random;

/**
 * Created by Andrew A. Campbell on 2/7/17.
 */
public class EmpiricalDistribution {

    public EmpiricalDistribution(){
    }

    public static int sampleIndex(Double[] probabilities){
        Random rnd = new Random();
        return getIndex(makeLookupCDF(probabilities), rnd.nextInt());

    }

    /**
     * Since we just interested in taking random samples, we don't need the inverse CDF to be sorted. We just
     * want to sample indices.
     * @return
     */
    public static Double[] makeLookupCDF(Double[] probabilities){
        Double[] cdf = new Double[probabilities.length];
        cdf[0] = probabilities[0];
        for (int j = 1; j < probabilities.length; j++) {
            cdf[j] = cdf[j - 1] + probabilities[j];
        }
        return cdf;
    }

    /**
     * Utility for finding the index of the first probability that is greater than the input double. This is how
     * we take a draw from the empirical cdf.
     * @param cdf Empirical cdf
     * @param rnd A random uniform draw from 0-1.
     * @return
     */
    private static int getIndex(Double[] cdf, double rnd) {
        for (int j = 0; j < cdf.length; j++) {
            if (rnd <= cdf[j]) {
                return j;
            }
        }
        return -1; //
    }


}
