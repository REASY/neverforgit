package sandbox.neverforgit.crsLocationChoice.events;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.Event;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.api.internal.HasPersonId;
import org.matsim.facilities.ActivityFacility;

/**
 * Created by Andrew A. Campbell on 1/25/17.
 */
public class RatedActivityEvent extends Event implements HasPersonId {

    private Id<Person> personId;
    private Id<ActivityFacility> facilityId;
    private double startTime;
    private double duration;

    public final static String ATTRIBUTE_FACILITYID = "facility_id";

    public RatedActivityEvent(double startTime, Id<Person> personId, Id<ActivityFacility> facId, double duration){
        super(startTime);
        this.startTime = startTime;
        this.personId = personId;
        this.facilityId = facId;
        this.duration = duration;
    }

    @Override
    public String getEventType() {
        return "RatedActivityEvent";
    }

    @Override
    public Id<Person> getPersonId() {
        return this.personId;
    }

    public Id<ActivityFacility> getActivityFacilityId(){
        return this.facilityId;
    }

    public double getStartTime(){
        return this.startTime;
    }

    public double getDuration(){
        return this.duration;
    }
}
