package sandbox.neverforgit.crsLocationChoice.events;

import com.google.inject.Inject;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.ActivityEndEvent;
import org.matsim.api.core.v01.events.ActivityStartEvent;
import org.matsim.api.core.v01.events.handler.ActivityEndEventHandler;
import org.matsim.api.core.v01.events.handler.ActivityStartEventHandler;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.api.experimental.events.EventsManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Andrew A. Campbell on 1/26/17.
 */
public class RatedActivityEventHandler implements ActivityStartEventHandler, ActivityEndEventHandler {

    private EventsManager eventsManager;
    private ArrayList<String> ratedActivities;
    private HashMap<Id<Person>, Double> startTimes = new HashMap<>();

    @Inject
    public RatedActivityEventHandler(EventsManager eventsManager, ArrayList<String> ratedActivities){
        this.eventsManager = eventsManager;
        this.ratedActivities = ratedActivities;
    }

    /**
     * If the activity is rated, add the person and start time to the map tracking start times.
     * @param event
     */
    @Override
    public void handleEvent(ActivityStartEvent event) {
        // Is this event type rated?
        if (!this.ratedActivities.contains(event.getActType())){
            return;  // not a rated activity
        }
        // Are we already tracking a RatedActivity event for this person?
        if (this.startTimes.keySet().contains(event.getPersonId())){
            throw new AmbiguousActivityException("Already tracking activitiy for " + event.getPersonId().toString());
        }
        this.startTimes.put(event.getPersonId(), event.getTime());
    }

    @Override
    public void handleEvent(ActivityEndEvent event) {
        // Creates a RatedActivityEvent and adds to the events stream: EventsManager.process
        double duration = event.getTime() - this.startTimes.get(event.getPersonId());
        RatedActivityEvent ratedActivityEvent = new RatedActivityEvent(event.getTime(), event.getPersonId(),
                event.getFacilityId(), duration);
        eventsManager.processEvent(ratedActivityEvent);

        // Pop the person from the starTimes HashMap!!!!
        this.startTimes.remove(event.getPersonId());
    }

    @Override
    public void reset(int iteration) {
        this.startTimes = new HashMap<>();
    }

    /**
     * Hopefully this is never thrown. It is thrown if we read two ActivityStartEvent for the same Person Id. The
     * problem is that it will be impossible to calculate durations if we don't get start-end pairs.
     */
    public class AmbiguousActivityException extends RuntimeException {

        public AmbiguousActivityException(String message){
            super(message);
        }
    }
}
