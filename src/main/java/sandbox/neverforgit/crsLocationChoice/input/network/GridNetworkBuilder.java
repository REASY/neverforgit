package sandbox.neverforgit.crsLocationChoice.input.network;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.Node;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrew A. Campbell on 4/7/15.
 * Creates and m x n Manhattan grid network for simple simulations.
 */

public class GridNetworkBuilder {
    private Network net;
    // Network link attributes
    private double blockLength; // [meters]
    private Set<String> modes = new HashSet<String>();
    private double capacity;
    private double freeSpeed; //[meters/sec]
    private int lanes;  // number of lanes, same for all roads and directions

    // Constructor
    public GridNetworkBuilder() {
        // initialize empty network
        this.net = ScenarioUtils.createScenario(ConfigUtils.createConfig()).getNetwork();
    }

    // Getters
    public Network getNetwork(){
        return this.net;
    }

    // Setters
    public void setBlockLength(double blockLength){
        this.blockLength = blockLength;
    }

    public void addMode(String mode){
        this.modes.add(mode);
    }

    public void setCapacity(double capacity){
        this.capacity = capacity;
    }

    public void setFreeSpeed(double freeSpeed){
        this.freeSpeed = freeSpeed;
    }

    public void setLanes(int lanes){
        this.lanes = lanes;
    }


    /**
     * This does the work of building the grid network.
     * @param m Number of "rows", i.e. horizontal east-west links
     * @param n Number of "columns", i.e. vertical east-west links
     */
    public void buildNetwork(int m, int n) {
        // Create the nodes
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                Id<Node> nodeId = Id.create(Integer.toString(i) + ',' + Integer.toString(j), Node.class);
                Coord nodeCoord = new Coord(i * blockLength, j * blockLength);
                Node node = this.net.getFactory().createNode(nodeId, nodeCoord);
                this.net.addNode(node);
            }
        }

        // Create vertical links
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < m - 1; i++) {
                Id<Node> nodeIdBottom = Id.create(Integer.toString(i) + ',' + Integer.toString(j), Node.class);
                Id<Node> nodeIdTop = Id.create(Integer.toString(i + 1) + ',' + Integer.toString(j), Node.class);
                Id<Link> linkIdUp = Id.create( Integer.toString(i) + ',' + Integer.toString(j) + "->"
                        + Integer.toString(i + 1) + ',' + Integer.toString(j), Link.class);
                Id<Link> linkIdDown = Id.create(Integer.toString(i + 1) + ',' + Integer.toString(j) + "->"
                        + Integer.toString(i) + ',' + Integer.toString(j), Link.class);
                Link linkUp = this.net.getFactory().createLink(linkIdUp, this.net.getNodes().get(nodeIdBottom),
                        this.net.getNodes().get(nodeIdTop));
                Link linkDown = this.net.getFactory().createLink(linkIdDown, this.net.getNodes().get(nodeIdTop),
                        this.net.getNodes().get(nodeIdBottom));
                SetLinkAttributes(linkUp);
                SetLinkAttributes(linkDown);
                this.net.addLink(linkUp);
                this.net.addLink(linkDown);

            }
        }
        // Create horizontal links
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n - 1; j++) {
                Id<Node> nodeIdLeft = Id.create(Integer.toString(i) + ',' + Integer.toString(j), Node.class);
                Id<Node> nodeIdRight = Id.create(Integer.toString(i) + ',' + Integer.toString(j+1), Node.class);
                Id<Link> linkIdLeft = Id.create(Integer.toString(i) + ',' + Integer.toString(j) + "->"
                        + Integer.toString(i) + "," + Integer.toString(j+1), Link.class);
                Id<Link> linkIdRight = Id.create(Integer.toString(i) + "," + Integer.toString(j+1) + "->"
                        + Integer.toString(i) + "," + Integer.toString(j), Link.class);
                Link linkLeft = this.net.getFactory().createLink(linkIdLeft, this.net.getNodes().get(nodeIdLeft),
                        this.net.getNodes().get(nodeIdRight));
                Link linkRight = this.net.getFactory().createLink(linkIdRight, this.net.getNodes().get(nodeIdRight),
                        this.net.getNodes().get(nodeIdLeft));
                SetLinkAttributes(linkLeft);
                SetLinkAttributes(linkRight);
                this.net.addLink(linkLeft);
                this.net.addLink(linkRight);
            }
        }
    }

    private void SetLinkAttributes(Link link){
        link.setAllowedModes(this.modes);
        link.setCapacity(this.capacity);
        link.setFreespeed(this.freeSpeed);
        link.setLength(this.blockLength);
        link.setNumberOfLanes(this.lanes);
    }
}
