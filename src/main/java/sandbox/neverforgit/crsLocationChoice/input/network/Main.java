package sandbox.neverforgit.crsLocationChoice.input.network;

import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.NetworkWriter;
import org.matsim.core.network.algorithms.NetworkCleaner;
import sandbox.neverforgit.crsLocationChoice.utilities.ConfigTools;

import java.io.IOException;
import java.util.HashMap;

/**
 * System input:
 *
 * 0 - Path to config.csv file
 * 1 - Path to network output
 *
 * Created by Andrew A. Campbell on 2/6/2017.
 */


public class Main {

    public static void main(String[] args) throws IOException {
        HashMap<String, String> confMap = ConfigTools.parseConfigNoHeader(args[0], ",");


        // Run GridNetworkBuilder
        GridNetworkBuilder netBuilder = new GridNetworkBuilder();
        netBuilder.setBlockLength(Double.valueOf(confMap.get("block_length")));
        netBuilder.setCapacity(Double.valueOf(confMap.get("capacity")));
        netBuilder.setBlockLength(Double.valueOf(confMap.get("block_length")));
        netBuilder.setFreeSpeed(Double.valueOf(confMap.get("ff_speed")));
        netBuilder.setLanes(Integer.valueOf(confMap.get("n_lanes")));
        netBuilder.addMode("car");

        int nRows = Integer.valueOf(confMap.get("n_rows"));
        int nCols = Integer.valueOf(confMap.get("n_cols"));
        netBuilder.buildNetwork(nRows, nCols);

        Network net = netBuilder.getNetwork();

        // Clean network and write
        new NetworkCleaner().run(net);
        new NetworkWriter(net).write(args[1]);

    }
}
