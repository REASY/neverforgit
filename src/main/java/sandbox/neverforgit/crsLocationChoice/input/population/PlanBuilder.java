package sandbox.neverforgit.crsLocationChoice.input.population;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.population.*;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.MutableScenario;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;

import java.util.*;

/**
 * Created by Andrew A. Campbell on 4/26/15.
 */
public class PlanBuilder {
    private final static Logger log = Logger.getLogger(PlanBuilder.class);
    private MutableScenario scenario;
    private int nPopulation; // number of agents in nPopulation
    private double homeEndTime; // when the home activity ends [seconds since midnight]
    private double ratedActStartTime; // when the event starts. Same for all agents.
    private double ratedActEndTime; // when the event ends.
    private String homeActivityType = "home";
    private String ratedActivityType = "ratedActivty";

    //TODO - make one constructor that reads facilities from a file and another that takes them as parameter
    /**
     * Initializes the PlanBuilder with the given parameters and default homeActivityType and ratedActivityType.
     * @param nPopulation
     * @param homeEndTime Seconds since midnight.
     * @param ratedActStartTime Seconds since midnight.
     * @param ratedAcEndTime Seconds since midnight.
     */
    public PlanBuilder(int nPopulation, double homeEndTime, double ratedActStartTime, double ratedAcEndTime,
                       ActivityFacilities facilities) {
        this.nPopulation = nPopulation;
        this.homeEndTime = homeEndTime;
        this.ratedActStartTime = ratedActStartTime;
        this.ratedActEndTime = ratedAcEndTime;
        this.scenario = ScenarioUtils.createMutableScenario(ConfigUtils.createConfig());
        this.scenario.setActivityFacilities(facilities);
    }

    ////
    // Setters
    ////
    public void setHomeActType(String homeType){
        this.homeActivityType = homeType;
    }

    public void setRatedActType(String ratedType){
        this.ratedActivityType = ratedType;
    }

    ////
    // Getters
    ////

    /**
     * Returns the Population. Should only be run after createPlans() has been run.
     * @return
     */
    public Population makePopulation(){
        this.createPlans();
        return this.scenario.getPopulation();
    }

    private void createPlans(){
        Population population = this.scenario.getPopulation();
        PopulationFactory populationFactory = population.getFactory();

        // Create agents and assign them to home facilities. Everyone initially chooses a random destination.
        TreeMap<Id<ActivityFacility>, ActivityFacility> leisureFacs = this.scenario.getActivityFacilities().getFacilitiesForActivityType(this.ratedActivityType);
        int n = leisureFacs.keySet().size(); // number of leisure facilities
        Random rand = new Random();
        ArrayList<Id<ActivityFacility>> destIDs = new ArrayList<>(leisureFacs.keySet());
        for (int i = 0; i<this.nPopulation; i++) {
            // Create the person
            // Home facilities are created first assigned ids = [0:nPopulation). Person ID is the same as
            // their home facility ID.
            Person person = populationFactory.createPerson(Id.createPersonId(i));
            population.addPerson(person);

            // Create agent's home activity and assign facility
            ActivityFacility homeFacility = this.scenario.getActivityFacilities().getFacilities().get(Id.create(i, ActivityFacility.class));
            Activity homeActivity = populationFactory.createActivityFromCoord(this.homeActivityType, homeFacility.getCoord());
            homeActivity.setFacilityId(homeFacility.getId());
            homeActivity.setEndTime(this.homeEndTime);
            // Return home activity to end the day
            Activity homeActvitivtyEnd = populationFactory.
                    createActivityFromCoord(this.homeActivityType, homeFacility.getCoord());
            homeActvitivtyEnd.setFacilityId(homeFacility.getId());

            // Create the leisure activity and assign a random facility
            int idx = rand.nextInt(n); // random index [0 : n]
            Id destId =  destIDs.get(idx); // randomly select one leisure facility
            ActivityFacility leisureFacility = this.scenario.getActivityFacilities().getFacilities().get(destId);
            Activity leisureActivity =  populationFactory.
                    createActivityFromCoord(this.ratedActivityType, leisureFacility.getCoord());
            leisureActivity.setFacilityId(leisureFacility.getId());
            leisureActivity.setStartTime(this.ratedActStartTime);
            leisureActivity.setEndTime(this.ratedActEndTime);

            // Create plan for person and add activities
            Plan plan = populationFactory.createPlan();
            person.addPlan(plan);
            person.setSelectedPlan(plan);
            plan.addActivity(homeActivity);
            plan.addLeg(populationFactory.createLeg("car"));
            plan.addActivity(leisureActivity);
            plan.addLeg(populationFactory.createLeg("car"));
            plan.addActivity(homeActvitivtyEnd);
        }
    }

    public void write(String path){
        PopulationWriter populationWriter = new PopulationWriter(this.scenario.getPopulation(), this.scenario.getNetwork());
        populationWriter.write(path);
        log.info("Plans file creation finished #################################");
    }

}




