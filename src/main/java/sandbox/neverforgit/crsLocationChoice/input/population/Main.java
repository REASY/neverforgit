package sandbox.neverforgit.crsLocationChoice.input.population;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;
import sandbox.neverforgit.crsLocationChoice.input.facilities.FacilityBuilder;
import sandbox.neverforgit.crsLocationChoice.input.facilities.LocationMethod;
import sandbox.neverforgit.crsLocationChoice.input.network.GridNetworkBuilder;
import sandbox.neverforgit.crsLocationChoice.utilities.ConfigTools;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

/**
 * System input:
 * 0 - Path to config.csv input
 * 1 - Path to plans.xml output
 *
 * Created by Andrew A. Campbell on 02/09/17.
 */
public class Main {
    public static void main(String[] args) throws ParseException, IOException {
        // Get parameters from config.xml
        HashMap<String, String> confMap = ConfigTools.parseConfigNoHeader(args[0], ",");

        ////
        // Step 0 - Build the Network
        ////
        // Run GridNetworkBuilder
        GridNetworkBuilder netBuilder = new GridNetworkBuilder();
        netBuilder.setBlockLength(Double.valueOf(confMap.get("block_length")));
        netBuilder.setCapacity(Double.valueOf(confMap.get("capacity")));
        netBuilder.setBlockLength(Double.valueOf(confMap.get("block_length")));
        netBuilder.setFreeSpeed(Double.valueOf(confMap.get("ff_speed")));
        netBuilder.setLanes(Integer.valueOf(confMap.get("n_lanes")));
        netBuilder.addMode("car");

        int nRows = Integer.valueOf(confMap.get("n_rows"));
        int nCols = Integer.valueOf(confMap.get("n_cols"));
        netBuilder.buildNetwork(nRows, nCols);

        Network net = netBuilder.getNetwork();

        ////
        // Step 1 - Build the AcitivityFacilities
        ////
        // Configure the FacilityBuilder
        FacilityBuilder fB = new FacilityBuilder(net);
        fB.setnRows(Integer.valueOf(confMap.get("n_rows")));
        fB.setnCols(Integer.valueOf(confMap.get("n_cols")));
        fB.setBlockLength(Double.valueOf(confMap.get("block_length")));
        fB.setNPopulation(Integer.valueOf(confMap.get("n_population")));
        fB.setNActivityFacilities(Integer.valueOf(confMap.get("n_activity_facilities")));
        fB.setHomeLocMethod(LocationMethod.valueOf(confMap.get("home_loc_method")));
        fB.setDestLocMethod(LocationMethod.valueOf(confMap.get("act_loc_method")));
        fB.setActFacOpenTime(Double.valueOf(confMap.get("act_open_time")));
        fB.setActFacCloseTime(Double.valueOf(confMap.get("act_close_time")));
        fB.setHomeActType(confMap.get("home_act_type"));
        fB.setRatedActType(confMap.get("act_type"));

        // Build the facilities
        fB.makeFacilities();

        // Add the quality coefficients
        double left = Double.valueOf(confMap.get("qc_left"));
        double mode = Double.valueOf(confMap.get("qc_mode"));
        double right = Double.valueOf(confMap.get("qc_right"));
        fB.addTriangleQualityCoefficients(left, mode, right);

        ////
        // Step 2 - Run PlanBuilder
        ////

        // Configure PlanBuilder
        int nPopulation = Integer.valueOf(confMap.get("n_population"));
        double homeEndTime = Double.valueOf(confMap.get("home_end_time"));
        double ratedActStartTime = Double.valueOf(confMap.get("act_start_time"));
        double ratedActEndTime = Double.valueOf(confMap.get("act_end_time"));

        // Run PlanBuilder and write to file
        PlanBuilder pB = new PlanBuilder(nPopulation, homeEndTime, ratedActStartTime, ratedActEndTime,
                fB.getActivityFacilities());
        pB.setHomeActType(confMap.get("home_act_type"));
        pB.setRatedActType(confMap.get("act_type"));

        Population population = pB.makePopulation();
        pB.write(args[1]);

        // Test adding population to new Scenario
        Scenario scenario = ScenarioUtils.loadScenario(ConfigUtils.createConfig());
        Population scenarioPopulation = scenario.getPopulation();
        for (Person p : population.getPersons().values()){
                scenarioPopulation.addPerson(p);
        }
        for (Person p: scenarioPopulation.getPersons().values()){
            System.out.println("PersonID: " + p.getId().toString());
        }
    }
}
