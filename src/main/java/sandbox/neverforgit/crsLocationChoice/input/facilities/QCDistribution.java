package sandbox.neverforgit.crsLocationChoice.input.facilities;

import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;

import java.util.Random;

/**
 * Created by Andrew A. Campbell on 2/7/17.
 */
public interface QCDistribution {

    /**
     * Adds a custom ObjectAttribute to each facility denoting its quality coefficient.
     * 
     * @param activityFacilities
     */
    void setQualityCoefficients(ActivityFacilities activityFacilities);

    /**
     * Uses a triangle distribution to draw quality coefficients.
     */
    class Triangle implements QCDistribution{
        
        private double left;
        private double mode;
        private double right;
        private Random rnd = new Random();

        private double height; // height of pdf at mode
        
        public Triangle(double left, double mode, double right){
            this.left = left;
            this.mode = mode;
            this.right = right;
            // calc the piecewise pdf
            this.height = 2.0 / (right - left);
        }

        /**
         * Returns the probability density at point x for the initialized Triangle distribution.
         * @param x
         * @return
         */
        private double pdf(double x){
            if (x >= this.left && x <= this.mode){
                return (this.height / (this.mode - this.left))*(x - this.left);
            } else {
                return this.height - (this.height)/(this.right - this.mode)*(x - this.mode);
            }
        }

        /**
         * Returns the probability that X<=x for the initialized Triangle distribution.
         * @param x
         * @return
         */
        private double cdf(double x){
            if (x >= this.left && x <= this.mode){
                return (this.height/(this.mode - this.left))*(Math.pow(x, 2)/2 - this.left*x);
            } else {
                return this.height*x - (this.height/(this.right - this.mode))*(Math.pow(x, 2)/2 - this.mode*x);
            }
        }

        /**
         * Inverse CDF function. For cumulative probability, F, returns the value.
         * @param F
         * @return
         */
        private double inverseCDF(double F){
            if (F>=0 && F<=(this.mode - this.left)/(this.right - this.left)){
                return this.left + Math.pow((this.mode - this.left)*(this.right - this.left)*F, 0.5);
            } else {
                return this.right - Math.pow((this.mode - this.left)*(this.right - this.left)*(1-F), 0.5);
            }
        }

        
        @Override
        public void setQualityCoefficients(ActivityFacilities activityFacilities) {
            // Generate the quality coefficients, drawn triangle distribution
            double[] qcs = this.generateQualityCoefficients(activityFacilities.getFacilities().size());
            // Add quality coefficients to the ActivityFacility
            int i = 0;
            for (ActivityFacility aF : activityFacilities.getFacilities().values()){
                aF.getCustomAttributes().put("qualityCoefficient", qcs[i]);
                i++;
            }
        }

        /**
         * Draws n quality coefficients from the cdf of the initialized Triangle distribution.
         * @param n
         * @return
         */
        private double[] generateQualityCoefficients(int n){
            double[] qcs = new double[n];
            for (int i=0; i<n; i++){
                qcs[i] = this.inverseCDF(rnd.nextDouble());
            }
            return qcs;
        }
    }


}
