package sandbox.neverforgit.crsLocationChoice.input.facilities;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.facilities.*;

import java.util.Random;

/**
 * Current version generates sets of "home" and "activty" facilities on a grid network (AAC 17/02/06). Different
 * methods for generating facilities locations are available. The Poisson method produces a Uniform even distribution.
 *
 * A more generalizable version is to come.
 * Created by Andrew A. Campbell on 4/26/15.
 */
//TODO - generalize by: 1) take network as input, not nRows, nCols and blockLength. 2) Multiple non-home activity types.
public class FacilityBuilder {

    private final static Logger log = Logger.getLogger(FacilityBuilder.class);
    private Scenario scenario;
    private int nRows; // number of nRows in  road network
    private int nCols; // number of columns in road network
    private double blockLength; // length of blocks in the network
    private int nPopulation; // number of nPopulation to generate
    private int nActivityFacilities; // number of destinations to generate
    private LocationMethod homeLocMethod;
    private LocationMethod destLocMethod;
    private double actFacOpenTime; // 5pm in seconds since midnight
    private double actFacCloseTime;
    private int facId = 0; // facility ID counter
    private String homeActType;
    private String ratedActType;
    private Network network;

    public FacilityBuilder(Network network){
        this.network = network;
    }


    ////
    // Setters
    ////
    public void setnRows(int nRows) {
        this.nRows = nRows;
    }

    public void setnCols(int nCols) {
        this.nCols = nCols;
    }

    public void setBlockLength(double blockLength) {
        this.blockLength = blockLength;
    }

    public void setNPopulation(int nPopulation) {
        this.nPopulation = nPopulation;
    }

    public void setNActivityFacilities(int nActivityFacilities) {
        this.nActivityFacilities = nActivityFacilities;
    }

    public void setHomeLocMethod(LocationMethod homeLocMethod) {
        this.homeLocMethod = homeLocMethod;
    }

    public void setDestLocMethod(LocationMethod destLocMethod) {
        this.destLocMethod = destLocMethod;
    }

    public void setActFacOpenTime(double actFacOpenTime) {
        this.actFacOpenTime = actFacOpenTime;
    }

    public void setActFacCloseTime(double actFacCloseTime) {
        this.actFacCloseTime = actFacCloseTime;
    }

    public void setHomeActType(String homeActType) {
        this.homeActType = homeActType;
    }

    public void setRatedActType(String ratedActType) {
        this.ratedActType = ratedActType;
    }


    ////
    // Getters
    ////

    /**
     * Returns all ActivityFacility created. Includes "home" and all other activity types.
     * @return
     */
    public ActivityFacilities getFacilities(){
        return this.scenario.getActivityFacilities();
    }



    ////
    // Workers
    ////
    public void makeFacilities() {
        Config config = ConfigUtils.createConfig();
        this.scenario = ScenarioUtils.createScenario(config);
        this.run();
    }

    private void run() {
        switch (this.homeLocMethod) {
            case GRID:
                createHomesGRID();
                break;
            case POISSON:
                createHomesPOISSON();
                break;
            case GAUSSIAN:
                createHomeGAUSSIAN();
                break;
        }
        switch (this.destLocMethod){
            case GRID:
                createDestsGRID();
                break;
            case POISSON:
                createDestsPOISSON();
                break;
            case GAUSSIAN:
                createDestsGAUSSIAN();
                break;
        }
        this.mapFacilitiesToLinks();

    }

    private void mapFacilitiesToLinks(){
        for (ActivityFacility aF : this.scenario.getActivityFacilities().getFacilities().values()){
            Link link = NetworkUtils.getNearestLink(this.network, aF.getCoord());
            ((ActivityFacilityImpl)aF).setLinkId(link.getId());
        }
    }

    private void createHomesGRID() {

    }

    private void createHomesPOISSON(){
        // Get the width and height of the network
        double hDist = (this.nCols -1) * this.blockLength;
        double vDist = (this.nRows -1) * this.blockLength;

        for (int i = 0; i<this.nPopulation; i++){
            // Randomly draw x and y coordinates from uniform distribution
            double x = new Random().nextDouble()*hDist;
            double y = new Random().nextDouble()*vDist;
            Coord coord = new Coord(x,y);
            ActivityFacility fac = this.scenario.getActivityFacilities().
                    getFactory().createActivityFacility(Id.create(this.facId, ActivityFacility.class), coord);
            this.scenario.getActivityFacilities().addActivityFacility(fac);
            addActivityOption(fac, this.homeActType);
            this.facId++; //increment the facility ID counter
        }
    }

    private void createHomeGAUSSIAN(){

    }

    private void createDestsGRID() {

    }

    private void createDestsPOISSON(){
        // Get the width and height of the network
        double hDist = (this.nCols -1) * this.blockLength;
        double vDist = (this.nRows -1) * this.blockLength;

        for (int i = 0; i<this.nActivityFacilities; i++){
            double x = new Random().nextDouble() * hDist;
            double y = new Random().nextDouble() * vDist;
            Coord coord = new Coord(x, y);
            ActivityFacility fac = this.scenario.getActivityFacilities().
                    getFactory().createActivityFacility(Id.create(this.facId, ActivityFacility.class), coord);
            this.scenario.getActivityFacilities().addActivityFacility(fac);
            addActivityOption(fac, this.ratedActType);
            this.facId++;
        }

    }


    private void createDestsGAUSSIAN(){

    }

    public void addTriangleQualityCoefficients(double left, double mode, double right){
        QCDistribution.Triangle triDist = new QCDistribution.Triangle(left, mode, right);
        triDist.setQualityCoefficients(this.getActivityFacilities());
    }


    // based on:
    //http://sourceforge.net/p/matsim/source/HEAD/tree/playgrounds/trunk/ivtExt/src/main/java/tutorial/CreateFacilities.java#l132

    private void addActivityOption(ActivityFacility facility, String type) {
        /*
         * [[ 1 ]] Specify the opening hours here for shopping and leisure. An example is given for the activities work and home.
         */
//        ActivityOptionImpl actOption = (ActivityOptionImpl)facility.getActivityOptions().get(type);
        ActivityOptionImpl actOption = new ActivityOptionImpl(type);
        OpeningTimeImpl opentime;
        if (type.equals(this.ratedActType)) {
            opentime = new OpeningTimeImpl(this.actFacOpenTime, this.actFacCloseTime);
        }
        // home activity. Open all day
        else {
            opentime = new OpeningTimeImpl(0.0 * 3600.0, 24.0 * 3600);
        }
        actOption.addOpeningTime(opentime);
        facility.addActivityOption(actOption);
    }

    public void write(String path) {
        new FacilitiesWriter(this.scenario.getActivityFacilities()).
                write(path);
        log.info("Facilities file creation finished #################################");
    }

    public ActivityFacilities getActivityFacilities(){
        return this.scenario.getActivityFacilities();
    }


}
