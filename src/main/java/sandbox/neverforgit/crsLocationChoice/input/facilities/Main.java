package sandbox.neverforgit.crsLocationChoice.input.facilities;

import org.matsim.api.core.v01.network.Network;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.input.network.GridNetworkBuilder;
import sandbox.neverforgit.crsLocationChoice.utilities.ConfigTools;
import sandbox.neverforgit.utils.CSVWriters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * System input:
 *
 * 0 - path to config.csv
 * 1 - output path for writing the quality coefficients
 * 1 - output path for writing facilities.xml
 *
 * Created by Andrew A. Campbell on 2/6/2017.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        // Generate the map of parameters to pass to CreateFacilities
        HashMap<String, String> confMap = ConfigTools.parseConfigNoHeader(args[0], ",");

        ////
        // Step 1 - Build the Network
        ////
        GridNetworkBuilder netBuilder = new GridNetworkBuilder();
        netBuilder.setBlockLength(Double.valueOf(confMap.get("block_length")));
        netBuilder.setCapacity(Double.valueOf(confMap.get("capacity")));
        netBuilder.setBlockLength(Double.valueOf(confMap.get("block_length")));
        netBuilder.setFreeSpeed(Double.valueOf(confMap.get("ff_speed")));
        netBuilder.setLanes(Integer.valueOf(confMap.get("n_lanes")));
        netBuilder.addMode("car");

        int nRows = Integer.valueOf(confMap.get("n_rows"));
        int nCols = Integer.valueOf(confMap.get("n_cols"));
        netBuilder.buildNetwork(nRows, nCols);

        Network net = netBuilder.getNetwork();

        ////
        // Step 2 - FacilityBuilder
        ////

        // Configure the FacilityBuilder
        FacilityBuilder fB = new FacilityBuilder(net);
        fB.setnRows(Integer.valueOf(confMap.get("n_rows")));
        fB.setnCols(Integer.valueOf(confMap.get("n_cols")));
        fB.setBlockLength(Double.valueOf(confMap.get("block_length")));
        fB.setNPopulation(Integer.valueOf(confMap.get("n_population")));
        fB.setNActivityFacilities(Integer.valueOf(confMap.get("n_activity_facilities")));
        fB.setHomeLocMethod(LocationMethod.valueOf(confMap.get("home_loc_method")));
        fB.setDestLocMethod(LocationMethod.valueOf(confMap.get("act_loc_method")));
        fB.setActFacOpenTime(Double.valueOf(confMap.get("act_open_time")));
        fB.setActFacCloseTime(Double.valueOf(confMap.get("act_close_time")));
        fB.setHomeActType(confMap.get("home_act_type"));
        fB.setRatedActType(confMap.get("act_type"));

        // Build the facilities
        fB.makeFacilities();

        // Add the quality coefficients
        double left = Double.valueOf(confMap.get("qc_left"));
        double mode = Double.valueOf(confMap.get("qc_mode"));
        double right = Double.valueOf(confMap.get("qc_right"));
        fB.addTriangleQualityCoefficients(left, mode, right);

        // Write the quality coefficients to file for checking.
        ActivityFacilities activityFacilities = fB.getActivityFacilities();
        ArrayList<String> facIds = new ArrayList<>();
        ArrayList<Double> qCS = new ArrayList<>();
        for (ActivityFacility aF : activityFacilities.getFacilities().values()){
            facIds.add(aF.getId().toString());
            qCS.add((Double)aF.getCustomAttributes().get("qualityCoefficient"));
        }

        ArrayList<String> header = new ArrayList<>();
        header.add("Facility_ID");
        header.add("Quality_Coefficient");
        ArrayList<Object> values = new ArrayList<>();
        values.add(facIds);
        values.add(qCS);
        CSVWriters.writeFileGeneric(values, header, ',', args[1]);
//        CSVWriters.writeFileGeneric(values, header, ',', args[2]);

        // Write facilities to file
        fB.write(args[2]);
    }


}
