package sandbox.neverforgit.crsLocationChoice.input.facilities;

/**
 * Created by Andrew A. Campbell on 4/26/15.
 */
public enum LocationMethod {
    GRID, POISSON, GAUSSIAN
}
