/**
 * This package handles generation of the input for the simulation. That includes: Network, home Facilities, rated
 * activity Facilities, and initial Plans.
 *
 * Ideally, these input can be written to files or passed straight to the main class (AAC 17/02/03).
 *
 * Created by Andrew A. Campbell on 2/3/17.
 */
package sandbox.neverforgit.crsLocationChoice.input;