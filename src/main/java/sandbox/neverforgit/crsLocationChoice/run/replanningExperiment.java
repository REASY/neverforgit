package sandbox.neverforgit.crsLocationChoice.run;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;
import org.matsim.core.scenario.MutableScenario;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;
import sandbox.neverforgit.crsLocationChoice.CRSGlobalData;
import sandbox.neverforgit.crsLocationChoice.analysis.listeners.RatedActivityTravelListener;
import sandbox.neverforgit.crsLocationChoice.crowdRatingSystem.CrowdRatingSystem;
import sandbox.neverforgit.crsLocationChoice.input.facilities.FacilityBuilder;
import sandbox.neverforgit.crsLocationChoice.input.facilities.LocationMethod;
import sandbox.neverforgit.crsLocationChoice.input.network.GridNetworkBuilder;
import sandbox.neverforgit.crsLocationChoice.input.population.PlanBuilder;
import sandbox.neverforgit.crsLocationChoice.replanning.CRSPlanProvider;
import sandbox.neverforgit.crsLocationChoice.utilities.ConfigTools;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

/**
 * System input:
 * 0 - Path to experimental config.csv
 * 1 - Path to MATSim's config.xml
 * 2 - Path to root output directory
 * 3 - Replanning Coefficient
 *
 * Created by Andrew A. Campbell on 2/10/17.
 */
public class replanningExperiment {
    public static void main(String[] args) throws ParseException, IOException {
        double startTime = System.currentTimeMillis() / 1000.0;

        // Get parameters from config.xml
        HashMap<String, String> confMap = ConfigTools.parseConfigNoHeader(args[0], ",");

        ////
        // Step 1 - Build the Network
        ////
        // Run GridNetworkBuilder
        GridNetworkBuilder netBuilder = new GridNetworkBuilder();
        netBuilder.setBlockLength(Double.parseDouble(confMap.get("block_length")));
        netBuilder.setCapacity(Double.parseDouble(confMap.get("capacity")));
        netBuilder.setBlockLength(Double.parseDouble(confMap.get("block_length")));
        netBuilder.setFreeSpeed(Double.parseDouble(confMap.get("ff_speed")));
        netBuilder.setLanes(Integer.parseInt(confMap.get("n_lanes")));
        netBuilder.addMode("car");

        int nRows = Integer.parseInt(confMap.get("n_rows"));
        int nCols = Integer.parseInt(confMap.get("n_cols"));
        netBuilder.buildNetwork(nRows, nCols);

        Network net = netBuilder.getNetwork();

        ////
        // Step 2 - Build the AcitivityFacilities
        ////
        // Configure the FacilityBuilder
        FacilityBuilder fB = new FacilityBuilder(net);
        fB.setnRows(Integer.parseInt(confMap.get("n_rows")));
        fB.setnCols(Integer.parseInt(confMap.get("n_cols")));
        fB.setBlockLength(Double.parseDouble(confMap.get("block_length")));
        fB.setNPopulation(Integer.parseInt(confMap.get("n_population")));
        fB.setNActivityFacilities(Integer.parseInt(confMap.get("n_activity_facilities")));
        fB.setHomeLocMethod(LocationMethod.valueOf(confMap.get("home_loc_method")));
        fB.setDestLocMethod(LocationMethod.valueOf(confMap.get("act_loc_method")));
        fB.setActFacOpenTime(Double.valueOf(confMap.get("act_open_time")));
        fB.setActFacCloseTime(Double.valueOf(confMap.get("act_close_time")));
        fB.setHomeActType(confMap.get("home_act_type"));
        fB.setRatedActType(confMap.get("act_type"));

        // Build the facilities
        fB.makeFacilities();

        // Add the quality coefficients
        double left = Double.parseDouble(confMap.get("qc_left"));
        double mode = Double.parseDouble(confMap.get("qc_mode"));
        double right = Double.parseDouble(confMap.get("qc_right"));
        fB.addTriangleQualityCoefficients(left, mode, right);

        ActivityFacilities facilities = fB.getActivityFacilities();

        ////
        // Step 3 - Build the Population
        ////

        // Configure PlanBuilder
        int nPopulation = Integer.parseInt(confMap.get("n_population"));
        double homeEndTime = Double.parseDouble(confMap.get("home_end_time"));
        double ratedActStartTime = Double.parseDouble(confMap.get("act_start_time"));
        double ratedActEndTime = Double.parseDouble(confMap.get("act_end_time"));

        // Run PlanBuilder and write to file
        PlanBuilder pB = new PlanBuilder(nPopulation, homeEndTime, ratedActStartTime, ratedActEndTime,
                facilities);
        pB.setHomeActType(confMap.get("home_act_type"));
        pB.setRatedActType(confMap.get("act_type"));

        Population population = pB.makePopulation();


        ////
        // Step 4 - Load Scenario
        ////

        // load the Scenario
        String configPath = args[1];
        Config config = ConfigUtils.loadConfig(configPath);
        MutableScenario scenario = ScenarioUtils.createMutableScenario(config);
        scenario.setPopulation(population);
        scenario.setNetwork(net);
        scenario.setActivityFacilities(facilities);

        ////
        // Step 5 - Initialze CRSGlobalData and CrowdRatingSystem
        ////
        CRSGlobalData.initializer();
        CRSGlobalData data = CRSGlobalData.data;
        data.HOME_ACTIVITY_TYPE = confMap.get("home_act_type");
        data.SINGLE_RATED_ACTIVITY_TYPE = confMap.get("act_type");
        data.HOME_DEP_TIME = Double.valueOf(confMap.get("home_end_time"));
        data.RATED_ACTIVITY_DURATION = Double.valueOf(confMap.get("act_duration"));
        data.VOT_TRAVELING = (config.planCalcScore().getModes().get(TransportMode.car).getMarginalUtilityOfTraveling())/3600.0;
        data.VOT_ACTIVITY = -1*data.VOT_TRAVELING;
        data.REPLANNING_COEFFICIENT = Double.valueOf(args[3]);

        CrowdRatingSystem crs = new CrowdRatingSystem(facilities);
        data.CROWD_RATING_SYSTEM = crs;
        for (ActivityFacility fac: facilities.getFacilities().values()){
            double qc = (Double)fac.getCustomAttributes().get("qualityCoefficient");
            crs.putRating(fac.getId(), qc*data.VOT_ACTIVITY*data.RATED_ACTIVITY_DURATION); // qc * vot * typ duration
        }

        ////
        // Step 6 - Initialize Controler and run
        ////

        // create the Controler and configuer
        Controler controler = new Controler(scenario);
//        controler.getConfig().controler().setOverwriteFileSetting(OutputDirectoryHierarchy.OverwriteFileSetting.overwriteExistingFiles);
        controler.addOverridingModule(new AbstractModule() {
            @Override
            public void install() {
                addPlanStrategyBinding("CRSLocationChoice").toProvider(CRSPlanProvider.class);
            }
        });
        controler.getConfig().planCalcScore().setMemorizingExperiencedPlans(true);
        controler.getConfig().controler().setOutputDirectory(args[2]);


        // Add listeners
        RatedActivityTravelListener ratl = new RatedActivityTravelListener(args[2]);
        controler.addControlerListener(ratl);

        // run!
        controler.run();

        double endTime = System.currentTimeMillis() / 1000.00;

        System.out.println("///////////////////////////////////////////////////////////////////////////");
        System.out.println("RUNNING TIME: " + (endTime - startTime));
        System.out.println("///////////////////////////////////////////////////////////////////////////");
    }
}
