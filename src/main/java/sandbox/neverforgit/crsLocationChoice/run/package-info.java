/**
 *
 * This is the package for main() classes to do the MATSim runs. Integration testing and experimentation should be
 * done here.
 *
 * Created by Andrew A. Campbell on 2/10/17.
 */
package sandbox.neverforgit.crsLocationChoice.run;