package sandbox.neverforgit.peviRouter.serializables;

import org.matsim.core.trafficmonitoring.TravelTimeData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew A. Campbell on 7/5/16.
 */
public class SOExample {
    private Map<String, NestedClass> myMap = new HashMap<>();

    public SOExample() {};

    static class NestedClass {
        final int data;
        NestedClass(final int data) {
            this.data = data;
        }
    }

    public void populateMyMap(){
        for (int i=0; i<100;  i++){
            this.myMap.put(Integer.toString(i), new NestedClass(i));
        }
    }
}
