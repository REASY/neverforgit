package sandbox.neverforgit.peviRouter;

import sandbox.neverforgit.peviRouter.serializables.SOExample;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew A. Campbell on 7/5/16.
 */
public class SOExampleMyPackage {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        SOExample example = new SOExample();
        example.populateMyMap();

        // Make the example.myMap field accessible
        Field f = example.getClass().getDeclaredField("myMap");
        f.setAccessible(true);
        Map<String, Object> myMapHere = (Map<String, Object>) f.get(example);

        // Loop through entries and reflect out the values
        Map<String, Integer> finalMap = new HashMap<String, Integer>();
        for (String k: myMapHere.keySet()){
            Field f2 = myMapHere.get(k).getClass().getDeclaredField("data");
            f2.setAccessible(true);
            finalMap.put(k, (Integer) f2.get(myMapHere.get(k)));
        }

        // Test it all
        for (String k: finalMap.keySet()){
            System.out.println("Key: " + k + " Value: " + finalMap.get(k));
        }
    }
}
