package sandbox.neverforgit.canonicalSmartBay.src;

import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.controler.OutputDirectoryHierarchy;

/**
 *
 * A very simple Controller that implements the default MATSim traffic assignment modules.
 *
 */

public class controller {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        String configPath = args[0];
        Config config = ConfigUtils.loadConfig(configPath);
        config.controler().setOverwriteFileSetting(OutputDirectoryHierarchy.OverwriteFileSetting.deleteDirectoryIfExists);
        Controler controler = new Controler(config);
        //controler.setOverwriteFiles(true);
        controler.run();
        long endTime = System.currentTimeMillis();
        long totTime = endTime - startTime;
        System.out.println("TOTAL RUNTIME: " + String.valueOf(totTime));

    }
}