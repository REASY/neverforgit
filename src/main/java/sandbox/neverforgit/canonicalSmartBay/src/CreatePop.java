package sandbox.neverforgit.canonicalSmartBay.src;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.*;
import org.matsim.core.api.internal.MatsimWriter;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.core.utils.geometry.CoordinateTransformation;
import org.matsim.core.utils.geometry.transformations.TransformationFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

/**
 * Generates a simple MATSim plan based on the output from generate_home_work_trips.py
 * <p>
 * USAGE: You only need to define paths to input and output files.
 * <p>
 * PATHS TO DEFINE:
 * 1 - hbwtFile is the out put from generate_home_work_trips.py
 * 2 - outPlan is the output. It should be saved to the project's input folder.
 */

public class CreatePop {

    public static void main(String[] args) {

        ////
        // PATHS (from system innput)
        ////

        //Input file with PersonID and trips
        //String hbwtFile = "./input/home_work_trips_100K.csv";
        String hbwtFile = args[0];
        // Output plan
        //String outPlan = "./input/plans_SF_Bay_demo_100K.xml";
        String outPlan = args[1];

        //Create new Config
        Config config = ConfigUtils.createConfig();
        //Create new scenario and assign some components to local vars
        Scenario sc = ScenarioUtils.createScenario(config);
        Network network = sc.getNetwork();
        Population population = sc.getPopulation();
        PopulationFactory populationFactory = population.getFactory();
        //Create a population.

        //////////////////////////////////////////////////////////////////////////
        //Build the population based on input file
        //////////////////////////////////////////////////////////////////////////

        try {

            //Indices of values in input file
            int index_personId = 0;
            int index_homeTime = 1;
            int index_xHomeCoord = 2;
            int index_yHomeCoord = 3;
            int index_workTime = 4;
            int index_xWorkCoord = 5;
            int index_yWorkCoord = 6;

            //Create coordinate transform (keep everything as is)
            //TODO this should be handled in a config file of system arguments
            CoordinateTransformation ct = TransformationFactory.getCoordinateTransformation("EPSG:26910", "EPSG:32610");

            //Iterate through input
            BufferedReader bufferedReader = new BufferedReader(new FileReader(hbwtFile));
            bufferedReader.skip(1); //skip header
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String parts[] = line.split(",");

                // /////////////////////////////////////////////////////////////////////////////////
                //Create new person and add to the population
                // Person person = populationFactory.createPerson(sc.createId(parts[index_personId]));

                Person person = populationFactory.createPerson(Id.createPersonId(parts[index_personId]));
                final Map<String, Object> customAttributes = person.getCustomAttributes();
                customAttributes.put("Employed", true);
                population.addPerson(person);

                // /////////////////////////////////////////////////////////////////////////////////
                //Create a plan for the person
                Plan plan = populationFactory.createPlan();
                person.addPlan(plan);

                ///////////////////////////////////////////////////////////////////////////////////
                //Create the home activity

                Coord homeCoordinates = CoordUtils.createCoord(Double.parseDouble(parts[index_xHomeCoord]),
                        Double.parseDouble(parts[index_yHomeCoord]));
                Activity activity1 =
                        populationFactory.createActivityFromCoord(
                                "h1", ct.transform(homeCoordinates));

                //Add time to leave home and add home Activity to plan.
                activity1.setEndTime(Double.parseDouble(parts[index_homeTime]));
                plan.addActivity(activity1);

                //Create a leg for leaving home and add to plan.
                plan.addLeg(populationFactory.createLeg("car"));

                ///////////////////////////////////////////////////////////////////////////////////
                //Create the work activity
                Coord woorkCoordinates = CoordUtils.createCoord(Double.parseDouble(parts[index_xWorkCoord]),
                        Double.parseDouble(parts[index_yWorkCoord]));
                Activity activity2 =
                        populationFactory.createActivityFromCoord("w1", woorkCoordinates);

                //Add time to leave work and add work to plan
                activity2.setEndTime(Double.parseDouble(parts[index_workTime]));
                plan.addActivity(activity2);

                //Create a leg for leaving work.
                plan.addLeg(populationFactory.createLeg("car"));

                //////////////////////////////////////////////////////////////////////////////////
                //Create the final home activity
                Activity activity3 =
                        populationFactory.createActivityFromCoord(
                                "h1", ct.transform(homeCoordinates));
                plan.addActivity(activity3);

            }
        }// end try
        catch (IOException e) {
            e.printStackTrace();
        }
        //Write this population (i.e. one person) to file
        MatsimWriter popWriter = new PopulationWriter(population, network);
        popWriter.write(outPlan);

        // //Set the contorler and run.
        // Controler controler = new Controler(config);
        // controler.setOverwriteFiles(true);
        // controler.run();
    }
}