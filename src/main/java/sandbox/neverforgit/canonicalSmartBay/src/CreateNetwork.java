package sandbox.neverforgit.canonicalSmartBay.src;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.NetworkWriter;
//import org.matsim.core.api.experimental.network.NetworkWriter;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.CoordinateTransformation;
import org.matsim.core.utils.geometry.transformations.TransformationFactory;
import org.matsim.core.utils.io.OsmNetworkReader;

/**
*
* Transforms an OSM network into the appropriate MATSim network format.
*
* USAGE: You need to define the coordinate projection system and some file paths.
*
* COORDINATE PROJECTION SYSTEM:
* -Lookup the EPSG code for the projection system you are using. We normally use UTM Standard, you
* just need to find the correct zone. A good source for EPSG codes is:
* http://spatialreference.org/ref/epsg/
*
* PATHS TO DEFINE:
* 1 - input OSM file
* 2 - output network file
*/
public class CreateNetwork {

public static void main(String[] args) {
////
// PATHS TO DEFINE
////

String osm = "./input/NewDelhi.osm"; //input OSM file
String outNet = "./input/network_New_Delhi.xml"; //output network file

////
// COORDINATE PROJECTION SYSTEM
////
String epsgCode = "EPSG:32643";


Config config = ConfigUtils.createConfig();
Scenario sc = ScenarioUtils.createScenario(config);
Network net = sc.getNetwork();
CoordinateTransformation ct = TransformationFactory.getCoordinateTransformation(TransformationFactory.WGS84, epsgCode);
OsmNetworkReader onr = new OsmNetworkReader(net,ct);
//Tell MATSim to preserve the original network geometry, creating a much greater number of nodes and links
//Comment out to setKeePaths to let MATSim simplify (this should be the default option)
onr.setKeepPaths(true);
onr.parse(osm);
new NetworkCleaner().run(net);
new NetworkWriter(net).write(outNet);

}

}