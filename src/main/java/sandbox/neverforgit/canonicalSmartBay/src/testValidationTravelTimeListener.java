package sandbox.neverforgit.canonicalSmartBay.src;

import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import sandbox.neverforgit.canonicalSmartBay.validation.travelTime.TravelTimeListener;

import java.io.IOException;
import java.util.ArrayList;


/**
 *
 * @author Andrew
 *
 * Used to run the Canonical Smart Bay experiments on the AT&T servers. System input:
 * 0 - path to config file
 * 1 - path to current input plans
 * 2 - path to current output directory
 * 3 - number of time bins for delay calculations
 */

public class testValidationTravelTimeListener {

    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();
        // Load the conifg and wet the input plans and output directories
        String configPath = args[0];
        Config config = ConfigUtils.loadConfig(configPath);
        config.plans().setInputFile(args[1]);
        config.controler().setOutputDirectory(args[2]);
        //TODO only overwrite output for testing! Delete this for final controller.
        config.controler().setOverwriteFileSetting(OutputDirectoryHierarchy.OverwriteFileSetting.deleteDirectoryIfExists);

        Controler controler = new Controler(config);

        // Add the travel time validation. Run on every 1'th iteration
        double cutOff = 0;
        ArrayList<Integer> iterations = new ArrayList<Integer>();
        for (int i = 0; i <= 10; i++){
            iterations.add(i);
        }
        String outDir = args[2];
        TravelTimeListener ttListener = new TravelTimeListener(cutOff, iterations, outDir, 24);
        ttListener.setWriteSummary(10);
        controler.addControlerListener(ttListener);

        // Do the MATSim run
        controler.run();
        long endTime = System.currentTimeMillis();
        long totTime = endTime - startTime;
        System.out.println("TOTAL RUNTIME: " + String.valueOf(totTime));
    }
}