package sandbox.neverforgit.canonicalSmartBay.src;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.PopulationWriter;
import org.matsim.core.population.io.PopulationReader;
import org.matsim.core.utils.io.IOUtils;
import org.matsim.utils.objectattributes.ObjectAttributes;
import org.matsim.utils.objectattributes.ObjectAttributesXmlWriter;
import org.matsim.vehicles.*;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * @author sfeygin on 3/29/17
 */
public class WeightedPopulationEnricher extends DefaultHandler {
    private static final String WEIGHT = "weight";
    private final String attrOutName;
    private final String inFile;
    private final ObjectAttributes popAttributes;
    private final Population population;
    private final String popOutName;
    private Vehicles vehicles;


    public WeightedPopulationEnricher(String rootPath, String fileName, Scenario scenario) {
        final String pattern = String.format("%s/%s", rootPath, fileName);
        this.inFile = pattern + ".xml";
        this.attrOutName = pattern + "_attributes.xml.gz";
        this.popOutName = pattern + "_mod.xml.gz";
        final PopulationReader popReader = new PopulationReader(scenario);
        popReader.setValidating(false);
        popReader.parse(IOUtils.getInputStream(inFile));
        this.population = scenario.getPopulation();
        this.vehicles = scenario.getVehicles();
        popAttributes = this.population.getPersonAttributes();
    }

    public ObjectAttributes getPopAttributes() {
        return popAttributes;
    }

    public Population getPopulation() {
        return population;
    }

    public void run() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            final SAXParser saxParser = factory.newSAXParser();
            final XMLReader reader = saxParser.getXMLReader();
            reader.setContentHandler(this);
            reader.parse(new InputSource(IOUtils.getInputStream(inFile)));
            generateVehicles();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private void generateVehicles() {
        VehiclesFactory vehicleFactory = vehicles.getFactory();

        for(Person person : population.getPersons().values()){
            VehicleType vehicleType = vehicleFactory.createVehicleType(Id.create(Id.createVehicleId(person.getId()), VehicleType.class));
            vehicles.addVehicleType(vehicleType);
            vehicleType.setPcuEquivalents((Double) person.getCustomAttributes().get(WEIGHT));
            Id<Vehicle> vehicleId = Id.create(person.getId(), Vehicle.class);
            Vehicle vehicle = vehicleFactory.createVehicle(vehicleId, vehicleType);
            vehicles.addVehicle( vehicle);
        }
    }


    private void writeOutPopulation() {
        final PopulationWriter populationWriter = new PopulationWriter(population);
        populationWriter.write(popOutName);
    }

    private void writeOutAttributes() {
        final ObjectAttributesXmlWriter objectAttributesXmlWriter = new ObjectAttributesXmlWriter(popAttributes);
        objectAttributesXmlWriter.useCompression(true);
        objectAttributesXmlWriter.writeFile(attrOutName);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        String tag = (uri.length() == 0) ? qName : localName;
        if (tag.equals("person")) {
            final String personId = attributes.getValue("id");
            final double weight = Double.parseDouble(attributes.getValue(WEIGHT));
            population.getPersons().get(Id.createPersonId(personId)).getCustomAttributes().put(WEIGHT, weight);
            popAttributes.putAttribute(personId, WEIGHT, weight);
        }
    }

}
