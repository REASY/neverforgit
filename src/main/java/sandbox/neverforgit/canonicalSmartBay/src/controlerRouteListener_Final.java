package sandbox.neverforgit.canonicalSmartBay.src;

import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import sandbox.neverforgit.peviRouter.RouterControlerListener_Final;

/**
 *
 * A very simple Controller that implements the default MATSim traffic assignment modules.
 *
 */

public class controlerRouteListener_Final {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
// TODO Auto-generated method stub
        String configPath = args[0];
        Config config = ConfigUtils.loadConfig(configPath);
        config.controler().setOverwriteFileSetting(OutputDirectoryHierarchy.OverwriteFileSetting.deleteDirectoryIfExists);
        Controler controler = new Controler(config);
//controler.setOverwriteFiles(true);
        RouterControlerListener_Final rcL = new RouterControlerListener_Final();
        controler.addControlerListener(rcL);
        controler.run();
        long endTime = System.currentTimeMillis();
        long totTime = endTime - startTime;
        System.out.println("TOTAL RUNTIME: " + String.valueOf(totTime));

    }
}