package sandbox.neverforgit.canonicalSmartBay.src;

import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import sandbox.neverforgit.canonicalSmartBay.validation.commute.CommuteAnalyzer;
import sandbox.neverforgit.canonicalSmartBay.validation.routing.TripRouterListener;
import sandbox.neverforgit.canonicalSmartBay.validation.travelTime.TravelTimeListener;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;


/**
 *
 * @author Andrew
 *
 * Used to initiate a single MATSim run. The intention is to call this class in parallel via a Bash script.
 *
 * Used to run the Canonical Smart Bay experiments on remote servers. This main class does a lot more than just run
 * MATSim. The work flow is:
 *
 * 1) Run MATSim with TravelTimeListener
 * 2) Run an routeCaching Python script to optimze counts and validate at screenlines
 * 3) Run CommuteAnalyzer
 * 4) Run MatsimCountsParser to produce the output kmz and validation graphs
 *
 * We can use this class for all the experiments with one config file. We just need to supply a sys arg for the
 * approprioate input plans file.
 *
 * System input:
 * 0 - Path to MATSim config file
 * 1 - Path to config file for Python optimization and validation script
 * 2 - Path to the input plans file to use
 * 3 - Path to the root parent output dir (e.g. ./output/). This dir will contain all the output dirs for each MATSim
 *     run.
 * 4 - Freeway cutoff speed. Defines the minimum speed for a link to be considered a freeway [meter/second].
 * 5 - Congestion cutoff speed. Defines the speed below which flow is considered to be congested [meter/second].
 * 6 - Number of agents to sample for TripRouter validation
 * 7 - Iteration interval for TravelTime validation
 * 8 - Iteration interval for TripRouterListener validation
 * 9 - flowCapacityFactor
 * 10 - storageCapacityFactor
 */

public class RunParallelExperiment {

    public static void main(String[] args) throws IOException {
        runMATSim(args);
    }


    /**
     * Initiates a MATSim run with the appropriate experiment specification.
     *
     * @param args
     */
    private static void runMATSim(String[] args) throws IOException {
        System.out.println(args.length);
        // Load the sys args
        String matConfigPath = args[0];
        String pyConfigPath = args[1];
        String inputPath = args[2];
        String rootOutDir = args[3];
        double freewaySpeed = Double.valueOf(args[4]);
        double congestionSpeed = Double.valueOf(args[5]);
        int numTRAgents = Integer.valueOf(args[6]);
        int ttValInterval = Integer.valueOf(args[7]);
        int trInterval = Integer.valueOf(args[8]);


        // Initiate the config with appopriate values
        Config config = ConfigUtils.loadConfig(matConfigPath);
        config.plans().setInputFile(args[2]);
        int lastIter = config.controler().getLastIteration();
        // Set flow and storage CapacityFactors
        double flowCapacityFactor = Double.valueOf(args[9]);
        double storageCapacityFactor = Double.valueOf(args[10]);
        config.qsim().setFlowCapFactor(flowCapacityFactor/100.0);
        config.qsim().setStorageCapFactor(storageCapacityFactor/100.0);
        // Build the runId and set it.
        String fFC = Double.toString(flowCapacityFactor).split("\\.")[0];  // string int reprsentation of flowCapacityFactor
        String sFC = Double.toString(storageCapacityFactor).split("\\.")[0];
        String runId = "0p" + fFC + "_0p" + sFC;
        config.controler().setRunId(runId);
        // Build the run output directory
        String runOutdir = MessageFormat.format("{0}/{1}", rootOutDir, runId);
        config.controler().setOutputDirectory(runOutdir);

        String eventsPath = Paths.get(runOutdir,
                MessageFormat.format("ITERS/it.{0}/{1}.{0}.events.xml.gz", lastIter, runId)).
                toString();

        double simHours = config.qsim().getEndTime() / 3600;  // how many complete hours in the sim day

        ////
        // 1 - MATSim run
        ////
        Controler controler = new Controler(config);
        // configure the TravelTimeListener
        ArrayList<Integer> iterations = new ArrayList<>();
        int interval = Integer.valueOf(args[7]);
        for (int i = interval; i <= lastIter; i += interval) {
            iterations.add(i);
        }
        TravelTimeListener ttListener = new TravelTimeListener(freewaySpeed, iterations, runOutdir, simHours);
        ttListener.setWriteSummary(congestionSpeed);
        controler.addControlerListener(ttListener);
        // configure the TripRouterListener
        TripRouterListener trListenser = new TripRouterListener(runOutdir, Integer.valueOf(args[6]),
                Integer.valueOf(args[8]));
        controler.addControlerListener(trListenser);
        // run the simulation
        controler.run();


        ////
        // 2 - Python script: counts optimization and screenline validation
        ////
        System.out.println();
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Running Python scripts");

        String pyOutRootPath = MessageFormat.format("{0}/ITERS/it.{1}/{2}.{1}", runOutdir,
                lastIter, runId);
        String[] cmd = {"python", "./python/optimize_and_validate_counts.py",
                args[1], pyOutRootPath};
        Process p = Runtime.getRuntime().exec(cmd);
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(p.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(p.getErrorStream()));

        // read the output from the command
        String s = null;
        System.out.println("Standard output:\n");
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
        }

        // read any errors from the attempted command
        System.out.println("Standard error(if any):\n");
        while ((s = stdError.readLine()) != null) {
            System.out.println(s);
        }

        ////
        // 3 - Run the CommuteAnalyzer
        ////
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Running CommuteAnalyzer");
        long t0 = System.currentTimeMillis();
        String netPath = "./input/" + config.network().getInputFile();
        CommuteAnalyzer analyzer = new CommuteAnalyzer(eventsPath, netPath);
        analyzer.getCommuteHandler().setHome("Home");
        analyzer.getCommuteHandler().setWork("Work");
        analyzer.run();
        System.out.println("Time to run: " + String.valueOf((System.currentTimeMillis() - t0) / 1000.0));
        long t1 = System.currentTimeMillis();
        String analyzerOutDir = Paths.get(runOutdir,
                MessageFormat.format("ITERS/it.{0}/", lastIter)).toString();
        analyzer.setOutTotalsName(MessageFormat.format("{0}.{1}.ValidationCommuteAnalyzerTotals.txt", runId, lastIter));
        analyzer.setOutFreewayName(MessageFormat.format("{0}.{1}.ValidationCommuteAnalyzerFwy.txt", runId, lastIter));
        try {
            analyzer.write(analyzerOutDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

