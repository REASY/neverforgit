package sandbox.neverforgit.canonicalSmartBay.src;


import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.io.IOUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A very simple main.controller that implements the default MATSim traffic assignment modules.
 */
public class Runner {

    private void runPopulationEnricher(String rootPath, String filename, Scenario scenario) {
        WeightedPopulationEnricher weightedPopulationEnricher = new WeightedPopulationEnricher(rootPath, filename, scenario);
        weightedPopulationEnricher.run();
    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        final Config config = ConfigUtils.loadConfig(args[0]);
        config.controler().setOverwriteFileSetting(OutputDirectoryHierarchy.OverwriteFileSetting.overwriteExistingFiles);
        Scenario scenario = ScenarioUtils.createScenario(config);
        ScenarioUtils.loadScenario(scenario);

        if (args.length > 1) {
            final Runner runner = new Runner();
            // runner.runPopulationEnricher(args[1], args[2],scenario);
        }

        Controler controler = new Controler(scenario);

        // controler.addOverridingModule(new ScoreTrackingModule());
        clearMain(true, config.controler().getOutputDirectory());

        controler.run();

        long endTime = System.currentTimeMillis();
        long totTime = endTime - startTime;
        System.out.println("TOTAL RUNTIME: " + totTime);
    }

    private static void clearMain(boolean shouldClearMain, String outputDir) {

        if (shouldClearMain)
            try {

                final Path dir = Paths.get(outputDir);
                if (Files.isDirectory(dir)) {
                    IOUtils.deleteDirectoryRecursively(dir);
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
    }
}
