import numpy as np
import pandas as pd

import activities

__author__ = "Andrew A Campbell"


class Day(object):
    """
    A single day of travel for a single person as recorded in the CLF format. This is read from one row the chains.csv
    sample format.
    """

    def __init__(self, day_series):
        """

        :param day_series: (pd.Series) One, unprocessed, row of CLF data. This will be on row of a Pandas DataFrame.
        """
        self.dayList = day_series
        self.activities = activities.Activities(day_series['activities'])
        # Initialize the tours attribute
        self.dist_series = None

    def get_distance_before(self, dtime):
        """

        :param dtime: (datetime.datetime) Defines the time for the query. Will return estimate of total travelled
         distance up to that point.
        :return: (double)
        """
        # Build the time distance pd.Series
        ds = Distance_Series(self.activities)
        self.dist_series = ds

        # Cast to numpy.datetime64
        dtime = np.datetime64(dtime)
        ds = self.dist_series.cum_dists

        # Check if time is before OR after all observations:
        if np.all(dtime < ds.index):  # time is before all observations
            return 0
        elif np.all(ds.index < dtime):
            return ds[-1]

        # last observed distance and time before dtime
        last_dist = ds[: dtime][-1]
        last_start_time = np.datetime64(ds[: dtime].index[-1])
        #TODO we don't know the end time of the last activitiy from the previous day
        if dtime > np.datetime64(self.dist_series.start_times[0]) and \
                        dtime < np.datetime64(self.dist_series.end_times[0]):
            last_end_time = np.datetime64(self.dist_series.start_times[0])  # just use start of first act as end of previous day's last
        else:
            last_end_time = np.datetime64(
                self.dist_series.end_times[np.searchsorted(self.dist_series.end_times, dtime)-1])

        # next observed distance and time after dtime
        next_dist = ds[dtime:][0]
        next_start_time = np.datetime64(ds[dtime:].index[0])

        # check if dtime is during a trip, or during an activity
        if last_start_time >= last_end_time:  # during an activity
            return last_dist
        else:  # during a trip
            # Return linearly interpolated estimate.
            ddist = next_dist - last_dist  # total distance between last and next activity
            num = dtime - last_end_time
            denom = next_start_time - last_end_time
            return ddist * (num / denom) + last_dist






class Distance_Series(object):

    def __init__(self, activities):
        """
        :param activities: (Activities)
        """
        # populate the trip_distances list
        coords = []
        stimes = []
        etimes = []
        dists = []
        last_coord = np.array([activities.act_list[0].lat, activities.act_list[0].lon])
        for act in activities.act_list:
            coords.append(np.array([act.lat, act.lon]))
            stimes.append(act.start_time)
            etimes.append(act.end_time)
            dists.append(100*np.linalg.norm(last_coord - coords[-1]))  # roughly 100km per degree
            last_coord = coords[-1]

        self.trip_dists = np.array(dists)

        # Create a series of trip distances. The index is datetime objects.
        self.cum_dists = pd.Series(index=stimes, data=np.cumsum(self.trip_dists))

        self.start_times = np.array(stimes)
        self.end_times = np.array(etimes)





