import datetime

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import day

chains = "/Users/daddy30000/dev/data/Simulated_CLF_activity_data/Chain.csv"
df = pd.read_csv(chains)

row = df.iloc[3,:]  # example row with lots of activities

d = day.Day(row)

td = datetime.timedelta(0,1800,0)
st = d.dist_series.start_times[0] - td
et = d.dist_series.end_times[-1] + td
earliest_time = np.datetime64(st.replace(second=0))
latest_time = np.datetime64(et.replace(second=0))

td = datetime.timedelta(0,60,0)  # two minute time deltas

dates = np.arange(earliest_time, latest_time, td)
dists = [d.get_distance_before(dt) for dt in dates]

plt.plot(dates, dists)
plt.show()
plt.close()

