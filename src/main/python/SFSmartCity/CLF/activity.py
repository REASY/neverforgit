from datetime import datetime

import numpy as np



class Activity(object):

    def __init__(self, raw_act_list):
        """
        List of activity attributes that has not been processed. Elements are strings.
        :param raw_act_list: ([str])
        """

        self.user_id = int(raw_act_list[0])
        self.cluster_id = int(raw_act_list[1])
        self.lat = np.double(raw_act_list[2])
        self.lon = np.double(raw_act_list[3])
        # Need to strip off the last 6 characters. They describe the UTC offset
        self.start_time = datetime.strptime(raw_act_list[4][:-6], '%Y-%m-%d %H:%M:%S')
        self.end_time = datetime.strptime(raw_act_list[5][:-6], '%Y-%m-%d %H:%M:%S')

