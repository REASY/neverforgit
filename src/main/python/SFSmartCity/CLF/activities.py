import ast

import activity

class Activities(object):

    def __init__(self, act_string):
        '''
        :param actString: (str) Unprocessed activity string from the chains.csv file.
        '''

        # Initialized the act_list attribute
        self.act_list = []
        self.__populate_activities(act_string)

    def __populate_activities(self, act_string):
        temp_list = ast.literal_eval(act_string)
        for a in temp_list:
            self.act_list.append(activity.Activity(a))



