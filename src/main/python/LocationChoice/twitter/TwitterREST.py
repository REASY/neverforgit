from ConfigParser import ConfigParser
from datetime import datetime
import json
import logging
import time
import sys

from numpy.random import randn
from requests.exceptions import ConnectionError, ReadTimeout
from TwitterAPI import TwitterAPI
from TwitterAPI import TwitterConnectionError



class TwitterREST(object):
    """
    Class for making many REST requests to the Twitter API.
    """

    def __init__(self, conf_path):
        """
        Initializes the object and authorizes.
        :param conf_path: (str) Path to the config file with Twitter API credentials.
        """
        # Parse the config file
        conf = ConfigParser()
        conf.read(conf_path)

        # Authorization
        consumer_key = conf.get('Credentials', 'consumer_key')
        consumer_secret = conf.get('Credentials', 'consumer_secret')
        access_token_key = conf.get('Credentials', 'access_token_key')
        access_token_secret = conf.get('Credentials', 'access_token_secret')

        # Authorize
        self.api = TwitterAPI(consumer_key, consumer_secret, access_token_key, access_token_secret)

        # Other constants
        self.max_connection_nap = 3600  # max seconds to sleep when experiencing ConnectionError

        # Initialize empty fields

    def get_friends(self, username):
        """
        Raises ValueError if the resource is not available.
        :param username: (str) Unique username
        :return: ()
        """
        #screen_name=twitterapi&count=5000
        endpoint = 'friends/ids'
        cursor = str(-1)
        params = {'screen_name': username,
                  'cursor': cursor,
                  'stringify_ids': 'true'}
        friend_ids = []
        logging.info("Downloading friends for: %s" % username)
        while cursor != str(0):
            # Place request and check status
            r = self.api.request(endpoint, params)
            if r.status_code == 200:
                try:
                    limited = check_rate_limit(r)
                except KeyError as e:
                    print "KeyError - Header is:"
                    print str(r.headers)
                    logging.error("KeyError - Header is: \n" + str(r.headers))
                    random_nap()
                    return self.get_friends(username)
                if limited:
                    logging.warning("Rate Limited")
                    continue
                j = json.loads(r.text)
                friend_ids += j['ids']
                cursor = str(j['next_cursor'])
                params['cursor'] = cursor
            else:
                logging.info("status_code: %s" % str(r.status_code))
                raise ValueError
        return friend_ids

    def get_followers(self, username):
        """
        Raises ValueError if the resource is not available.
        :param username: (str) Unique username
        :return:
        """
        endpoint = 'followers/ids'
        cursor = str(-1)
        params = {'screen_name': username,
                  'cursor': cursor,
                  'stringify_ids': 'true'}
        follower_ids = []
        logging.info("Downloading friends for: %s" % username)
        while cursor != str(0):
            # Place request and check status
            r = self.api.request(endpoint, params)
            if r.status_code == 200:
                try:
                    limited = check_rate_limit(r)
                except KeyError as e:
                    print "KeyError - Header is:"
                    print str(r.headers)
                    logging.error("KeyError - Header is: \n" + str(r.headers))
                    random_nap()
                    return self.get_followers(username)
                if limited:
                    logging.warning("Rate Limited")
                    continue
                j = json.loads(r.text)
                follower_ids += j['ids']
                cursor = str(j['next_cursor'])
                params['cursor'] = cursor
            else:
                logging.info("status_code: %s" % str(r.status_code))
                raise ValueError
        return follower_ids

    def get_egonets(self, un_list, out_path, n_bad=0):
        """
        Builds the egonets (friends and followers) of all usernames in the un_list.
        :param un_list: ([str]) List of usernames.
        :param out_path:
        :return:
        """
        with open(out_path, 'w') as fo:
            fo.write("username\tnum_friends\tnum_followers\tfriend_list\tfollower_list\tdate\n")
            for i, username in enumerate(un_list):
                logging.info("Downloading egonet for: %s" % username)
                print "%d - Downloading egonet for: %s" % (i, username)
                try:
                    friends, followers = self.get_one_egonet(username)
                # Bad username
                except ValueError:
                    print "Bad name: %d" % n_bad
                    n_bad += 1
                    continue
                # Dropped internet connection
                except (ConnectionError, ReadTimeout, TwitterConnectionError) as e:
                    # friends, followers = self.egonet_connection_nap(username, initial_nap=60)
                    friends, followers = self.connection_nap(e, initial_nap=60, request_method=self.get_one_egonet,
                                                             username=username)
                    if not friends: # Returns false if bad username
                        continue

                # Successful download, write output and take a nap
                today = datetime.today()
                fo.write("%s\t%s\t%s\t%s\t%s\t%s\n" % (username, str(len(friends)), str(len(followers)),
                                               str(friends), str(followers), str(today)[0:10]))
                random_nap()

    def get_one_egonet(self, username):
        friends = self.get_friends(username)
        random_nap(30, 7)
        followers = self.get_followers(username)
        return friends, followers

    def get_users_lookup(self, username_path, out_path, request_type='POST', start_block=0, end_block=None):
        """
        Queries the users/lookup REST endpoint
        :param username_path: (str) Path to file with usernames. The name must be the first item on each line.
        :param out_path: (str) Path to write the big JSON dump.
        :param request_type: (str) Determines whether to use a REST or POST request.
        :param start_block: (int) Used for restarting from middle of list of usernames.
        :param end_block: (int) Used for defining last block, exclusive
        :return:
        """
        endpoint = 'users/lookup'
        params = {'screen_name': ''}
        un_blocks = username_blocks(username_path)
        with open(out_path, 'w') as fo:
            # Burn off blocks if start_block specified
            if start_block != 0:
                i = 0
                while i < start_block:
                    un_blocks.next()
                    i += 1
            # setup end block
            diff = None
            if start_block and end_block:
                diff = end_block - start_block
            for i, block in enumerate(un_blocks):
                # stop if end block reached
                if diff:
                    if i == diff:
                        break
                logging.info("Downloading users/lookup for block: %d" % (i + start_block))
                print "Downloading users/lookup for block: %d" % (i + start_block)
                params['screen_name'] = ','.join(name for name in block)
                try:
                    r = self.get_one_users_lookup(endpoint, params, request_type)
                except (ConnectionError, ReadTimeout, TwitterConnectionError) as e:
                    r = self.connection_nap(e, initial_nap=60, request_method=self.get_one_users_lookup,
                                            endpoint=endpoint, params=params, request_type=request_type)
                # Should successfully have a request returned, now write the text to output
                j = json.loads(r.text)  # Loadas a list of dicts
                # Write each user's dict to a separate line
                for d in j:
                    fo.write(json.dumps(d) + '\n')
                random_nap(mean_time=5, var_time=1)

    def get_one_users_lookup(self, endpoint, params, request_type):
        """

        :param endpoint:
        :param params:
        :return:
        """
        r = self.api.request(endpoint, params, method_override=request_type)
        if r.status_code == 200:  # good request
            try:
                limited = check_rate_limit(r)
            except KeyError as e:
                print "KeyError - Header is:"
                print str(r.headers)
                logging.error("KeyError - Header is: \n" + str(r.headers))
                random_nap()
                return self.get_one_users_lookup(endpoint, params)
            if limited:
                logging.warning("Rate Limited")
                # continue
            return r
        else:  # In case of bad username.
            logging.info("status_code: %s" % str(r.status_code))
            print r.status_code
            print r.headers
            return False

    ##
    # Helper methods of TWitterREST
    ##

    def set_max_connection_nap(self, seconds):
        """
        Overwrites the default maximum nap time of 1hr.
        :param seconds: (int)
        :return:
        """
        self.max_connection_nap = seconds

    def egonet_connection_nap(self, username, initial_nap=60):
        """
        Used to handle ConnectionError during the long get_egonets() runs. Sleeps for an amount of time that doubles
        each iteration up until a limit. Then waits for raw_input to indicate whether it should try and reconnect.
         Presumably, the user will verify that the internet connection is up and working before trying to reconnect
         again.
        :param username:
        :param initial_nap:
        :return:
        """
        ##
        # Try to reconnect automatically while taking increasing naps.
        ##
        nap = initial_nap
        while nap <= self.max_connection_nap:
            print "ConnectionError. Attempting to reconnect in %d seconds." % nap
            logging.info("ConnectionError. Attempting to reconnect in %d seconds." % nap)
            time.sleep(nap)
            try:
                friends = self.get_friends(username)
                random_nap()
                followers = self.get_followers(username)
            except ValueError:  # If we get a value error, we have a bad user, really need to break out of parent loop too
                return False, False
            except (ConnectionError, ReadTimeout):
                nap *= 2
                continue
            return friends, followers
        ##
        # Max nap time exceeded. Wait for user to manually decide whether or not to reconnect.
        ##
        print "Reconnect attempts timed out after %d seconds" % nap
        u_input = raw_input("Attempt to reconnect again [Y/N]?\nDo nothing if you want to wait to try and reconnect.\n")

        def validate_input(text):
            if text not in ['Y', 'N']:
                print "Only valid entries are 'Y' or 'N'."
                text = raw_input("Attempt to reconnect again [Y/N]?\n")
                return validate_input(text)
            else:
                return text
        u_input = validate_input(u_input)
        if u_input == 'Y':  # Try one more time to reconnect.
            print "Attempting to download egonet for: %s" % username
            try:
                friends = self.get_friends(username)
                random_nap()
                followers = self.get_followers(username)
            except ValueError:  # If we get a value error, we have a bad user, really need to break out of parent loop too
                return False, False
            except (ConnectionError, ReadTimeout):  # Exit if we still have a bad connection.
                print("Connection still not working. Exiting now")
                logging.info("ConnectionError. Failed manual reconnect.")
                sys.exit()
            return friends, followers  # Success!
        else:  # Since we validated, u_input must be 'N'
            print("You have chosen not to reconnect. Exiting now")
            logging.info("ConnectionError. User elected to exit.")
            sys.exit()

    def connection_nap(self, exception, initial_nap=60, request_method=None,  **kwargs):
        """
        Used to handle ConnectionError during the long runs of requests. Sleeps for an amount of time that doubles
        each iteration up until a limit. Then waits for raw_input to indicate whether it should try and reconnect.
         Presumably, the user will verify that the internet connection is up and working before trying to reconnect
         again.
        :param exception: The exception that was raised triggering a call to this method.
        :param initial_nap: (float) The initial time to sleep in seconds. Defaults to 60 seconds.
        :param request_method: One of the request methods defined for the TwitterREST class. Takes **kwargs
        :param kwargs: The named arguments passed to the request_method.
        :return: (tuple) Returns whatever the request_method returns.
        """
        if not request_method:
            print "Must specify a request_method!"
            return  # Presumably this will ultimately result in a sys.exit
        ##
        # Try to reconnect automatically while taking increasing naps.
        ##
        nap = initial_nap
        while nap <= self.max_connection_nap:
            print "%s: Attempting to reconnect in %d seconds." % (str(exception.message), nap)
            logging.info("%s: Attempting to reconnect in %d seconds." % (str(exception.message), nap))
            time.sleep(nap)
            try:
                out = request_method(**kwargs)  # This is tuple of unkown number of values!
            except ValueError:  # If we get a value error, we have a bad user, really need to break out of parent loop too
                return False, False
            except (ConnectionError, ReadTimeout, TwitterConnectionError):
                nap *= 2
                continue
            return out
        ##
        # Max nap time exceeded. Wait for user to manually decide whether or not to reconnect.
        ##
        print "Reconnect attempts timed out after %d seconds" % nap
        u_input = raw_input("Attempt to reconnect again [Y/N]?\nDo nothing if you want to wait to try and reconnect.\n")

        def validate_input(text):
            if text not in ['Y', 'N']:
                print "Only valid entries are 'Y' or 'N'."
                text = raw_input("Attempt to reconnect again [Y/N]?\n")
                return validate_input(text)
            else:
                return text
        u_input = validate_input(u_input)
        if u_input == 'Y':  # Try one more time to reconnect.
            print "Attempting to reconnect with method: %s" % str(request_method)
            try:
                out = request_method(**kwargs)
            except ValueError:  # If we get a value error, we have a bad user, really need to break out of parent loop too
                return False, False
            except (ConnectionError, ReadTimeout, TwitterConnectionError) as e:  # Exit if we still have a bad connection.
                print(str(e.message))
                print "Still not working. Exiting now."
                logging.info(str(e.message))
                logging.info("Exiting due to failed reconnect attempts.")
                sys.exit()
            return out  # Success!
        else:  # Since we validated, u_input must be 'N'
            print("You have chosen not to reconnect. Exiting now")
            logging.info(" User elected to exit.")
            sys.exit()

##
# Stand alone helper functions
##

def random_nap(mean_time=60, var_time=15):
    """
    Sleeps for random number of seconds, drawn from the normal distribution. Min nap time is 0.
    :param mean_time: (float)
    :param var_time: (float)
    :return:
    """
    r = max(0, randn()*var_time + mean_time)
    time.sleep(r)

def check_rate_limit(r):
    """
    Checks the rate_limit status and sleeps if necessary.
    :param r: (TwitterAPI.request)
    :return:
    """
    if r.headers['x-rate-limit-remaining'] == str(0):
        sleep_time = int(r.headers['x-rate-limit-reset']) - time.time() + 15  # Wait additional 15 seconds
        print "Rate limited. Sleeping for: %d" % sleep_time
        time.sleep(sleep_time)
        return True
    else:
        return False

def username_blocks(username_path, block_len=100, header=True):
    """
    A generator to yield sublists of usernames of length block_len.
    :param username_path: (str) Path to file with usernames. The name must be the first item on each line.
    :param block_len: (int) Length of blocks.
    :return:
    """
    with open(username_path, 'r') as fi:
        if header: fi.next()  # burn off the header
        block = []
        for i, line in enumerate(fi):
            block.append(line.split(',')[0].strip('@'))
            if (i+1) % block_len == 0:
                yield block
                block = []
    yield block

