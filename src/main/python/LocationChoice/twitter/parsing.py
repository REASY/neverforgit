import re

'''
These tools are used for parsing the Twitter dump provided by Eric Fischer.
'''

def get_username(line):
    """

    :param line: (str) One line of the file from Eric's Twitter dump.
    :return: (str) The unique username of the person tweeting.
    """
    s = re.search(r'@[\w]*', line)
    return line[s.start(): s.end()]

def get_date(line):
    """

    :param line: (str) One line of the file from Eric's Twitter dump.
    :return: (str) String representation of the tweet date.
    """
    s = re.search(r'[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}', line)
    return line[s.start(): s.end()]

def get_coords(line):
    """

    :param line: (str) One line of the file from Eric's Twitter dump.
    :return: (str) String representation of the Lon and Lat
    """
    s = re.search(r'-?[0-9]{1,3}\.[0-9]*,-?[0-9]{1,3}\.[0-9]*', line)
    return line[s.start(): s.end()]

def get_offset(line):
    """

    :param line: (str) One line of the file from Eric's Twitter dump.
    :return:
    """
    #TODO, it might be safer to first get the end of the coords and only search over the remaining line
    s = re.search(r'[0-1][.][0-9]*,[0-1][.][0-9]*', line)
    return line[s.start(): s.end()]


def get_url(line):
    """

    :param line: (str) One line of the file from Eric's Twitter dump.
    :return:
    """
    s = re.search(r'http://twitter.com[/\w]*', line)
    return line[s.start(): s.end()]

def get_source(line):
    """

    :param line: (str) One line of the file from Eric's Twitter dump.
    :return:
    """
    s = re.search(r'source:[\w]*', line)
    return line[s.start(): s.end()]

def get_name(line):
    """

    :param line:
    :return:
    """
    s = re.search(r'name:[\w]*', line)
    return line[s.start(): s.end()]

def get_message(line):
    """

    :param line: (str) One line of the file from Eric's Twitter dump.
    :return:
    """
    name = get_name(line)
    return line[name.end()+1:]
