import ConfigParser
import sys

import numpy as np

import twitter.parsing as prs

__author__ = 'Andrew A Campbell'
# This script is used to generate a random sample of the 4.3M geotagged tweets given to me by Eric Fischer

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    big_tweetbig_tweet_log_path_log = conf.get('Paths', 'big_tweet_log_path')
    sample_out_path = conf.get('Paths', 'sample_out_path')

    # Params
    sample_size = int(conf.get('Params', 'sample_size').strip())

    coords = []
    with open(big_tweetbig_tweet_log_path_log, 'r') as fi:
        coords = [prs.get_coords(line) for line in fi]

    sample = np.random.choice(np.arange(len(coords)), size=sample_size, replace=False)  # Indices of sample

    with open(sample_out_path, 'w') as fo:
        fo.write('Latitude,Longitude\n')
        for idx in sample:
            fo.write("%s\n" % coords[idx])









