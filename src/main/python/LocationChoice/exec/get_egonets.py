from collections import Counter
import ConfigParser
from datetime import datetime
import logging
import os
import sys


from twitter import TwitterREST

__author__ = 'Andrew A Campbell'
# Retrievs the egonetworks (friends and followers) for all usernames. Writes the user_ids to timestamped file

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    ego_nets_dir = conf.get('Paths', 'ego_nets_dir')
    twitter_rest_config_path = conf.get('Paths', 'twitter_rest_config_path')
    usersnames_path = conf.get('Paths', 'usersnames_path')
    log_path = conf.get('Paths', 'log_path')

    # Params
    first = int(conf.get('Params', 'first'))
    last = int(conf.get('Params', 'last'))

    # Start logging
    logging.basicConfig(filename=log_path, level=logging.DEBUG)

    # Initialize the API connection
    TR = TwitterREST.TwitterREST(twitter_rest_config_path)
    # TR.set_max_connection_nap(59)

    # Build the list of usernames
    num_range = range(first, last)
    usernames = []
    with open(usersnames_path, 'r') as fi:
        fi.next()  # burn off the header
        for i, line in enumerate(fi):
            if i in num_range:
                usernames.append(line.split(',')[0].strip('@'))

    # Download the egonets
    ego_net_path = os.path.join(ego_nets_dir, "%d_%d_egonets.txt" % (first+1, last-1))
    TR.get_egonets(usernames, ego_net_path)





