import ConfigParser
import logging
import os
import sys


from twitter import TwitterREST

__author__ = 'Andrew A Campbell'
# Retrieves the egonetworks (friends and followers) for all usernames after filtering based on number of tweets,
# friends and followers. Filtering was done in ../../notebooks/twitter_eda.ipynb
#  Writes the user_ids to a file

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    egonets_dir = conf.get('Paths', 'egonets_dir')
    twitter_rest_config_path = conf.get('Paths', 'twitter_rest_config_path')
    filtered_users_path = conf.get('Paths', 'filtered_users_path')
    egonets_so_far_path = conf.get('Paths', 'egonets_so_far_path')
    log_path = conf.get('Paths', 'log_path')

    # Params
    first = int(conf.get('Params', 'first'))
    last = int(conf.get('Params', 'last'))
    n_bad = int(conf.get('Params', 'n_bad'))

    # Start logging
    logging.basicConfig(filename=log_path, level=logging.DEBUG)

    # Users who I have alreday downloaded egonets for
    so_far = []
    with open(egonets_so_far_path, 'r') as fi:
        for line in fi:
            so_far.append(line.strip())

    # Build the list of usernames to download
    num_range = range(first, last)
    usernames = []
    with open(filtered_users_path, 'r') as fi:
        fi.next()  # burn off the header
        x = 0
        for i, line in enumerate(fi):
            if (i in num_range) and (line.strip() not in so_far):
                usernames.append(line.strip())
            else:
                # print "%s already downloaded" % line.strip()
                x += 1
    print "%d names already downloaded" % x
    print "%d names need to be download" % len(usernames)

    # Download the egonets
    # Initialize the API connection
    TR = TwitterREST.TwitterREST(twitter_rest_config_path)
    # TR.set_max_connection_nap(59)
    ego_net_path = os.path.join(egonets_dir, "%d_%d_egonets.txt" % (first+1, last-1))
    TR.get_egonets(usernames, ego_net_path, n_bad)





