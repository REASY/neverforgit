import ConfigParser
import json
import os
import sys

__author__ = 'Andrew A Campbell'
# Parses all the output files from the first runs of get_egonets.py and produces a file with all the user names.

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    ego_nets_dir = conf.get('Paths', 'ego_nets_dir')
    out_path = conf.get('Paths', 'out_path')

    # Params
    suffix = conf.get('Params', 'suffix')

    # Get names of all files to parse
    o_dir = os.getcwd()
    os.chdir(ego_nets_dir)
    fnames = [n.strip() for n in os.listdir('.') if suffix in n]

    # Parse each file and get the username
    usernames = set()
    for name in fnames:
        print name
        with open(name, 'r') as fi:
            fi.next()  # burn off header
            for line in fi:
                usernames.add(line.split('\t')[0][1:])

    # Write the output file
    with open(out_path, 'w') as fo:
        for un in usernames:
            fo.write(un+'\n')

    os.chdir(o_dir)





