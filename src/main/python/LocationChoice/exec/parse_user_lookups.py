import ConfigParser
import json
import sys


__author__ = 'Andrew A Campbell'
# This script reads the output csv created by get_user_lookups.py and creates a square csv with
# with the following fields for each user:
# id
# screen_name
# location
# friends_count
# followers_count
# statuses_count
# listed_count


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    users_lookup_path = conf.get('Paths', 'users_lookup_path')
    out_path = conf.get('Paths', 'out_path')

    # Params
    keys = [k.strip() for k in conf.get('Params', 'keys').split(',')]

    with open(users_lookup_path, 'r') as fi:
        str_types = [str, unicode]
        with open(out_path, 'w') as fo:
            fo.write('\x1f'.join(keys)+'\n')  # header
            for line in fi:
                j = json.loads(line)
                # row = ','.join([str(j[k]) if type(k) != str else k for k in keys]).encode('utf8')
                row = '\x1f'.join([j[k] if type(j[k]) in str_types else str(j[k]) for k in keys]).encode('utf8')
                fo.write(row+'\n')
