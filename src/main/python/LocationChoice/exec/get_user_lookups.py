import ConfigParser
import logging
import os
import sys


from twitter import TwitterREST

__author__ = 'Andrew A Campbell'
# Retrievs the user/lookups for all members of sample. This endpoint provides a lot of metadata about individual users

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    users_lookup_dir = conf.get('Paths', 'users_lookup_dir')
    twitter_rest_config_path = conf.get('Paths', 'twitter_rest_config_path')
    usersnames_path = conf.get('Paths', 'usersnames_path')
    log_path = conf.get('Paths', 'log_path')

    # Params
    start_block = conf.get('Params', 'start_block')
    if start_block:
        first_block = start_block
        start_block = int(start_block)
    else:
        first_block = '0'
        start_block = 0
    end_block = conf.get('Params', 'end_block')
    if end_block:
        last_block = end_block
        end_block = int(end_block)
    else:
        last_block='end'

    # Start logging
    logging.basicConfig(filename=log_path, level=logging.DEBUG)

    # Initialize the API connection
    TR = TwitterREST.TwitterREST(twitter_rest_config_path)
    # TR.set_max_connection_nap(59)

    # Download the user/lookups
    users_lookup_path = os.path.join(users_lookup_dir, "%s_%s_users_lookup.txt" % (first_block, last_block))
    TR.get_users_lookup(usersnames_path, users_lookup_path, start_block=start_block, end_block=end_block)





