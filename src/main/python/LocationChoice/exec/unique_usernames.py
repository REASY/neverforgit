from collections import Counter
import ConfigParser
import sys


import twitter.parsing as prs

__author__ = 'Andrew A Campbell'
# This script gets all the unique usernames and their number of tweets from Eric's Twitter dump

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    big_tweetbig_tweet_log_path_log = conf.get('Paths', 'big_tweet_log_path')
    users_out_path = conf.get('Paths', 'users_out_path')

    usernames = []
    with open(big_tweetbig_tweet_log_path_log, 'r') as fi:
        usernames = [prs.get_username(line) for line in fi]

    usernames = Counter(usernames)
    n = 0  # totla number of unique users
    with open(users_out_path, 'w') as fo:
        fo.write('Username,Number_Tweets\n')
        for u, c in usernames.iteritems():
            fo.write('%s,%d\n' % (u, c))
            n += 1
    print 'Total number of unique users: %d' % n
