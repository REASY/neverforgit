import ConfigParser
from datetime import datetime
import sys

import utils.counts

__author__ = 'Andrew A Campbell'
# This script creates MATSim validation counts for a single day based on the output of PeMS_Tools.

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)
    # Paths
    station_TS_dir = conf.get('Paths', 'station_TS_dir')  # Path to station Time Series
    stat_link_map_file = conf.get('Paths', 'stat_link_map_file')  # Template xml doc for building counts
    out_file = conf.get('Paths', 'out_file')  # Where to write the counts file

    # Parameters
    day_date = datetime.strptime(conf.get('Params', 'day_date'), '%Y/%m/%d')
    counts_name = conf.get('Params', 'counts_name')
    counts_desc = conf.get('Params', 'counts_desc')
    counts_year = conf.get('Params', 'counts_year')


    # Create the counts file
    utils.counts.create_PeMS_Tools_counts_singleday(station_TS_dir, stat_link_map_file,
                                                    day_date,
                                                    counts_name,
                                                    counts_desc,
                                                    counts_year,
                                                    out_file=out_file)

