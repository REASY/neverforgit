import ConfigParser
from datetime import datetime
import sys


__author__ = 'Andrew A Campbell'
# This script creates MATSim validation counts for a single day based on the output of PeMS_Tools.

def main(args):
    config_path = args[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)
    # Paths
    test_val = conf.get('Params', 'test_val')  # Path to station Time Series

    print "The test_val is: %s" % test_val

print "The __name__ is: " + __name__

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    main(sys.argv)


