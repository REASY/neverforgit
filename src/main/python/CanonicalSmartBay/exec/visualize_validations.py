import ConfigParser
import time
import sys

import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from utils import spatial_tools

__author__ = "Andrew A Campbell"

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)

    # Paths
    taz_file = conf.get('Paths', 'taz_file')
    commute_validation_file = conf.get('Paths', 'commute_validation_file')
    commute_fwy_validation_file = conf.get('Paths', 'commute_fwy_validation_file')
    county_file = conf.get('Paths', 'county_file')

    VIZ_1_out_file = conf.get('Paths', 'VIZ_1_out_file')
    VIZ_2_time_out_file = conf.get('Paths', 'VIZ_2_time_out_file')
    VIZ_3_dist_out_file = conf.get('Paths', 'VIZ_3_dist_out_file')
    VIZ_4_tab_out_file = conf.get('Paths', 'VIZ_4_tab_out_file')
    VIZ_4_bars_out_file = conf.get('Paths', 'VIZ_4_bars_out_file')

    # Params

    ####################################################################################################################
    # VIZ_1 - total commute times by TAZ
    ####################################################################################################################

    # Load the TAZ shapefile and validation files
    taz_gdf = gpd.read_file(taz_file)
    crs_orig = {'makeFacilities' :'epsg:26910'}
    val_gdf = spatial_tools.text_to_points_gdf(commute_validation_file, 'HomeX', 'HomeY', sep='\t', crs=crs_orig)
    crs_new = {'makeFacilities' :'epsg:4326'}
    val_gdf.to_crs(crs_new, inplace=True)  # project to WGS84

    ##
    # Spatial join map points to TAZs
    ##

    # Method 1 - works but (was slow) now fast w/ Geopandas update
    t0 = time.time()
    val_gdf_1 = gpd.sjoin(val_gdf, taz_gdf, how='left', op='within')
    print 'Method 1 Time: %f' % (time.time() - t0)
    # >>>Method 1 Time: 431.206596 (old)
    # >>>Method 1 Time: 15.086126 (new)

    # Method 2 - using r-tree
    # t0 = time.time()
    # sindex = val_gdf.sindex
    # val_gdf['taz_key'] = np.nan
    # for i, bounds in enumerate(taz_gdf.bounds.iterrows()):
    #     possible_matches_index = list(sindex.intersection(bounds[1]))
    #     possible_matches = val_gdf.iloc[possible_matches_index]
    #     precise_matches_index = possible_matches.intersects(taz_gdf.iloc[i].geometry)
    #     precise_matches_index = precise_matches_index[precise_matches_index].index
    #     val_gdf.loc[precise_matches_index, 'taz_key'] = taz_gdf.iloc[i]['taz_key']
    # print 'Method 2 Time: %f' % (time.time() - t0)
    #>>>Method 2 Time: 73.439373

    ##
    # Aggregate values to TAZ level
    ##
    g_1 = val_gdf_1.groupby('taz_key')
    means_1 = g_1.mean()
    means_1['taz_key'] = means_1.index.astype(int)
    # join with the geometries
    merged_1 = taz_gdf.merge(means_1, how='outer', on='taz_key')


    ##
    # Plots
    ##

    # Total HW commute time
    alpha = 1
    linewidth = .1
    clrmap = 'BuPu_r'
    merged_1['TotalTimeH2W_Minutes'] = merged_1['TotalTimeH2W'] / 60.0
    vmin = np.min(merged_1['TotalTimeH2W_Minutes'])
    vmax = np.max(merged_1['TotalTimeH2W_Minutes'])
    ax = merged_1.plot('TotalTimeH2W_Minutes', colormap=clrmap, vmin=vmin, vmax=vmax, figsize=(15, 12.5),
                       linewidth=linewidth, alpha=alpha)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])

    fig = ax.get_figure()
    cax = fig.add_axes([0.91, 0.11, 0.03, 0.775])
    sm = plt.cm.ScalarMappable(cmap=clrmap, norm=plt.Normalize(vmin=vmin, vmax=vmax))
    # fake up the array of the scalar mappable. Urgh...
    sm._A = []
    cb = fig.colorbar(sm, cax=cax, alpha=alpha)
    cb.set_label('minutes', fontsize=20)
    cb.ax.tick_params(labelsize=15)
    # using ColorBase
    # cb1 = mpl.colorbar.ColorBase(cax, cmap=sm, orientation='vertical' )
    plt.savefig(VIZ_1_out_file)
    plt.show()
    plt.close()

    ####################################################################################################################
    # VIZ_2 and VIZ_3 - freeway commute times and distances stables
    ####################################################################################################################

    # Load the freeway-only validation file
    val_fwy_gdf = spatial_tools.text_to_points_gdf(commute_validation_file, 'HomeX', 'HomeY', sep='\t', crs=crs_orig)
    val_fwy_gdf.to_crs(crs_new, inplace=True)  # project to WGS84

    ##
    # Spatial join to map points to Counties
    ##
    county_gdf = gpd.read_file(county_file)

    ##
    # Spatial join map points to Counties
    ##
    val_fwy_gdf_2 = gpd.sjoin(val_fwy_gdf, county_gdf, how='left', op='within')
    
    ##
    # Aggregate values at County level
    ##
    g_2 = val_fwy_gdf_2.groupby('COUNTY')
    means_2 = g_2.mean()
    means_2['COUNTY'] = means_2.index
    # join with the geometries
    merged_2 = county_gdf.merge(means_2, how='outer', on='COUNTY')

    ##
    # Tables
    ##
    
    # Times
    comm_times = merged_2[['TotalTimeH2W', 'DelayTimeH2W','TimeInCongestionH2W']]
    comm_times.index = merged_2['COUNTY']
    s = val_fwy_gdf.mean()[['TotalTimeH2W', 'DelayTimeH2W','TimeInCongestionH2W']]
    s.name = 'TOTALS'
    comm_times = comm_times.append(s)
    comm_times = (comm_times/60).round(1)
    comm_times.sort(inplace=True)
    comm_times.index.rename('', inplace=True)
    comm_times.to_csv(VIZ_2_time_out_file, sep='\t')

    # Distances
    comm_dists = merged_2[['TotalDistH2W', 'DistInCongestionH2W']]
    comm_dists.index = merged_2['COUNTY']
    s = val_fwy_gdf.mean()[['TotalDistH2W', 'DistInCongestionH2W']]
    s.name = 'TOTALS'
    comm_dists = comm_dists.append(s)
    comm_dists = (comm_dists/1609.344).round(1)
    comm_dists.sort(inplace=True)
    comm_dists.index.rename('', inplace=True)
    comm_dists.to_csv(VIZ_3_dist_out_file, sep='\t')


    ####################################################################################################################
    # VIZ_4 Commute patterns - horizontal stacked bars
    ####################################################################################################################

    ##
    # Calculate the county-to-county h2w commute flows
    ##

    # Get home counties from totals gdf
    val_gdf_4_home = gpd.sjoin(val_gdf, county_gdf, how='left', op='within')
    # val_gdf_4_home.rename(index=str, columns={'COUNTY': 'COUNTY_HOME', 'geometry': 'geometry_home'}, inplace=True)

    # Create a geometry column of work locations
    x_col, y_col = 'WorkX', 'WorkY'
    val_gdf_4_work = spatial_tools.text_to_points_gdf(commute_validation_file, x_col, y_col, sep='\t', crs=crs_orig)
    val_gdf_4_work.to_crs(crs_new, inplace=True)
    val_gdf_4_work = gpd.sjoin(val_gdf_4_work, county_gdf, how='left', op='within')

    # Create merged df w/ home and work counties
    merged_4 = pd.DataFrame({'COUNTY_HOME': val_gdf_4_home['COUNTY'],
                             'COUNTY_WORK': val_gdf_4_work['COUNTY'], 'cnt': 1})
    # Group by counties and get total counts
    g_4 = merged_4.groupby(['COUNTY_HOME', 'COUNTY_WORK'])
    commute_patterns = g_4.count()
    cp = commute_patterns.unstack()
    cp.to_csv(VIZ_4_tab_out_file, sep='\t')

    ##
    # Build the visualization
    ##

    counties = sorted(cp.index.values)  # sorted list of county names
    cp.sort(inplace=True)
    cp.sort(axis=1, inplace=True)

    # Get the widths of each individual bar in the horizontal stacks
    widths = []
    for i in range(cp.shape[0]):
        # row = [-1 * n for n in cp.iloc[i,:].values.tolist()] + cp.iloc[:,i].values.tolist()[::-1]
        row = cp.iloc[i, :].values.tolist() + cp.iloc[:, i].values.tolist()[::-1]
        row = np.delete(row, [i, len(row) -i - 1])  # delete the self-self flows
        widths.append(row)
    widths = np.array(widths)
    widths = 15.5*widths  # Scaling up to whole population.

    # Calc left edges of each bar
    lefts = []
    for i in range(cp.shape[0]):
        left = [-1*np.sum(widths[i, 0:widths.shape[1]/2])]
        left = np.append(left, left[0] + np.cumsum(widths[i, 0:-1]))
        #
        # for j in np.arange(widths.shape[1] - 1):
        #     left.append(left[j] + widths[i, j])
        lefts.append(left)
    lefts = np.array(lefts)

    # Define colors for each bar. Skips the colors for self-self flows
    cmmap_name = 'Set1'
    cmap = plt.get_cmap(cmmap_name)
    all_colors = [cmap(i) for i in np.linspace(0, 1, cp.shape[0])]
    all_colors = all_colors + all_colors[::-1]
    colors = [np.array(all_colors[1:-1])]
    for i in range(1, cp.shape[0]):
        c_left = all_colors[0:i]
        c_mid = all_colors[i+1: -i -1]
        c_right = all_colors[-i:]
        colors.append(np.array(c_left + c_mid + c_right))
    colors = np.array(colors)

    # Build the stacked horizontal bar plot
    pos = -1*np.arange(cp.shape[0]) - 0.5
    fig = plt.figure(figsize=(16, 9))
    plts = []
    for i in np.arange(widths.shape[1]):
    # for i in np.arange(3):
        p = plt.barh(pos, widths[:, i], left=lefts[:, i], color=colors[:, i, :], alpha=0.5)
        plts.append(p)
    # patches = [p[0] for p in plts]
    patches = [plts[i].patches[i+1] for i in np.arange(cp.shape[0]-1)]
    patches.append(plts[cp.shape[0]].patches[0])
    #
    # face_colors = [plts[i].patches[i+1].get_facecolor() for i in np.arange(cp.shape[0]-1)]
    # face_colors.append(plts[cp.shape[0]-1].patches[0].get_facecolor())
    plt.legend(patches[0:9], counties, bbox_to_anchor=(0.0, -0.15, 1.0, .102), loc=3,
               ncol=5, mode="expand", borderaxespad=0.0, fontsize=15)
    plt.yticks(pos+0.4, counties, fontsize=20)
    ax = fig.get_axes()[0]
    # ax.set_xticklabels(ax.get_xticklabels(), fontsize=20)
    plt.setp(ax.get_xticklabels(), fontsize=20)
    plt.savefig(VIZ_4_bars_out_file, bbox_inches='tight')
    plt.show()
    plt.close()
    #TODO - find better cmap than Paired
    #TODO - fine formatting (remove y-ticks, move legend to below)









    # # val_gdf_4['geometry_work'] = val_gdf_4.apply(lambda z: Point(z[x_col], z[y_col]), axis=1)
    # # val_gdf_4.set_geometry('geometry_work', inplace=True)
    # val_gdf_4['geometry'] = val_gdf_4.apply(lambda z: Point(z[x_col], z[y_col]), axis=1)
    # val_gdf_4.set_geometry('geometry', inplace=True)
    # val_gdf_4.crs = crs_orig
    # val_gdf_4.to_crs(crs_new, inplace=True)
    #
    #
    # # Get work counties
    # val_gdf_4_5 = gpd.sjoin(val_gdf_4, county_gdf, how='left', op='within')
    # val_gdf_4_5.rename(index=str, columns={'COUNTY': 'COUNTY_WORK'}, inplace=True)





















