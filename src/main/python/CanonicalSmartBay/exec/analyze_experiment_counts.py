import ConfigParser
import sys

import numpy as np
import pandas as pd

import utils.counts

__author__ = 'Andrew A Campbell'
# This script analyzes the counts from some MATSim runs

if __name__ == '__main__':
    # if len(sys.argv) < 2:
    #     print 'ERROR: need to supply the path to the conifg file'
    # config_path = sys.argv[1]
    # conf = ConfigParser.ConfigParser()
    # conf.read(config_path)
    # # Paths
    # station_TS_dir = conf.get('Paths', 'station_TS_dir')  # Path to station Time Series
    # stat_link_map_file = conf.get('Paths', 'stat_link_map_file')  # Template xml doc for building counts
    # out_file = conf.get('Paths', 'out_file')  # Where to write the counts file
    #
    # # Parameters
    # day_date = datetime.strptime(conf.get('Params', 'day_date'), '%Y/%m/%d')
    # counts_name = conf.get('Params', 'counts_name')
    # counts_desc = conf.get('Params', 'counts_desc')
    # counts_year = conf.get('Params', 'counts_year')

    df0 = pd.read_csv("/Users/daddy30000/dev/data/Canonical_SmartBay/experiments/mtc_initial/set0/ITERS/it.10/run0.10.countscompare.txt", sep="\t")
    df1 = pd.read_csv("/Users/daddy30000/dev/data/Canonical_SmartBay/experiments/mtc_initial/set1/ITERS/it.10/run0.10.countscompare.txt", sep="\t")
    df2 = pd.read_csv("/Users/daddy30000/dev/data/Canonical_SmartBay/experiments/mtc_initial/set2/ITERS/it.10/run0.10.countscompare.txt", sep="\t")
    df3 = pd.read_csv("/Users/daddy30000/dev/data/Canonical_SmartBay/experiments/mtc_initial/set3/ITERS/it.10/run0.10.countscompare.txt", sep="\t")

    print utils.counts.optimize_counts(df0)
    print utils.counts.optimize_counts(df1)
    print utils.counts.optimize_counts(df2)
    print utils.counts.optimize_counts(df3)




