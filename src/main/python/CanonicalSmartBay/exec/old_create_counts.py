import ConfigParser
import sys

import pandas as pd

import utils.counts

__author__ = 'Andrew A Campbell'
# WARNING: This script and its methods are old and out of date. It is based on the work that Emin and Andrew
# did in 2014.

# This script creatues counts.xml files for counts validation with MATSim.

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'ERROR: need to supply the path to the conifg file'
    config_path = sys.argv[1]
    conf = ConfigParser.ConfigParser()
    conf.read(config_path)
    # Paths
    data_path = conf.get('Paths', 'data_path')  # Path to MTC PeMS typical weekday
    output_path = conf.get('Paths', 'output_path')
    template_path = conf.get('Paths', 'template_path')  # Template xml doc for building counts
    matched_path = conf.get('Paths', 'matched_path') # Path to csv of file matching stations to links
    # Parameters
    flow_type = conf.get('Params', 'flow_type')
    year = int(conf.get('Params', 'year'))
    counts_name = conf.get('Params', 'counts_name')
    counts_desc = conf.get('Params', 'counts_desc')


    # Open the DataFrames, filter and configure
    pems_df = pd.read_csv(data_path)
    pems_df = pems_df[pems_df['year'] == year]
    matched_df = pd.read_csv(matched_path)
    matched_df.index = matched_df['station']

    # Create the counts file
    utils.counts.create_MTC_counts(pems_df, matched_df, flow_type, template_path,
                                       counts_name, counts_desc, year, output_path)

