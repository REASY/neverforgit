from __future__ import  division
import numpy as np
import pandas as pd
import datetime
import pytz
from datetime import timedelta
import scipy.stats as stats
import glob, os
import warnings
import utm
import json
import math
from dateutil import parser
import geopandas as gpd
from shapely.geometry import Point
from utm.error import OutOfRangeError
warnings.simplefilter("ignore")

import sys
sys.path.append("/home/mogeng/attResearch/resources/IO-HMM/code/main/")
sys.path.append("/home/mogeng/attResearch/resources/IO-HMM/code/auxiliary")
from UnSupervised_IOHMM import *

# example: /home/mogeng/.conda/envs/MogengPython27/bin/python2.7 /home/mogeng/attResearch/scripts/server/AM_PM/IOHMM_Sequence_Generator/sequenceGeneratorForMultipleHoursOptimized.py /home/mogeng/attResearch/sample/CLF/AM_PM/SF/single_week/20150608-20150614/IOHMM_Plans/2015-6-10/ /home/mogeng/attResearch/sample/CLF/AM_PM/SF/single_week/20150608-20150614/data/commuter/all/ /home/mogeng/attResearch/output/CLF/ComparisonOverCities/SF/single_month/20150601-20150628/9_spec_4_20000_DT/snapshot_4/SHMM/ /home/mogeng/attResearch/output/CLF/ComparisonOverCities/SF/single_month/20150601-20150628/9_spec_4_20000_DT/spec.json /home/mogeng/attResearch/output/CLF/ComparisonOverCities/SF/single_month/20150601-20150628/9_spec_4_20000_DT/activity_labels.json 2015 6 10 --location_method=1 --num_data=100


def censoredEStep(SHMM, flags):
    SHMM.log_gammas = []
    SHMM.log_epsilons = []
    SHMM.lls = []
    for seq in range(SHMM.num_seqs):
        n_records = SHMM.dfs[seq].shape[0]
        log_prob_initial = SHMM.model_initial.predict_log_probability(SHMM.inp_initials[seq]).reshape(SHMM.num_states,)
        assert log_prob_initial.shape == (SHMM.num_states,)
        log_prob_transition = np.zeros((n_records - 1, SHMM.num_states, SHMM.num_states))
        for st in range(SHMM.num_states):
             log_prob_transition[:,st,:] = SHMM.model_transition[st].predict_log_probability(SHMM.inp_transitions[seq]) 
        assert log_prob_transition.shape == (n_records-1,SHMM.num_states,SHMM.num_states)

        log_Ey = np.zeros((n_records,SHMM.num_states))
        for emis in range(SHMM.num_emissions):
            model_collection = [models[emis] for models in SHMM.model_emissions]
            log_Ey += np.vstack([model.log_probability(SHMM.inp_emissions[emis][seq],
                                                       SHMM.out_emissions[emis][seq]) for model in model_collection]).T

        # for the last step, need a special consideration for log_Ey of the last timestep

        if flags[seq] == 0:
#             print 'ever com in'
#             print log_Ey[-1,:]
            log_Ey_last = np.zeros((1,SHMM.num_states))
            for emis in range(SHMM.num_emissions):
                model_collection = [models[emis] for models in SHMM.model_emissions]
                if SHMM.responses_emissions[emis] != ['duration']:
                    log_Ey_last += np.vstack([model.log_probability(SHMM.inp_emissions[emis][seq][-1,:].reshape(1,-1),
                                                               SHMM.out_emissions[emis][seq][-1,:].reshape(1,-1)) for model in model_collection]).T
                # now deal with duration
                if SHMM.responses_emissions[emis] == ['duration']:
                    log_Ey_last += np.vstack([stats.norm.logsf(SHMM.out_emissions[emis][seq][-1,:].reshape(1,-1), 
                                                    loc=model.predict(SHMM.inp_emissions[emis][seq][-1,:].reshape(1,-1)), 
                                                    scale=np.sqrt(model.dispersion)) for model in model_collection]).T

            log_Ey[-1,:] = log_Ey_last
#             print log_Ey_last
#         print log_Ey[-1,:]
        log_gamma, log_epsilon, ll = calHMM(log_prob_initial, log_prob_transition, log_Ey)
        SHMM.log_gammas.append(log_gamma)
        SHMM.log_epsilons.append(log_epsilon)
        SHMM.lls.append(ll)
        SHMM.ll = sum(SHMM.lls)
    return SHMM


# In[66]:

def censored1EStep(SHMM, seq, flag):
    SHMM.log_gammas = []
    SHMM.log_epsilons = []
    SHMM.lls = []

    n_records = SHMM.dfs[seq].shape[0]
    log_prob_initial = SHMM.model_initial.predict_log_probability(SHMM.inp_initials[seq]).reshape(SHMM.num_states,)
    assert log_prob_initial.shape == (SHMM.num_states,)
    log_prob_transition = np.zeros((n_records - 1, SHMM.num_states, SHMM.num_states))
    for st in range(SHMM.num_states):
         log_prob_transition[:,st,:] = SHMM.model_transition[st].predict_log_probability(SHMM.inp_transitions[seq]) 
    assert log_prob_transition.shape == (n_records-1,SHMM.num_states,SHMM.num_states)

    log_Ey = np.zeros((n_records,SHMM.num_states))
    for emis in range(SHMM.num_emissions):
        model_collection = [models[emis] for models in SHMM.model_emissions]
        log_Ey += np.vstack([model.log_probability(SHMM.inp_emissions[emis][seq],
                                                   SHMM.out_emissions[emis][seq]) for model in model_collection]).T

    # for the last step, need a special consideration for log_Ey of the last timestep

    if flag == 0:
#             print 'ever com in'
#             print log_Ey[-1,:]
        log_Ey_last = np.zeros((1,SHMM.num_states))
        for emis in range(SHMM.num_emissions):
            model_collection = [models[emis] for models in SHMM.model_emissions]
            if SHMM.responses_emissions[emis] != ['duration']:
                log_Ey_last += np.vstack([model.log_probability(SHMM.inp_emissions[emis][seq][-1,:].reshape(1,-1),
                                                           SHMM.out_emissions[emis][seq][-1,:].reshape(1,-1)) for model in model_collection]).T
            # now deal with duration
            if SHMM.responses_emissions[emis] == ['duration']:
                log_Ey_last += np.vstack([stats.norm.logsf(SHMM.out_emissions[emis][seq][-1,:].reshape(1,-1), 
                                                loc=model.predict(SHMM.inp_emissions[emis][seq][-1,:].reshape(1,-1)), 
                                                scale=np.sqrt(model.dispersion)) for model in model_collection]).T

        log_Ey[-1,:] = log_Ey_last
#             print log_Ey_last
#         print log_Ey[-1,:]
    log_gamma, log_epsilon, ll = calHMM(log_prob_initial, log_prob_transition, log_Ey)
    SHMM.log_gammas.append(log_gamma)
    SHMM.log_epsilons.append(log_epsilon)
    SHMM.lls.append(ll)
    SHMM.ll = sum(SHMM.lls)
    return SHMM




def isStartHomeHour(aTime):
    return aTime.hour >= 17 
def isStartMorningWorkHour(aTime):
    return (aTime.hour >= 5 and aTime.hour <= 10) and aTime.isoweekday() <= 5
def isStartAfternoonWorkHour(aTime):
    return (aTime.hour >= 12 and aTime.hour <= 14) and aTime.isoweekday() <= 5
def isStartMorningHour(aTime):
    return (aTime.hour >= 5 and aTime.hour <= 10)
def isStartAfternoonHour(aTime):
    return (aTime.hour >= 12 and aTime.hour <= 14)
def isStartWorkHour(aTime):
    return isStartMorningWorkHour(aTime) or isStartAfternoonWorkHour(aTime)
def isStartLunchHour(aTime):
    return aTime.hour >= 11 and aTime.hour <= 13
def isStartDinnerHour(aTime):
    return aTime.hour >= 17 and aTime.hour <= 19
def isStartFoodHour(aTime):
    return isStartLunchHour(aTime) or isStartDinnerHour(aTime)
def isNight(aTime):
    return aTime.hour > 17 or aTime.hour <= 5
def isEvening(aTime):
    return aTime.hour > 17 and aTime.hour <= 23
def isHomeHour(aTime):
    return aTime.hour <= 5
def isLunchHour(aTime):
    return aTime.hour > 10 and aTime.hour <= 14
def isDinnerHour(aTime):
    return aTime.hour > 16 and aTime.hour <= 20
def isFoodHour(aTime):
    return isLunchHour(aTime) or isDinnerHour(aTime)
def isWorkingHour(aTime):
    return aTime.hour >= 13 and aTime.hour <= 16 and aTime.isoweekday() <= 5


def getRandPtInTaz(tazs, taz_id):
    my_taz = tazs[tazs['taz_key']==taz_id]['geometry'].iloc[0]
    my_lat, my_lon = 0,0

    while not my_taz.contains(Point(my_lon,  my_lat)):
        my_lon = np.random.uniform(my_taz.bounds[0], my_taz.bounds[2])
        my_lat = np.random.uniform(my_taz.bounds[1], my_taz.bounds[3])
    return my_lon, my_lat


# In[149]:

def initSHMM(dfs, spec_infile):
    # this is different from maddie's version since we observe part of the sequence
    with open(spec_infile) as f:
        spec = json.load(f)

    num_states = spec['num_states']
    field_of_interest = [item for sublist in spec["outputs"] for item in sublist]
    field_of_interest += spec['covariates_initial']
    field_of_interest += spec['covariates_transition']
    field_of_interest += [item for sublist in spec['covariates_emissions'] for item in sublist]
    field_of_interest = list(set(field_of_interest))

    SHMM = UnSupervisedIOHMM(num_states=num_states, max_EM_iter=1000, EM_tol=1)
    SHMM.setData(dfs)
    SHMM.setModels(model_emissions = [eval(x) for x in spec['model_emissions']], 
                           model_initial = eval(spec['model_initial']),
                           model_transition= eval(spec['model_transition']))
    SHMM.setInputs(covariates_initial = spec['covariates_initial'], 
                   covariates_transition = spec['covariates_transition'], 
                   covariates_emissions = spec['covariates_emissions'])
    SHMM.setOutputs(spec['outputs'])

    return SHMM
    
def getSHMMactivityLabels(activity_labels_infile):
    with open(activity_labels_infile) as f:
        activity_labels = json.load(f)
    return activity_labels

def getSHMMinitDeparture(departure_infile):
    with open(departure_infile) as f:
        departure_params = json.load(f)
    return departure_params


def getSHMMmodel(dfs, shmm_path, spec_path):
    spec_infile = spec_path
    SHMM = initSHMM(dfs, spec_infile)

    model_initial_coef = np.load(shmm_path + 'model_initial_coef.npy')

    model_transition_coef = []
    model_transition_sd = []

    model_emissions_coef = []
    model_emissions_sd = []
    model_emissions_dispersion = []
    for i in range(SHMM.num_states):
        model_transition_coef.append(np.load(shmm_path + 'model_transition_coef_'+str(i)+'.npy'))
        mec = []
        mes = []
        med = []

        for j in range(SHMM.num_emissions):
            mec.append(np.load(shmm_path + 'model_emissions_coef_'+str(i)+'_' + str(j) + '.npy'))
            med.append(np.load(shmm_path + 'model_emissions_dispersion_'+str(i)+'_' + str(j) + '.npy'))
        model_emissions_coef.append(mec)
        model_emissions_sd.append(mes)
        model_emissions_dispersion.append(med)

    SHMM.setParams(model_initial_coef, model_transition_coef, model_emissions_coef, model_emissions_dispersion)
    return SHMM

def get_mode_choice(taz_o, taz_d, taz_mode_map):
    ps = taz_mode_map.get((taz_o, taz_d), None)
    if not ps:
        # print taz_o, taz_d
        return 0
    return int(np.random.choice(len(ps), 1, ps))

def gen_location_from_taz(SHMM, st, dists_taz_home, dists_taz_work, taz_ids):
    # this where the location choice happens
    # the method used here is sample a taz first and then sample a location with in the taz
    
    #dist from home
    mu = SHMM.model_emissions[st][1].predict(np.array([[]]).astype('float64'))
    sigma = np.sqrt(SHMM.model_emissions[st][1].dispersion)
    p_home = stats.norm.pdf(abs(dists_taz_home-mu)/sigma)
    #dist from work
    mu = SHMM.model_emissions[st][2].predict(np.array([[]]).astype('float64'))
    sigma = np.sqrt(SHMM.model_emissions[st][2].dispersion)
    p_work = stats.norm.pdf(abs(dists_taz_work-mu)/sigma)

    # did this because the argmax starts from 0 but taz id starts from 1?
    taz = int(taz_ids[np.argmax(np.random.multinomial(1,p_home*p_work/sum(p_home*p_work)))])        
    # loc = getRandPtInTaz(tazs, taz)
    return taz

def gen_location_from_historical_locations_by_distances_to_home_and_work(SHMM, st, dists_cluster_home, dists_cluster_work, cluster_ids):
    # this where the location choice happens
    # the method used here is to get the cluster that gives the highest probability based on the distance o home and distance to work
    # essentially the same as gen_location, just changed the argument names.
    # dist from home
    
    mu = SHMM.model_emissions[st][1].predict(np.array([[]]).astype('float64'))
    sigma = np.sqrt(SHMM.model_emissions[st][1].dispersion)
    p_home = stats.norm.pdf(abs(dists_cluster_home-mu)/sigma)
    #dist from work
    mu = SHMM.model_emissions[st][2].predict(np.array([[]]).astype('float64'))
    sigma = np.sqrt(SHMM.model_emissions[st][2].dispersion)
    p_work = stats.norm.pdf(abs(dists_cluster_work-mu)/sigma)

    cluster = int(cluster_ids[np.argmax(np.random.multinomial(1,p_home*p_work/sum(p_home*p_work)))])        
    # loc = getRandPtInTaz(tazs, taz)
    return str(cluster)

def gen_location_from_historical_locations_by_activty_location_probability(st, state_cluster_map):
    # this where the location choice happens
    # the method used here is to get the cluster that has the higest probability based on the activity history
    m = state_cluster_map[st]
    cluster_ids = [k for k, v in m.iteritems()]
    ps = [v for k, v in m.iteritems()]

    cluster = int(cluster_ids[np.random.choice(len(ps), 1, p=ps)])        
    # loc = getRandPtInTaz(tazs, taz)
    return cluster

def draw_from_trunc_gamma(trunc_max, alpha, loc, scale):
    F_trunc = stats.gamma.cdf(trunc_max, alpha, loc, scale)
    random_draw = np.random.rand()
    return stats.gamma.ppf(F_trunc*random_draw, alpha, loc, scale)

def draw_from_trunc_gamma_two_sided(trunc_min, trunc_max, alpha, loc, scale):
    F_trunc = stats.gamma.cdf(trunc_max, alpha, loc, scale)
    f_trunc = stats.gamma.cdf(trunc_min, alpha, loc, scale)
    random_draw = np.random.uniform(f_trunc, F_trunc)
    return stats.gamma.ppf(random_draw, alpha, loc, scale)
# In[204]:
def check_good_seq(seq):
    activ_types = [activ[1] for activ in seq]
#     if sequence does not end at home
    if activ_types[-1] != 'Home':
        return False
    
    # if sequence has travel
    if 'Travel' in activ_types:
        return False        

    # removing this criteria for now!
    # # if sequence has H-Stop in transit-H or W-Stop in transit-W
    # while 'Stop in Transit' in activ_types:
    #     activ_types.remove('Stop in Transit')
    # home_activs = [activ_types[i] == 'Home' for i in range(len(activ_types))]
    # if True in [home_activs[i] & home_activs[i+1] for i in range(len(home_activs)-1)]:
    #     return False

    # work_activs = [activ_types[i] == 'Work' for i in range(len(activ_types))]
    # if True in [work_activs[i] & work_activs[i+1] for i in range(len(work_activs)-1)]:
    #     return False
    
    # if sequence has 3 of the same activity in a row
    two_in_a_row = [activ_types[i] == activ_types[i+1] for i in range(len(activ_types)-1)]
    three_in_a_row = [two_in_a_row[i] & two_in_a_row[i+1] for i in range(len(two_in_a_row)-1)]
    if any(three_in_a_row):
        return False

    # else (if didn't find any bad patterns):
    return True

def init_chain(h_latlon, w_latlon, h_taz, w_taz):
    chain = {}
    chain['profile'] = {}
    chain['profile']['h_latlon'] = h_latlon
    chain['profile']['h_id'] = 0#
    chain['profile']['w_latlon'] = w_latlon
    chain['profile']['w_id'] = 1#
    chain['profile']['vids'] = {0: h_latlon, 1: w_latlon}
    chain['profile']['h_taz'] = h_taz
    chain['profile']['w_taz'] = w_taz
    chain['chains'] = {}

    return chain

def create_chain(chain, activities, day_start):
    # if activ not home or work
    activ_number = 0
    next_cluster_id = 2

    chain['chains'][day_start] = {}
    for activ in activities:
        # should add tz info!
        start = activ[2].strftime('%Y-%m-%d %H:%M:%S'+'-07:00')
        end = (activ[2]+datetime.timedelta(minutes=math.ceil(60*activ[3]))).strftime('%Y-%m-%d %H:%M:%S'+'-07:00')
        event = {'cell_id': 0,
                 'start': start,
                 'end': end,
                 'coords': activ[4][::-1],
                 'taz': activ[-2],
                 'mode':activ[-1]}
        if activ[1] == 'Home':
            event['cluster'] = 0
        elif activ[1] == 'Work':
            event['cluster'] = 1 
        else:
            event['cluster'] = next_cluster_id
            chain['profile']['vids'][next_cluster_id] = activ[4][::-1]
            next_cluster_id += 1
            
        chain['chains'][day_start][activ_number] = event
        activ_number += 1
    return chain
def get_home_work_activ(activity_labels):
    for key, val in activity_labels.iteritems():
        if val == 'Home':
            home_key = key
        elif val == 'Work':
            work_key = key
    return int(home_key), int(work_key)



# the following are the things that get changed

def gen_mode_choice(dt, activity_type_o, st_o, loc_o, taz_o, activity_type_d, st_d, loc_d, taz_d):
    ps = travel_mode_map.get((taz_o, taz_d), None)
    if not ps:
        return 0
    return int(np.random.choice(len(ps), 1, ps))


def est_travel_time(dt, activity_type_o, st_o, loc_o, taz_o, activity_type_d, st_d, loc_d, taz_d, mode):
    return np.sqrt(((loc_d[1]-loc_o[1])*89.7)**2 + ((loc_d[0]-loc_o[0])*112)**2) / travel_speed_map[mode]

def update_context(context, dt):
    context.set_value(0,'start_weekend',(datetime.datetime.isoweekday(dt) > 5) * 1)
    context.set_value(0,'start_morning_hour', isStartMorningHour(dt) * 1)
    context.set_value(0,'start_lunch_hour', isStartLunchHour(dt) * 1)
    context.set_value(0,'start_afternoon_hour', isStartAfternoonHour(dt) * 1)
    context.set_value(0,'start_dinner_hour', isStartDinnerHour(dt) * 1)
    context.set_value(0,'start_home_hour', isStartHomeHour(dt) * 1)
    return context

def draw_transition_time(min_time):
    fit_alpha = 0.384384911907
    fit_loc = 0.0166666666667
    fit_beta = 13.0693990504
    return draw_from_trunc_gamma_two_sided(min_time, 2, fit_alpha, fit_loc, fit_beta)

def distance(my_lat, my_lon, pts):
    # approximate distance in km
    return np.sqrt(((my_lat-pts[:,0])*110)**2 + ((my_lon-pts[:,1])*90)**2)



def gen_location_mode(SHMM, dt, activity_type, st, home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
                      activity_type_prev, st_prev, loc_prev, taz_prev,location_method = 1, user_clusters = None, user_clusters_taz = None, user_cluster_activity_map = None):
    if activity_type == 'Home':
        loc = [home_loc[1], home_loc[0]]
        taz = home_taz
    elif activity_type == 'Work':
        loc = [work_loc[1], work_loc[0]]
        taz = work_taz
    else:
        if location_method == 0:
            # generate location using tazs by distance to home and work
            dists_taz_home = distance(home_loc[0], home_loc[1], taz_centroids[:,1:])
            dists_taz_work = distance(work_loc[0], work_loc[1], taz_centroids[:,1:])
            taz_ids = taz_centroids[:,0]

            taz = gen_location_from_taz(SHMM, st, dists_taz_home, dists_taz_work, taz_ids)
            loc = getRandPtInTaz(tazs, taz)

        elif location_method == 1:
            # generate location using previous visited locatons by distance to home and work
            cluster_centroids = np.array([[k, v[0], v[1]] for k, v in user_clusters.iteritems()]).astype('float64')
            dists_cluster_home = distance(home_loc[0], home_loc[1], cluster_centroids[:,1:])
            dists_cluster_work = distance(work_loc[0], work_loc[1], cluster_centroids[:,1:])
            cluster_ids = cluster_centroids[:,0]

            cluster = gen_location_from_historical_locations_by_distances_to_home_and_work(SHMM, st, dists_cluster_home, dists_cluster_work, cluster_ids)
            # convert from lat lon to lon lat
            loc = user_clusters[cluster][::-1]
            taz = user_clusters_taz[cluster]


        else: 
            # generate location using previous visited locatons by distance to home and work

            cluster = gen_location_from_historical_locations_by_activty_location_probability(st, user_cluster_activity_map)
            # convert from lat lon to lon lat
            loc = user_clusters[cluster][::-1]
            taz = user_clusters_taz[cluster]

    mode = gen_mode_choice(dt, activity_type_prev, st_prev, loc_prev, taz_prev, activity_type, st, loc, taz)
    return loc, taz, mode


def gen_next_activity(SHMM, context, dt, min_transit, home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
                      activity_types, activity_type_prev, st_prev, loc_prev, taz_prev,
                      location_method = 1, user_clusters = None, user_clusters_taz = None, user_cluster_activity_map = None):
    
    # dt is end time of the previous activity
    transition_time = draw_transition_time(min_transit)
    dt_tentative = dt + datetime.timedelta(minutes = math.ceil(transition_time*60))
    context = update_context(context, dt_tentative)
 
    log_prob_states = SHMM.model_transition[st_prev].predict_log_probability(context[SHMM.covariates_transition])
    st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(log_prob_states).reshape(-1,)))
    while (st == home_state and st_prev == home_state) or (st == work_state and st_prev == work_state):
        st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(log_prob_states).reshape(-1,)))
    activity_type = activity_types[str(st)]
    loc, taz, mode =  gen_location_mode(SHMM, dt, activity_type, st,  home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
                                    activity_type_prev, st_prev, loc_prev, taz_prev, location_method = location_method,
                                    user_clusters = user_clusters, user_clusters_taz = user_clusters_taz, 
                                    user_cluster_activity_map = user_cluster_activity_map)

    dt = dt_tentative
    # sample duration
    lower, upper = 5/60, 48
    mu = SHMM.model_emissions[st][0].predict(np.array(context[SHMM.covariates_emissions[0]]).astype('float64'))
    sigma = np.sqrt(SHMM.model_emissions[st][0].dispersion)/1.5
    dur = stats.truncnorm.rvs((lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,size=1)[0]
    if st == work_state:
        context.set_value(0,'hours_at_work_that_day', context.ix[0, 'hours_at_work_that_day'] + dur)

    # in this run, we need to augment the activity to include taz, rather than cluster id?
    activity = [st, activity_type, dt, dur, loc, taz, mode]#, loc]
#         print dur
    return activity

# def gen_next_activity(SHMM, context, dt, min_transit, home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
#                       activity_types, activity_type_prev, st_prev, loc_prev, taz_prev,
#                       location_method = 1, user_clusters = None, user_clusters_taz = None, user_cluster_activity_map = None):
    
#     # dt is end time of the previous activity
#     transition_time = draw_transition_time(min_transit)
#     dt_tentative = dt + datetime.timedelta(minutes = int(transition_time*60))
#     context = update_context(context, dt_tentative)
#     old_context = 0
#     new_context = 1
    
#     i = 0
#     while old_context != new_context and i < 5:
#         old_context = list(context.values[0])
#         log_prob_states = SHMM.model_transition[st_prev].predict_log_probability(context)
#         st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(log_prob_states).reshape(-1,)))
#         while (st == home_state and st_prev == home_state) or (st == work_state and st_prev == work_state):
#             st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(log_prob_states).reshape(-1,)))
#         activity_type = activity_types[str(st)]
#         loc, taz, mode =  gen_location_mode(SHMM, dt,activity_type,st,  home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
#                                         activity_type_prev, st_prev, loc_prev, taz_prev, location_method = location_method,
#                                         user_clusters = user_clusters, user_clusters_taz = user_clusters_taz, 
#                                         user_cluster_activity_map = user_cluster_activity_map)
        
#         transition_time = est_travel_time(dt, activity_type_prev, st_prev, loc_prev, taz_prev, activity_type, st, loc, taz, mode)
#         dt_tentative  = dt + datetime.timedelta(minutes = int(transition_time*60))
#         context = update_context(context, dt_tentative)
#         new_context = list(context.values[0])
#         i+=1
        

#     dt = dt_tentative
#     # sample duration
#     lower, upper = 5/60, 48
#     mu = SHMM.model_emissions[st][0].predict(np.array(context[SHMM.covariates_emissions[0]]).astype('float64'))
#     sigma = np.sqrt(SHMM.model_emissions[st][0].dispersion)/2
#     dur = stats.truncnorm.rvs((lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,size=1)[0]
#     if st == work_state:
#         context.set_value(0,'hours_at_work_that_day', context.ix[0, 'hours_at_work_that_day'] + dur)

#     # in this run, we need to augment the activity to include taz, rather than cluster id?
#     activity = [st, activity_type, dt, dur, loc, taz, mode]#, loc]
# #         print dur
#     return activity


def genSequencefromCensoredData(SHMM, seq_index, context_variables, start_date, cut_time, done, home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, activity_types,location_method = 1, user_clusters = None, user_clusters_taz = None, user_cluster_activity_map = None):
    # if use historical locations, tazs and taz_centoid can be empty
    # if use previous method (random point in taz), clusters can be empty
    # df should be a day
    # the location choice can either happen here or later, preferably later to keep this only a function of activity sampling
    df = SHMM.dfs[seq_index]
    SHMM = censored1EStep(SHMM, seq_index, done)
    activities = []
    activity_type_prev = None
    st_prev = None
    loc_prev = None
    taz_prev = None
    et = None
    for df_i in range(len(df)):
        if df.iloc[df_i]['cluster_id'] == home_id:
            st = home_state
        elif df.iloc[df_i]['cluster_id'] == work_id:
            st = work_state
        else:
            st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(SHMM.log_gammas[0][df_i]).reshape(-1,)))
        activity_type = activity_types[str(st)]
        loc = [df.iloc[df_i]['lon'], df.iloc[df_i]['lat']]
        taz = user_clusters_taz.get(str(df.iloc[df_i]['cluster_id']), -1)
        mode = gen_mode_choice(et, activity_type_prev, st_prev, loc_prev, taz_prev, activity_type, st, loc, taz)
        activities.append([st, activity_type, parser.parse(df.iloc[df_i].start_time), df.iloc[df_i]['duration'], loc, taz, mode])
        activity_type_prev = activity_type
        st_prev = st
        loc_prev = loc
        taz_prev = taz
        et = parser.parse(df.iloc[df_i].end_time)
    # context_variables = ["start_weekend", "start_morning_hour", "start_lunch_hour", "start_afternoon_hour","start_dinner_hour", "start_home_hour", "hours_at_work_that_day"]
    context = pd.DataFrame(columns=[context_variables], dtype = float)
    

    if done == 0:
        # only need to resample duration
        context.set_value(0,'hours_at_work_that_day',df.iloc[:-1][df['cluster_id'] == work_id]['duration'].sum(axis = 0))
        dt = activities[-1][2]
        st = activities[-1][0]
        lower, upper = max(5/60,activities[-1][3]) , 48
        context = update_context(context, dt)
        mu = SHMM.model_emissions[st][0].predict(np.array(context[SHMM.covariates_emissions[0]]).astype('float64'))
        sigma = np.sqrt(SHMM.model_emissions[st][0].dispersion)/1.5
        dur = stats.truncnorm.rvs((lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,size=1)[0]

        if st == work_state:
            context.set_value(0,'hours_at_work_that_day', context.ix[0, 'hours_at_work_that_day'] + dur)
        activities[-1][3]=dur
        et = activities[-1][2] + datetime.timedelta(minutes = math.ceil(60*dur))
        transition_time = 0
    
    else:
        context.set_value(0,'hours_at_work_that_day',df[df['cluster_id'] == work_id]['duration'].sum(axis = 0))
        transition_time = (cut_time - et).total_seconds()/3600
        if transition_time >= 2:
            return '',''


    while et < start_date+datetime.timedelta(hours=24):
    # while dt.date() == start_date.date():
        #sets context at activity start.
        activity_type_prev, st_prev, loc_prev, taz_prev = activities[-1][1], activities[-1][0], activities[-1][4], activities[-1][5] 
        activity = gen_next_activity(SHMM, context, et, transition_time, 
                     home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
                      activity_types, activity_type_prev, st_prev, loc_prev, taz_prev,
                      location_method, user_clusters, user_clusters_taz, user_cluster_activity_map)
        
        et = activity[2] + datetime.timedelta(minutes = math.ceil(60*activity[3]))
        
        if activity[2] < start_date+datetime.timedelta(hours=24):
            activities.append(activity)
   
    # only generate location if it's a good seq
    if check_good_seq(activities):
        activ_chain = init_chain(home_loc, work_loc, home_taz, work_taz)
        activ_chain = create_chain(activ_chain, activities, str(start_date))

        return activities, activ_chain
    return '',''




def genSequencefromCensoredDataGroundTruth(SHMM, seq_index, context_variables, start_date, home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, activity_types, location_method = 1, taz_centroids = None, user_clusters = None, user_clusters_taz = None, user_cluster_activity_map = None):
    # if use historical locations, tazs and taz_centoid can be empty
    # if use previous method (random point in taz), clusters can be empty
    # df should be a day
    # the location choice can either happen here or later, preferably later to keep this only a function of activity sampling
    df = SHMM.dfs[seq_index]
    SHMM = censored1EStep(SHMM, seq_index, 1)
    activities = []
    activity_type_prev = None
    st_prev = None
    loc_prev = None
    taz_prev = None
    et = None
    for df_i in range(len(df)):
        if df.iloc[df_i]['cluster_id'] == home_id:
            st = home_state
        elif df.iloc[df_i]['cluster_id'] == work_id:
            st = work_state
        else:
            st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(SHMM.log_gammas[0][df_i]).reshape(-1,)))

        activity_type = activity_types[str(st)]
        loc = [df.iloc[df_i]['lon'], df.iloc[df_i]['lat']]
        taz = user_clusters_taz.get(str(df.iloc[df_i]['cluster_id']), -1)
        mode = gen_mode_choice(et, activity_type_prev, st_prev, loc_prev, taz_prev, activity_type, st, loc, taz)
        activities.append([st, activity_type, parser.parse(df.iloc[df_i].start_time), df.iloc[df_i]['duration'], loc, taz, mode])
        activity_type_prev = activity_type
        st_prev = st
        loc_prev = loc
        taz_prev = taz
        et = parser.parse(df.iloc[df_i].end_time)

    if check_good_seq(activities):
        # distance of each taz to home and work location, plus 
        
        # this is the usage of generate locations, and travel mode. Currently, 
        # travel mode does not consider the activity type, we can do that as well later
        activ_chain = init_chain(home_loc, work_loc, home_taz, work_taz)
        activ_chain = create_chain(activ_chain, activities, str(start_date))

        return activities, activ_chain
    return '',''

def genSequences(dfs, shmm_path, spec_path, activity_label_path, start_date, cut_time, dones, users_home_id, users_home, users_home_taz, users_work_id, 
                 users_work, users_work_taz,
                 location_method = 0,users_clusters = None, users_cluster_taz = None, users_cluster_activity_map = None):
    # this function generate the sequences of all the dfs in a superdistrict.
    # the SHMM here is the SHMM for a superdistrict.

    # location_method: 0: random point in taz based on distance to home and work. 
    #                  1: previous cluster based on distance to home and work
    #                  2: previous cluster based on loc_activity probability

    SHMM = getSHMMmodel(dfs, shmm_path, spec_path)
    activity_labels = getSHMMactivityLabels(activity_label_path)
    home_state, work_state = get_home_work_activ(activity_labels)
    n_sequences = len(dfs)
    seqs = []
    all_chains = {}
    n = 0
    context_variables = ["start_weekend", "start_morning_hour", "start_lunch_hour", "start_afternoon_hour","start_dinner_hour", "start_home_hour", "hours_at_work_that_day"]


    for i in range(n_sequences):
        # print i
        try:
            done = dones[i]
            home_id = users_home_id[i]
            work_id = users_work_id[i]
            home_loc = users_home[i]
            work_loc = users_work[i]
            home_taz = users_home_taz[i]
            work_taz = users_work_taz[i]
            user_clusters = users_clusters[i]
            user_cluster_activity_map = None
            user_clusters_taz = users_cluster_taz[i]
            if location_method == 2:
                user_cluster_activity_map = users_cluster_activity_map[i]
            seq = ''
            num_it = 0
            while seq == '' and num_it < 5:
                seq, chain = genSequencefromCensoredData(SHMM, i, context_variables, start_date, cut_time, done,
                    home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
                    activity_labels,location_method,user_clusters, user_clusters_taz, user_cluster_activity_map)
                num_it += 1
            if i % 1000 == 0:
                print i
                print datetime.datetime.now()
            seqs.append(seq)
            uid = SHMM.dfs[i].iloc[0]['uid']
            all_chains[uid] = chain
        except:
            print 'bad number in i', i
            print SHMM.dfs[i]
            print home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz

    return seqs, all_chains



def genSequencesGroundTruth(dfs, shmm_path, spec_path, activity_label_path, start_date, users_home_id, users_home, users_home_taz, users_work_id, 
                 users_work, users_work_taz,
                 location_method = 0,users_clusters = None, users_cluster_taz = None, users_cluster_activity_map = None):
    # this function generate the sequences of all the dfs in a superdistrict.
    # the SHMM here is the SHMM for a superdistrict.

    # location_method: 0: random point in taz based on distance to home and work. 
    #                  1: previous cluster based on distance to home and work
    #                  2: previous cluster based on loc_activity probability

    SHMM = getSHMMmodel(dfs, shmm_path, spec_path)
    activity_labels = getSHMMactivityLabels(activity_label_path)
    home_state, work_state = get_home_work_activ(activity_labels)
    n_sequences = len(dfs)
    seqs = []
    all_chains = {}
    n = 0
    context_variables = ["start_weekend", "start_morning_hour", "start_lunch_hour", "start_afternoon_hour","start_dinner_hour", "start_home_hour", "hours_at_work_that_day"]
    
    taz_centroids = None

    if location_method == 0:
        taz_centroids = np.array([[b, a.xy[1][0], a.xy[0][0]] for a,b in zip(tazs.centroid,tazs.taz_key)])


    for i in range(n_sequences):
        # print i
        try:
            home_id = users_home_id[i]
            work_id = users_work_id[i]
            home_loc = users_home[i]
            work_loc = users_work[i]
            home_taz = users_home_taz[i]
            work_taz = users_work_taz[i]
            user_clusters = users_clusters[i]
            user_cluster_activity_map = None
            user_clusters_taz = users_cluster_taz[i]
            if location_method == 2:
                user_cluster_activity_map = users_cluster_activity_map[i]
            seq = ''
            num_it = 0
            while seq == '' and num_it < 5:
                seq, chain = genSequencefromCensoredDataGroundTruth(SHMM, i, context_variables, start_date, 
                    home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz, 
                    activity_labels, location_method, taz_centroids, user_clusters, user_clusters_taz, 
                    user_cluster_activity_map)
                num_it += 1
            if i % 1000 == 0:
                print i
                print datetime.datetime.now()
            seqs.append(seq)
            uid = SHMM.dfs[i].iloc[0]['uid']
            all_chains[uid] = chain
        except:
            print 'bad number in i', i
            print SHMM.dfs[i]
            print home_state, home_id, home_loc, home_taz, work_state, work_id, work_loc, work_taz

    return seqs, all_chains


# # Test the function for bunch data
# global env
path = '/home/mogeng/attResearch/resources/BATS2000_1454MTCTAZ_TripTableData/'
travel_mode_df = pd.read_table(path + 'TOTALTripTable.jan0505.dat', sep='\s+', skiprows=0, header=None, index_col=False, 
                   names = ['o','d','0','1','2','3','4','5','6','7','8','9'])
travel_mode_dict = travel_mode_df.set_index(['o','d']).T.to_dict('list')
travel_mode_map = {k: [float(i)/sum(v) for i in v] for k, v in travel_mode_dict.iteritems()}
travel_speed_map = {i:30 for i in range(10)}
taz_infile = '/home/mogeng/attResearch/resources/COCs_2011_All_TAZs/COCs_2011_All_TAZs.shp'
tazs = gpd.read_file(taz_infile)
taz_centroids = np.array([[b, a.xy[1][0], a.xy[0][0]] for a,b in zip(tazs.centroid,tazs.taz_key)])
tz = pytz.timezone("US/Pacific")
field_of_interests = ['uid','duration', 'lat', 'lon', 'dist_to_home', 'dist_to_work', 'start_time','end_time',
'has_visited', 'has_visited2', 'cluster_id', 'hours_at_work_that_day',
"start_weekend", "start_morning_hour", "start_lunch_hour", "start_afternoon_hour", "start_dinner_hour", "start_home_hour"]
tz = pytz.timezone("US/Pacific")
def main():
    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('output_path', type=str)
    argparser.add_argument('data_path', type=str)
    argparser.add_argument('model_path', type=str)
    argparser.add_argument('spec_path', type=str)
    argparser.add_argument('label_path', type=str)
    argparser.add_argument("year", type=int)
    argparser.add_argument("month", type=int)
    argparser.add_argument("date", type=int)
    argparser.add_argument('--location_method', type=int, default=0,
                        help='location generation algorithm')
    argparser.add_argument('--num_data', type=int, default=0,
                        help='number of data to read')

    args = argparser.parse_args()
    print datetime.datetime.now()
    output_path = args.output_path
    data_path = args.data_path
    model_path = args.model_path
    spec_path = args.spec_path
    activity_label_path = args.label_path

    start_date = tz.localize(datetime.datetime(args.year,args.month,args.date,3,0,0,0))

    location_method = args.location_method




    # prepare_df
    dfs_all = []
    profiles_all = []
    
    os.chdir(data_path)
    z = 0

    if args.num_data <= 0:
        for fi in glob.glob("*.csv"):
            user_id = fi.split('.csv')[0]
            df = pd.read_csv(fi)
            with open(user_id + '.json') as json_data:
                profile = json.load(json_data)

            df['dist_to_home'] = df['dist_to_home'] / 1609
            df['dist_to_work'] = df['dist_to_work'] / 1609
            df['has_visited2'] = 1-df['has_visited']   
            dfs_all.append(df[field_of_interests])
            profiles_all.append(profile)
            z += 1
            if z % 1000 == 0:
                print z
                print datetime.datetime.now()
    else:
        for fi in glob.glob("*.csv")[:args.num_data]:
            user_id = fi.split('.csv')[0]
            df = pd.read_csv(fi)
            with open(user_id + '.json') as json_data:
                profile = json.load(json_data)
            
            df['dist_to_home'] = df['dist_to_home'] / 1609
            df['dist_to_work'] = df['dist_to_work'] / 1609
            df['has_visited2'] = 1-df['has_visited']   
            dfs_all.append(df[field_of_interests])
            profiles_all.append(profile)
            z += 1
            if z % 1000 == 0:
                print z
                print datetime.datetime.now()
        # else:
        #     print df.iloc[0]
        #     print profile['h_id']
        #     print ''
    


    for hour in range(3, 24):
        if not os.path.exists(output_path+str(hour) + '_' + str(args.location_method) + '_1.5_std_dur_transit.json'):
            print 'hour is: ', hour
            cut_time = tz.localize(datetime.datetime(args.year,args.month,args.date,hour,0,0,0))
            # print cut_time
            dfs = []
            dones = []
            users_home_id = []
            users_home = []
            users_home_taz = []
            users_work_id = []
            users_work = []
            users_work_taz = []
            users_clusters = []
            users_cluster_taz = []
            users_cluster_activity_map = None
            for i in range(len(dfs_all)):
                # print i
                df = dfs_all[i]
                profile = profiles_all[i]
                df_new = df[(df.start_time <= str(cut_time)) & (df.end_time > str(start_date))]
                # make sure the first activity is home
                if len(df_new) > 0 and df_new.iloc[0]['cluster_id'] == profile['h_id']:
                    # df_new.iloc[-1,df_new.columns.get_loc('end_time')] = str(cut_time)
                    # print 'yeah'
                    # print df_new.columns.get_loc('duration')
                    # break
                    # df_new.duration = df_new.apply(lambda x: (min(parser.parse(x['end_time']), cut_time) - parser.parse(x['start_time'])).total_seconds()/3600, axis = 1)
                    if str(cut_time) >= df_new.iloc[-1]['end_time']:
                        dones.append(1)
                    else:
                    # df_new.duration.iloc[-1] = (cut_time - parser.parse(df_new.iloc[-1]['start_time'])).total_seconds()/3600
                        df_new.set_value(df_new.index[-1], 'end_time', str(cut_time))
                        df_new.set_value(df_new.index[-1], 'duration', (cut_time - parser.parse(df_new.iloc[-1]['start_time'])).total_seconds()/3600)
                        dones.append(0)
                    # print 'yeah done'
                    dfs.append(df_new)
                    users_home_id.append(profile['h_id'])
                    users_home.append(profile['h_latlon'])
                    users_home_taz.append(profile['h_taz'])
                    users_work_id.append(profile['w_id'])
                    users_work.append(profile['w_latlon'])
                    users_work_taz.append(profile['w_taz'])
                    users_clusters.append(profile['vids'])
                    users_cluster_taz.append(profile['cluster_taz'])
            print 'number of data: ', len(dfs)
            print datetime.datetime.now()
            seqs, all_chains = genSequences(dfs, model_path, spec_path, activity_label_path, start_date, cut_time, dones, users_home_id, users_home, users_home_taz, users_work_id, 
                             users_work, users_work_taz,
                             location_method = location_method,users_clusters = users_clusters, users_cluster_taz = users_cluster_taz, users_cluster_activity_map = users_cluster_activity_map)
            print 'finished generating sequences'
            print datetime.datetime.now()
            # print seqs
            if not os.path.exists(output_path):
                os.makedirs(output_path)
            with open(output_path+str(hour) + '_' + str(args.location_method) + '_1.5_std_dur_transit.json', 'w') as outfile:
                json.dump(all_chains, outfile, indent = 4)
            print 'finished saving sequences'
            print datetime.datetime.now()

    # get ground truth
    if not os.path.exists(output_path+ 'gt_1.5_std_dur_transit.json'):
        cut_time = start_date + datetime.timedelta(days=1)
        dfs = []
        users_home_id = []
        users_home = []
        users_home_taz = []
        users_work_id = []
        users_work = []
        users_work_taz = []
        users_clusters = []
        users_cluster_taz = []
        users_cluster_activity_map = None
        for i in range(len(dfs_all)):
            df = dfs_all[i]
            profile = profiles_all[i]
            df_new = df[(df.start_time <= str(cut_time)) & (df.end_time > str(start_date))]
            # make sure the first activity is home
            if len(df_new) > 0 and df_new.iloc[0]['cluster_id'] == profile['h_id']:
                dfs.append(df_new)
                users_home_id.append(profile['h_id'])
                users_home.append(profile['h_latlon'])
                users_home_taz.append(profile['h_taz'])
                users_work_id.append(profile['w_id'])
                users_work.append(profile['w_latlon'])
                users_work_taz.append(profile['w_taz'])
                users_clusters.append(profile['vids'])
                users_cluster_taz.append(profile['cluster_taz'])
        print 'number of data: ', len(dfs)
        print datetime.datetime.now()
        seqs, all_chains = genSequencesGroundTruth(dfs, model_path, spec_path, activity_label_path, start_date, users_home_id, users_home, users_home_taz, users_work_id, 
                         users_work, users_work_taz,
                         location_method = location_method,users_clusters = users_clusters, users_cluster_taz = users_cluster_taz, users_cluster_activity_map = users_cluster_activity_map)
        print 'finished generating sequences'
        print datetime.datetime.now()
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        with open(output_path + 'gt_1.5_std_dur_transit.json', 'w') as outfile:
            json.dump(all_chains, outfile, indent = 4)
        print 'finished saving sequences'
        print datetime.datetime.now()

if __name__ == '__main__':
    main()



