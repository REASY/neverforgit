#


'''
Generate sequence helper functions.
'''

from shapely.geometry import Point
import numpy as np

def isStartHomeHour(aTime):
    return aTime.hour >= 17 
def isStartMorningWorkHour(aTime):
    return (aTime.hour >= 5 and aTime.hour <= 10) and aTime.isoweekday() <= 5
def isStartAfternoonWorkHour(aTime):
    return (aTime.hour >= 12 and aTime.hour <= 14) and aTime.isoweekday() <= 5
def isStartMorningHour(aTime):
    return (aTime.hour >= 5 and aTime.hour <= 10)
def isStartAfternoonHour(aTime):
    return (aTime.hour >= 12 and aTime.hour <= 14)
def isStartWorkHour(aTime):
    return isStartMorningWorkHour(aTime) or isStartAfternoonWorkHour(aTime)
def isStartLunchHour(aTime):
    return aTime.hour >= 11 and aTime.hour <= 13
def isStartDinnerHour(aTime):
    return aTime.hour >= 17 and aTime.hour <= 19
def isStartFoodHour(aTime):
    return isStartLunchHour(aTime) or isStartDinnerHour(aTime)
def isNight(aTime):
    return aTime.hour > 17 or aTime.hour <= 5
def isEvening(aTime):
    return aTime.hour > 17 and aTime.hour <= 23
def isHomeHour(aTime):
    return aTime.hour <= 5
def isLunchHour(aTime):
    return aTime.hour > 10 and aTime.hour <= 14
def isDinnerHour(aTime):
    return aTime.hour > 16 and aTime.hour <= 20
def isFoodHour(aTime):
    return isLunchHour(aTime) or isDinnerHour(aTime)
def isWorkingHour(aTime):
    return aTime.hour >= 13 and aTime.hour <= 16 and aTime.isoweekday() <= 5


def getRandPtInTaz(tazs, taz_id):
    my_taz = tazs[tazs['TAZ1454']==taz_id]['geometry'].iloc[0]
    my_lat, my_lon = 0,0

    while not my_taz.contains(Point(my_lon,  my_lat)):
        my_lon = np.random.uniform(my_taz.bounds[0], my_taz.bounds[2])
        my_lat = np.random.uniform(my_taz.bounds[1], my_taz.bounds[3])
    return my_lon, my_lat