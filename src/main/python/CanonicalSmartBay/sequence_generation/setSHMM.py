



import json
import sys
sys.path.append("/Users/madeleinesheehan/GitHub/IO-HMM/code/main")
sys.path.append("/Users/madeleinesheehan/GitHub/IO-HMM/code/auxiliary")
from UnSupervised_IOHMM import *


def initSHMM(spec_infile):
    with open(spec_infile) as f:
        spec = json.load(f)

    num_states = spec['num_states']
    field_of_interest = [item for sublist in spec["outputs"] for item in sublist]
    field_of_interest += spec['covariates_initial']
    field_of_interest += spec['covariates_transition']
    field_of_interest += [item for sublist in spec['covariates_emissions'] for item in sublist]
    field_of_interest = list(set(field_of_interest))

    SHMM = UnSupervisedIOHMM(num_states=num_states, max_EM_iter=1000, EM_tol=1)
    SHMM.setData([pd.DataFrame(index = [0], columns=[field_of_interest]).fillna(0)
    ])
    SHMM.setModels(model_emissions = [eval(x) for x in spec['model_emissions']], 
                           model_initial = eval(spec['model_initial']),
                           model_transition= eval(spec['model_transition']))
    SHMM.setInputs(covariates_initial = spec['covariates_initial'], 
                   covariates_transition = spec['covariates_transition'], 
                   covariates_emissions = spec['covariates_emissions'])
    SHMM.setOutputs(spec['outputs'])

    return SHMM
    
def getSHMMactivityLabels(activity_labels_infile):
    with open(activity_labels_infile) as f:
        activity_labels = json.load(f)
    return activity_labels

def getSHMMinitDeparture(departure_infile):
    with open(departure_infile) as f:
        departure_params = json.load(f)
    return departure_params


def getSHMMmodel(shmm_path):
    spec_infile = shmm_path + 'spec.json'
    SHMM = initSHMM(spec_infile)

    model_initial_coef = np.load(shmm_path + 'model_initial_coef.npy')

    model_transition_coef = []
    model_transition_sd = []

    model_emissions_coef = []
    model_emissions_sd = []
    model_emissions_dispersion = []
    for i in range(SHMM.num_states):
        model_transition_coef.append(np.load(shmm_path + 'model_transition_coef_'+str(i)+'.npy'))
        mec = []
        mes = []
        med = []

        for j in range(SHMM.num_emissions):
            mec.append(np.load(shmm_path + 'model_emissions_coef_'+str(i)+'_' + str(j) + '.npy'))
            med.append(np.load(shmm_path + 'model_emissions_dispersion_'+str(i)+'_' + str(j) + '.npy'))
        model_emissions_coef.append(mec)
        model_emissions_sd.append(mes)
        model_emissions_dispersion.append(med)

    SHMM.setParams(model_initial_coef, model_transition_coef, model_emissions_coef, model_emissions_dispersion)
    return SHMM