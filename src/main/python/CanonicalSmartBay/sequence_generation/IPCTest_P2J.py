import os
import subprocess

os.chdir("/Users/daddy30000/dev/ucb_smartcities_all/sandbox/neverforgit/build/classes/main")

# p = subprocess.Popen(["java", "IPCTest"], stdin=subprocess.PIPE)
p = subprocess.Popen(['java', 'sandbox.neverforgit.serialTripRouter.run.IPCTest'], stdin=subprocess.PIPE,
                     stdout=subprocess.PIPE)
# Testing P2J communication
p.stdin.write("First line\r\n")
p.stdin.write("Second line\r\n")
p.stdin.write("x\r\n") # this line will not be printed into the file
# Testing J2P communication
line = p.stdout.readline()
while(line != "x\n"):
    print line
    line = p.stdout.readline()
