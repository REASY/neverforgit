
import datetime
from utils import *
from setSHMM import *
import scipy.stats as stats
import numpy as np


def genSequence(SHMM, start_date, home_state, home_loc, work_state, work_loc, activity_types, departure_params, taz_centroids, tazs):
    # we can generate sequences with length of a day, or a week
    # this is too customized that I don't know if should be put here
    # we assume the activities will start form a home.


    context = pd.DataFrame(columns=[["start_weekend", "start_morning_hour", "start_lunch_hour", 
                                     "start_afternoon_hour","start_dinner_hour", "start_home_hour", "hours_at_work_that_day"]], dtype = float)
    
    # start minutes
    lower,upper = 270, 900
    
    # weekday
    # Use departure time from spec instead of parameters below
    mu = departure_params['weekday']['mu']*60 #8.814*60
    sigma = departure_params['weekday']['std']*60#2.016*60
    start_minutes = int(stats.truncnorm.rvs((lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,size=1))

    # start_datetime = start time of first activity after leaving home.
    dt = start_date+datetime.timedelta(minutes=start_minutes)
    st = home_state
    cov = SHMM.covariates_emissions
    context.loc[0,'hours_at_work_that_day'] = 0
    activities = [[home_state, 'Home', dt.replace(hour=0, minute=0), start_minutes/60.]]

    #TODO model transition:
    fit_alpha = 0.384384911907
    fit_loc = 0.0166666666667
    fit_beta = 13.0693990504
    transition_time = draw_from_trunc_gamma(2,fit_alpha,fit_loc,fit_beta)
    dt += datetime.timedelta(minutes = int(transition_time*60))


    while dt < start_date+datetime.timedelta(hours=24):
    # while dt.date() == start_date.date():
        #sets context at activity start.
        context.loc[0,'start_weekend'] = (datetime.datetime.isoweekday(dt) > 5) * 1
        context.loc[0,'start_morning_hour'] = isStartMorningHour(dt) * 1
        context.loc[0,'start_lunch_hour'] = isStartLunchHour(dt) * 1
        context.loc[0,'start_afternoon_hour'] = isStartAfternoonHour(dt) * 1
        context.loc[0,'start_dinner_hour'] = isStartDinnerHour(dt) * 1
        context.loc[0,'start_home_hour'] = isStartHomeHour(dt) * 1

        log_prob_states = SHMM.model_transition[st].predict_log_probability(context)
        st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(log_prob_states).reshape(-1,)))
        while (st == home_state and activities[-1][0] == home_state) or (st == work_state and activities[-1][0] == work_state):
            st = int(np.random.choice(SHMM.num_states, 1, p=np.exp(log_prob_states).reshape(-1,)))
            
        # sample duration
        lower, upper = 0.25, 20
        mu = SHMM.model_emissions[st][0].predict(np.array(context[SHMM.covariates_emissions[0]]).astype('float64'))
        sigma = np.sqrt(SHMM.model_emissions[st][0].dispersion)
        
        dur = stats.truncnorm.rvs((lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,size=1)[0]

        if st == work_state:
            context.loc[0,'hours_at_work_that_day'] += dur

        activity = [st, activity_types[str(st)], dt, dur]#, loc]
        
        dt += datetime.timedelta(minutes = int(60*dur))
        

        #TODO: check units of transition time?
        transition_time = draw_from_trunc_gamma(2,fit_alpha,fit_loc,fit_beta)
        # print int(transition_time*60)
        dt += datetime.timedelta(minutes = int(transition_time*60))
        activities.append(activity)
    def distance(my_lat, my_lon, pts):
        # approximate distance in km
        return np.sqrt(((my_lat-pts[:,0])*110)**2 + ((my_lon-pts[:,1])*90)**2)

    # only generate location if it's a good seq
    if check_good_seq(activities):
        # distance of each taz to home and work location, plus 
        dists_taz_home = distance(home_loc[0], home_loc[1], taz_centroids[:,1:])
        dists_taz_work = distance(work_loc[0], work_loc[1], taz_centroids[:,1:])
        taz_ids = taz_centroids[:,0]


        for activ in activities:
            st = activ[0]
            activity_type= activ[1]
            if activity_type == 'Home':
                location = [home_loc[1], home_loc[0]]
            elif activity_type == 'Work':
                location = [work_loc[1], work_loc[0]]
            else:
                taz = gen_location(SHMM, st, dists_taz_home, dists_taz_work, taz_ids)
                location = getRandPtInTaz(tazs, taz)
            activ.append(location)
        
        activ_chain = init_chain(home_loc, work_loc)
        activ_chain = create_chain(activ_chain, activities)

        return activities[1:], activ_chain
    return '',''

def draw_from_trunc_gamma(trunc_max, alpha, loc, scale):
    F_trunc = stats.gamma.cdf(trunc_max, alpha, loc, scale)
    random_draw = np.random.rand()
    return stats.gamma.ppf(F_trunc*random_draw, alpha, loc, scale)

def genSequences(sd_shmms, sd_activity_labels, sd_departure_params, n_sequences, tazs, taz_sd_lookup, hw_matrix, n_strt=0):
    seqs = []
    all_chains = {}
    n = 0

    taz_centroids = np.array([[b, a.xy[1][0], a.xy[0][0]] for a,b in zip(tazs.centroid,tazs.TAZ1454)])

    for i in range(n_sequences):
        if i%1000 == 0:
            print i
        #sample origin taz
        #get home taz
        taz_h = np.argmax(np.random.multinomial(1,np.sum(hw_matrix,1)/np.sum(hw_matrix)))
        while taz_h == 1173: #TODO TAZ 1174 missing from taz shapefile, so resample if get this taz
            taz_h = np.argmax(np.random.multinomial(1,np.sum(hw_matrix,1)/np.sum(hw_matrix)))
        # given home taz, set work:
        taz_w = np.argmax(np.random.multinomial(1,hw_matrix[taz_h]/np.sum(hw_matrix[taz_h])))
        while taz_w == 1173:
            taz_w = np.argmax(np.random.multinomial(1,hw_matrix[taz_h]/np.sum(hw_matrix[taz_h])))
        
        taz_h += 1 #0-index to 1-indexed
        taz_w += 1 #0-index to 1-indexed

        SHMM, activity_labels, departure_params = get_model(sd_shmms, sd_activity_labels, 
        													sd_departure_params, taz_h, 
        													taz_sd_lookup)
        seq, chain = gen1seq(SHMM, tazs, activity_labels, departure_params, taz_h, taz_w, taz_centroids)

        seqs.append(seq)
        all_chains[str(i+n_strt).zfill(15)] = chain

    return seqs, all_chains

def gen1seq(SHMM, tazs, activity_labels, departure_params, home_taz, work_taz, taz_centroids):
    home_activ, work_activ = get_home_work_activ(activity_labels)
    start_date = datetime.datetime(year = 2016, month = 3, day = 23)
    home_lon, home_lat = getRandPtInTaz(tazs, home_taz)
    work_lon, work_lat = getRandPtInTaz(tazs, work_taz)
    seq = ''
    while seq == '':
        seq, activ_chain = genSequence(SHMM, start_date,
                                home_activ, [home_lat, home_lon],
                                work_activ, [work_lat, work_lon],
                                activity_labels, departure_params,
                                taz_centroids, tazs)
    return seq, activ_chain

def get_model(models, activity_labels, departure_params, taz_h, taz_sd_lookup):
    model_number = taz_sd_lookup[str(taz_h)]
    return models[model_number-1], activity_labels[model_number-1], departure_params[model_number-1]

def get_all_sd_models(shmms_path):
    sd_shmms = []
    sd_activity_labels = []
    sd_departure_params = []
    for sd in range(34):
        shmm_path = shmms_path + '7_mr_spec_8_2000_super_%i/'%sd
        SHMM = getSHMMmodel(shmm_path)
        activity_labels = getSHMMactivityLabels(shmm_path+'activity_labels.json')
        departure_params = getSHMMactivityLabels(shmm_path+'end_time_param.json')
        sd_shmms.append(SHMM)
        sd_activity_labels.append(activity_labels)
        sd_departure_params.append(departure_params)
    return sd_shmms, sd_activity_labels, sd_departure_params



def gen_location(SHMM, st, dists_taz_home, dists_taz_work, taz_ids):
    
    #dist from home
    mu = SHMM.model_emissions[st][1].predict(np.array([[]]).astype('float64'))
    sigma = np.sqrt(SHMM.model_emissions[st][1].dispersion)
    p_home = stats.norm.pdf(abs(dists_taz_home-mu)/sigma)
    #dist from work
    mu = SHMM.model_emissions[st][2].predict(np.array([[]]).astype('float64'))
    sigma = np.sqrt(SHMM.model_emissions[st][2].dispersion)
    p_work = stats.norm.pdf(abs(dists_taz_work-mu)/sigma)

    taz = int(taz_ids[np.argmax(np.random.multinomial(1,p_home*p_work/sum(p_home*p_work)))])        
    # loc = getRandPtInTaz(tazs, taz)
    return taz

#new file:
def init_chain(h_latlon, w_latlon):
    chain = {}
    chain['profile'] = {}
    chain['profile']['h_latlon'] = h_latlon
    chain['profile']['h_id'] = 0#
    chain['profile']['w_latlon'] = w_latlon
    chain['profile']['w_id'] = 1#
    chain['profile']['vids'] = {0: h_latlon, 1: w_latlon}
    chain['chains'] = {}
    return chain

def create_chain(chain, activities):
    # if activ not home or work
    activ_number = 0
    next_cluster_id = 2
    day_start = activities[0][2].replace(hour=3,minute=0).strftime('%Y-%m-%d %H:%M:%S')+'-07:00'
    chain['chains'][day_start] = {}
    for activ in activities:
        # should add tz info!
        start = activ[2].strftime('%Y-%m-%d %H:%M:%S'+'-07:00')
        end = (activ[2]+datetime.timedelta(minutes=int(60*activ[3]))).strftime('%Y-%m-%d %H:%M:%S'+'-07:00')
        event = {'cell_id': 0,
                 'start': start,
                 'end': end,
                 'coords': activ[4][::-1]}
        if activ[1] == 'Home':
            event['cluster'] = 0
        elif activ[1] == 'Work':
            event['cluster'] = 1 
        else:
            event['cluster'] = next_cluster_id
            chain['profile']['vids'][next_cluster_id] = activ[4][::-1]
            next_cluster_id += 1
            
        chain['chains'][day_start][activ_number] = event
        activ_number += 1
    return chain

def check_good_seq(seq):
    activ_types = [activ[1] for activ in seq]
#     if sequence does not end at home
    if activ_types[-1] != 'Home':
        return False
    
    # if sequence has travel
    if 'Travel' in activ_types:
        return False        

    # removing this criteria for now!
    # # if sequence has H-Stop in transit-H or W-Stop in transit-W
    # while 'Stop in Transit' in activ_types:
    #     activ_types.remove('Stop in Transit')
    # home_activs = [activ_types[i] == 'Home' for i in range(len(activ_types))]
    # if True in [home_activs[i] & home_activs[i+1] for i in range(len(home_activs)-1)]:
    #     return False

    # work_activs = [activ_types[i] == 'Work' for i in range(len(activ_types))]
    # if True in [work_activs[i] & work_activs[i+1] for i in range(len(work_activs)-1)]:
    #     return False
    
    # if sequence has 3 of the same activity in a row
    two_in_a_row = [activ_types[i] == activ_types[i+1] for i in range(len(activ_types)-1)]
    three_in_a_row = [two_in_a_row[i] & two_in_a_row[i+1] for i in range(len(two_in_a_row)-1)]
    if any(three_in_a_row):
        return False

    # else (if didn't find any bad patterns):
    return True


    
def get_home_work_activ(activity_labels):
    for key, val in activity_labels.iteritems():
        if val == 'Home':
            home_key = key
        elif val == 'Work':
            work_key = key
    return int(home_key), int(work_key)


if __name__ =='__main__':
    import geopandas as gpd
    taz_infile = 'resources/bay_area_tazs/bay_area_tazs.shp'
    shmms_path = 'resources/BayArea_models/' 
    hw_matrix = np.load('resources/MTC_OD.npy')
    n_sequences = 100000
    n_strt = 0
    

    tazs = gpd.read_file(taz_infile)
    sd_shmms, sd_activity_labels = get_all_sd_models(shmms_path)
    with open('resources/taz_sd_lookup.json') as f:
        taz_sd_lookup = json.load(f)

    seqs, all_chains = genSequences(sd_shmms, sd_activity_labels) #add dept time here, n_sequences, tazs, 
                                    #taz_sd_lookup, hw_matrix, n_strt)

    with open('resources/sample_chains_out.json', 'w') as f:
        json.dump(all_chains, f, indent=4)
