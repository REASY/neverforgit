import numpy as np

__author__ = 'Andrew A Campbell'


######################
# Matching the Links #
######################

def validate_screenline(link_ids, aggr_hours, counts_df, counts_col, facility_name):
    """

    :param link_ids: ([int]) List of the IDs of the MATSim network links.
    :param aggr_hours: ([int]) Defines the hours to be included in aggregation. Uses 24-hr time. First hour is 1, last
    is 24.
    :param counts_df: (pd.DataFrame) DataFrame of the MATSim counts validation output (countscompare.txt). DF has not been processed
    significantly after running pd.read_csv.
    :param counts_col: (str) Name of the column of counts to aggregate. This is useful if you have added a rescaled
    column.
    :param facility_name: (str) Name of screenline facility
    :return: ([int, int, int, double])  Screenline Facility, Observed, Predicted, Predicted_Less_Obs, Prcnt_Difference
    """
    rows = counts_df[np.array([id in link_ids for id in counts_df['Link Id']]) &
                     np.array([count in aggr_hours for count in counts_df['Hour']])]
    observed = np.sum(rows['Count volumes'])
    predicted = np.sum(rows[counts_col])
    diff = predicted - observed
    try:
        prct_diff = np.true_divide(diff, observed)
    except ZeroDivisionError:
        prct_diff = float("inf")
    return [facility_name, observed, predicted, diff, prct_diff]

def optimize_counts(df):
    """
    Used for optimizing the CountsScaleFactor parameter that MATSim uses for scaling counts.
    :param df: (pd.DataFrame) A DF read directly from the run0.*.countscompare.txt file that MATSim
    produces.
    :return: (float, float, float) The optimized value to scale the CountsScaleFactor by, original RMSE, and new RMSE
    after applying the optimized CountsScaleFactor
    """
    # Calculate original RMSE
    o_numer = np.dot(df['MATSIM volumes'].subtract(df['Count volumes']), df['MATSIM volumes'].subtract(df['Count volumes']))
    o_denom = df.shape[0]
    o_RMSE = np.sqrt(np.true_divide(o_numer, o_denom))

    # Optimal value to rescale CountsScaleFactor
    alpha = np.true_divide(df['MATSIM volumes'].dot(df['Count volumes']), df['MATSIM volumes'].dot(df['MATSIM volumes']))

    # Rescaled RMSE
    r_numer = np.dot(alpha * df['MATSIM volumes'] - df['Count volumes'],
                     alpha * df['MATSIM volumes'] - df['Count volumes'])
    r_RMSE = np.sqrt(np.true_divide(r_numer, o_denom))

    return alpha, o_RMSE, r_RMSE